﻿using UnityEngine;
using System.Collections;

public enum ITEMTYPE
{
    USED = 0,
    GARO,
    SERO,
    KING,
    QUEEN,
    COLORBOMBER_RED,
    COLORBOMBER_YELLOW,
    COLORBOMBER_GREEN,
    COLORBOMBER_BLUE,
    COLORBOMBER_PURPLE,
    BOMBER,
}

public class GameManager
{

    public enum BLOCKTYPE
    {
        COLORED = 0,
        NUI,
        WALL,
        EMPTY,
        ITEM,
    }


    public class IntPoint
    {
        public IntPoint(int x, int y) { this.x = x; this.y = y; }
        public int x;
        public int y;
    }

    public class MissionAdapter
    {
        public Mission mission;
        public bool completed;
        public int option;
        public GameObject text;
    }

    public class MissionSnapShot
    {
        public bool completed;
        public int option;
    }

    public class BlockCompact
    {
        public int option;
        public int type;
        public int item;
    }

    public class SnapShot
    {
        public System.Collections.Generic.List<BlockCompact> snapshot;
        public System.Collections.Generic.List<MissionSnapShot> missions;
        public int chainCombo;
        public int chainColor;
        public int comboMeter;
        public float deltaTime;
        public int beforeWorldPoint;
        public int point;
        public IntPoint touchPoint;

    }

    private void SetRedWhite()
    {
        if (redWhiteFlag == true)
            return;
        redWhiteTimer = 0f;
        redWhiteColor = true;
        redWhiteFlag = true;
    }

    private void UnSetRedWhite()
    {
        redWhiteTimer = 0f;
        redWhiteColor = false;
        redWhiteFlag = false;
    }



    /*
    public class YComparer : IComparer
    {
        Comparer comparer = new Comparer(System.Globalization.CultureInfo.CurrentCulture);
        public int Compare(object x, object y)
        {
            IntPoint left = (IntPoint)x;
            IntPoint right = (IntPoint)y;
            if (left.y == right.y)
                return comparer.Compare(left.x, right.x);
            return comparer.Compare(left.y, right.y);
        }
    }*/

    private bool redWhiteFlag;
    private float redWhiteTimer;
    private bool redWhiteColor;

    private bool isInfinite;
    private bool[] generateFlag;
    private int[] generateColor;
    private int generateKind;
    private System.Collections.Generic.List<MissionAdapter>[] missions;
    private System.Collections.Generic.List<BlockCompact> generateList;

    private GameObject blockParent;

    private int chainCombo;
    private int chainColor;

    private bool isItemStage;

    private bool kindRedWhiteFlag;
    private int kindRedWhiteRemainTime;
    private bool kindRedWhiteColor;
    private float kindRedWhiteProgress;

    private int width;
    private int height;

    private int inGameStartY;
    private float limitScale;
    private bool limitFlag;
    private GameObject limitObject;
    private GameObject limitKind;
    private int comboMeter;
    public bool isRanked { get; private set; }
    public int prevScore { get; private set; }
    public int score { get; private set; }
    //public int bonus { get; private set; }
    public int bonus_turn { get; private set; }
    public int mission_turn { get; private set; }
    public int play_turn { get; private set; }
    public float play_time { get; private set; }
    public bool cleared { get; private set; }
    private Block[,] blockArr;
    private float originX;
    private float originY;

    public bool noMore;

    private GameObject scoreFrame;
    private GameObject bonusFrame;
    private float scoreFrameValue;
    private float scoreFrameRealValue;
    private float bonusFrameValue;
    private float bonusFrameRealValue;
    private bool scoreFrameFlag;
    private bool bonusFrameFlag;

    private int[] itemQTY;


    private int[] grade_score;

    public System.Collections.Generic.List<SnapShot> snapShotList;
    private int worldPoint;
    //private ArrayList beforeList;   // save is CHECK_PROCESS after, because ITEM_PROCESS
    //private int beforePoint;

    private bool gameWasOver;

    private bool gamePaused;

    private float interval = 0.65f;

    //    private float secondCounter;

    private bool timerSwitch;
    private float timer;


    GameObject red;
    GameObject yellow;
    GameObject green;
    GameObject blue;
    GameObject purple;
    GameObject nui;
    GameObject wall;
    GameObject font;
    GameObject mark;
    GameObject garo;
    GameObject sero;
    GameObject king;

    private static GameManager manager;
    public static GameManager GetInstance()
    {
        if (manager == null)
            manager = new GameManager();
        return manager;
    }

    public void SetFrame(GameObject scoreObject, GameObject bonusObject)
    {
        scoreFrame = scoreObject;
        bonusFrame = bonusObject;

        scoreFrameValue = 0f;
        scoreFrameRealValue = 0f;

        bonusFrameValue = 0f;
        bonusFrameRealValue = 0f;

        scoreFrameFlag = true;
        bonusFrameFlag = true;

        scoreFrame.GetComponent<UITexture>().fillAmount = 0f;
        bonusFrame.GetComponent<UITexture>().fillAmount = 0f;
    }

    private int GetBonusScore(int allTurn, int spendTurn)
    {
        float a = ((float)allTurn) / spendTurn - 1;
        if (a > 0f)
            return (int)(a * 416f * allTurn);
        return 0;
    }

    private void LimitScaleApply()
    {
        limitFlag = true;
        limitScale = 3f;
        if (limitObject != null)
            limitObject.transform.localScale = new Vector3(limitScale, limitScale, 1f);
    }


    public static void RemoveInstance()
    {
        manager = null;
    }


    private ArrayList ItemPop(int x, int y, int type)
    {
        ArrayList list = new ArrayList();
        if (type == (int)ITEMTYPE.COLORBOMBER_RED)
        {
            for (int i = inGameStartY; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (blockArr[i, j] != null && blockArr[i, j].type == (int)BLOCKTYPE.COLORED && blockArr[i, j].option == (int)BLOCK.BLOCK_RED)
                    {
                        list.Add(new IntPoint(j, i));
                    }
                }
            }
        }
        else if (type == (int)ITEMTYPE.COLORBOMBER_YELLOW)
        {
            for (int i = inGameStartY; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (blockArr[i, j] != null && blockArr[i, j].type == (int)BLOCKTYPE.COLORED && blockArr[i, j].option == (int)BLOCK.BLOCK_YEL)
                    {
                        list.Add(new IntPoint(j, i));
                    }
                }
            }
        }
        else if (type == (int)ITEMTYPE.COLORBOMBER_GREEN)
        {
            for (int i = inGameStartY; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (blockArr[i, j] != null && blockArr[i, j].type == (int)BLOCKTYPE.COLORED && blockArr[i, j].option == (int)BLOCK.BLOCK_GRN)
                    {
                        list.Add(new IntPoint(j, i));
                    }
                }
            }
        }
        else if (type == (int)ITEMTYPE.COLORBOMBER_BLUE)
        {
            for (int i = inGameStartY; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (blockArr[i, j] != null && blockArr[i, j].type == (int)BLOCKTYPE.COLORED && blockArr[i, j].option == (int)BLOCK.BLOCK_BLU)
                    {
                        list.Add(new IntPoint(j, i));
                    }
                }
            }
        }
        else if (type == (int)ITEMTYPE.COLORBOMBER_PURPLE)
        {
            for (int i = inGameStartY; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (blockArr[i, j] != null && blockArr[i, j].type == (int)BLOCKTYPE.COLORED && blockArr[i, j].option == (int)BLOCK.BLOCK_PURPLE)
                    {
                        list.Add(new IntPoint(j, i));
                    }
                }
            }
        }
        else if (type == (int)ITEMTYPE.KING)
        {
            for (int i = y - 1; i <= y + 1; i++)
            {
                for (int j = x - 1; j <= x + 1; j++)
                {
                    if (i >= inGameStartY && i < height && j >= 0 && j < width && blockArr[i, j] != null)
                    {
                        list.Add(new IntPoint(j, i));
                    }
                }
            }
        }
        else if (type == (int)ITEMTYPE.GARO)
        {
            for (int i = 0; i < width; i++)
            {
                if (blockArr[y, i] != null)
                {
                    list.Add(new IntPoint(i, y));
                }
            }
        }
        else if (type == (int)ITEMTYPE.SERO)
        {
            for (int i = inGameStartY; i < height; i++)
            {
                if (blockArr[i, x] != null)
                {
                    list.Add(new IntPoint(x, i));
                }
            }
        }
        return list;
        /*
        ArrayList result = new ArrayList();
        ArrayList nextSearch = new ArrayList();
        bool[,] checkMap = new bool[height, width];
        int type = blockArr[y, x].type;
        int color = blockArr[y, x].option;
        if (type != (int)BLOCKTYPE.COLORED)
            return result;
        nextSearch.Add(new IntPoint(x, y));
        checkMap[y, x] = true;
        while (true)
        {
            #region CompactFF_PROCESS
            ArrayList nowSearch = new ArrayList(nextSearch);
            nextSearch.Clear();
            foreach (IntPoint block in nowSearch)
            {
                result.Add(block);
                if (block.y + 1 != height && blockArr[block.y + 1, block.x] != null &&
                    blockArr[block.y + 1, block.x].type == type &&
                    blockArr[block.y + 1, block.x].option == color && checkMap[block.y + 1, block.x] == false)
                {
                    nextSearch.Add(new IntPoint(block.x, block.y + 1));
                    checkMap[block.y + 1, block.x] = true;
                }
                if (block.y > inGameStartY && blockArr[block.y - 1, block.x] != null &&
                    blockArr[block.y - 1, block.x].type == type &&
                    blockArr[block.y - 1, block.x].option == color && checkMap[block.y - 1, block.x] == false)
                {
                    nextSearch.Add(new IntPoint(block.x, block.y - 1));
                    checkMap[block.y - 1, block.x] = true;
                }
                if (block.x + 1 != width && blockArr[block.y, block.x + 1] != null &&
                    blockArr[block.y, block.x + 1].type == type &&
                    blockArr[block.y, block.x + 1].option == color && checkMap[block.y, block.x + 1] == false)
                {
                    nextSearch.Add(new IntPoint(block.x + 1, block.y));
                    checkMap[block.y, block.x + 1] = true;
                }
                if (block.x != 0 && blockArr[block.y, block.x - 1] != null &&
                    blockArr[block.y, block.x - 1].type == type
                    && blockArr[block.y, block.x - 1].option == color && checkMap[block.y, block.x - 1] == false)
                {
                    nextSearch.Add(new IntPoint(block.x - 1, block.y));
                    checkMap[block.y, block.x - 1] = true;
                }
            }
            if (nextSearch.Count == 0)
                break;
            #endregion
        }
        return result;*/
    }

    public string GetMissionString(MissionAdapter adapter)
    {
        string color = "";
        if ((MISSION)adapter.mission.type == MISSION.MISSION_CB || (MISSION)adapter.mission.type == MISSION.MISSION_PM)
        {
            switch ((BLOCK)adapter.mission.optionArray[0])
            {
                case BLOCK.BLOCK_RED:
                    color = @"Red";
                    break;
                case BLOCK.BLOCK_YEL:
                    color = @"Yellow";
                    break;
                case BLOCK.BLOCK_GRN:
                    color = @"Green";
                    break;
                case BLOCK.BLOCK_BLU:
                    color = @"Blue";
                    break;
                case BLOCK.BLOCK_PURPLE:
                    color = @"Purple";
                    break;
                case BLOCK.BLOCK_NUI:
                    color = @"Black";
                    break;
            }
        }
        switch ((MISSION)adapter.mission.type)
        {
            case MISSION.MISSION_AC:
                return "Remove all blocks";
            case MISSION.MISSION_CB:
                return string.Format("Remove {0} blocks ({1}/{2})", color, adapter.option, adapter.mission.optionArray[1]);
            case MISSION.MISSION_DB:
                return string.Format("Remove the target block ({0},{1})", adapter.mission.optionArray[0], adapter.mission.optionArray[1]);
            case MISSION.MISSION_ML:
                return string.Format("{0}", adapter.option);
            case MISSION.MISSION_PM:
			return string.Format("{1} Remove {0} blocks, with 1 turn (best:{2})", color, adapter.mission.optionArray[1], adapter.option);
            case MISSION.MISSION_SC:
                return string.Format("Reach {0} score", adapter.mission.optionArray[0]);
            case MISSION.MISSION_TL:
                return string.Format("{0}", adapter.option);
            default:
                return "";
        }
		/*
		 * 
            case MISSION.MISSION_AC:
                return "Remove all blocks";
            case MISSION.MISSION_CB:
				return string.Format("{1} Remove {0} blocks", color, adapter.optionArray[1]);
            case MISSION.MISSION_DB:
                return string.Format("Remove the target block ({0},{1})", adapter.optionArray[0], adapter.optionArray[1]);
            case MISSION.MISSION_ML:
                return string.Format("Complete in {0} turns", adapter.optionArray[0]);
            case MISSION.MISSION_PM:
			return string.Format("{1} Remove {0} blocks, in 1 turn", color, adapter.optionArray[1]);
            case MISSION.MISSION_SC:
                return string.Format("Reach {0}score", adapter.optionArray[0]);
            case MISSION.MISSION_TL:
                return string.Format("Complete in {0} seconds", adapter.optionArray[0]);
            default:
                return "";
		 * */
    }

    public void SetPreviousScore(int score)
    {
        if (score == 0)
        {
            isRanked = false;
            prevScore = 0;
        }

        else
        {
            isRanked = true;
            prevScore = score;
        }
    }

    public void GenerateProcess()
    {
        if (isInfinite == true)
        {
            generateList.Clear();
            bool isPass = false;
            for (int j = 0; j < width && isPass == false; j++)
            {
                for (int i = inGameStartY; i < height && isPass == false; i++)
                {
                    if (blockArr[i, j] != null && blockArr[i, j].type == (int)BLOCKTYPE.ITEM)
                    {
                        isPass = true;
                        break;
                    }
                    if (PopTest(j, i).Count > 1)
                        isPass = true;
                }
            }

            for (int j = 0; j < width; j++)
            {
                for (int i = height - 1; i >= 0; i--)
                {
                    if (blockArr[i, j] == null)
                    {
                        BlockCompact instance = new BlockCompact();
                        int color = Random.Range(0, generateKind);
                        //GameObject instance = null;
                        instance.option = color;
                        instance.type = (int)BLOCKTYPE.COLORED;
                        instance.item = 0;
                        generateList.Add(instance);
                    }
                }
            }
        }
    }

    public int TakeSnapShot(IntPoint touchPoint, float delay)
    {
        SnapShot sayCheese = new SnapShot();
        sayCheese.deltaTime = delay;
        sayCheese.beforeWorldPoint = worldPoint;
        sayCheese.point = score;
        sayCheese.touchPoint = touchPoint;
        sayCheese.chainCombo = chainCombo;
        sayCheese.chainColor = chainColor;
        sayCheese.comboMeter = comboMeter;
        sayCheese.missions = new System.Collections.Generic.List<MissionSnapShot>();
        sayCheese.snapshot = new System.Collections.Generic.List<BlockCompact>();
        for (int i = 1; i < 6; i++)
        {
            for (int j = 0; j < missions[i].Count; j++)
            {
                MissionSnapShot mission = new MissionSnapShot();
                mission.completed = missions[i][j].completed;
                mission.option = missions[i][j].option;
                sayCheese.missions.Add(mission);
            }
        }

        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                BlockCompact block = null;
                if (blockArr[i, j] != null)
                {
                    block = new BlockCompact();
                    block.item = blockArr[i, j].item;
                    block.option = blockArr[i, j].option;
                    block.type = blockArr[i, j].type;
                }
                sayCheese.snapshot.Add(block);
            }
        }
        snapShotList.Add(sayCheese);
        worldPoint = snapShotList.Count - 1;
        return snapShotList.Count - 1;
    }

    public string PrintMapForReplayIndex()
    {
        string result = "{\"map\":\"";

        for (int i = inGameStartY; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                string charSet;
                int type;
                int option;
                int item;
                if (snapShotList[0].snapshot[i * width + j] == null)
                {
                    type = (int)BLOCKTYPE.EMPTY;
                    option = 0;
                    item = 0;
                }
                else
                {
                    type = snapShotList[0].snapshot[i * width + j].type;// blockArr[i, j].type;
                    option = snapShotList[0].snapshot[i * width + j].option;
                    item = snapShotList[0].snapshot[i * width + j].item;
                }
                switch (type)
                {
                    case (int)BLOCKTYPE.COLORED:
                        if (option == 0) charSet = "R,";
                        else if (option == 1) charSet = "Y,";
                        else if (option == 2) charSet = "G,";
                        else if (option == 3) charSet = "B,";
                        else if (option == 4) charSet = "P,";
                        else charSet = "0,";
                        break;

                    case (int)BLOCKTYPE.NUI:
                        charSet = "N" + option.ToString() + ",";
                        break;

                    case (int)BLOCKTYPE.WALL:
                        charSet = "W,";
                        break;

                    case (int)BLOCKTYPE.EMPTY:
                        charSet = "0,";
                        break;
                    case (int)BLOCKTYPE.ITEM:
                        charSet = "I";
                        switch (item)
                        {
                            case (int)ITEMTYPE.GARO:
                                charSet += "H,";
                                break;
                            case (int)ITEMTYPE.SERO:
                                charSet += "V,";
                                break;
                            case (int)ITEMTYPE.COLORBOMBER_RED:
                                charSet += "R,";
                                break;
                            case (int)ITEMTYPE.COLORBOMBER_YELLOW:
                                charSet += "Y,";
                                break;
                            case (int)ITEMTYPE.COLORBOMBER_GREEN:
                                charSet += "G,";
                                break;
                            case (int)ITEMTYPE.COLORBOMBER_BLUE:
                                charSet += "B,";
                                break;
                            case (int)ITEMTYPE.COLORBOMBER_PURPLE:
                                charSet += "P,";
                                break;
                            case (int)ITEMTYPE.KING:
                                charSet += "3,";
                                break;
                            default:
                                Debug.Log("REPLAY ERROR");
                                break;
                        }
                        break;
                    default:
                        charSet = "0,";
                        break;
                }
                result += charSet;
            }
            result = result.Remove(result.Length - 1);
            result += "!";
        }
        result = result.Remove(result.Length - 1);
        result += "\",\"rule\":\"true,";//+isInfinite.ToString()+;
        if (isInfinite == true) result += "true,";
        else result += "false,";

        for (int i = 0; i < 5; i++)
        {
            if (generateFlag[i] == true) result += "1,";
            else result += "0,";
        }
        result = result.Remove(result.Length - 1);
        result += "\",\"width\":" + width + ",\"mission\":\"";


        for (int i = 0; i < StageReader.GetInstance().GetStage().missionArray.Count; i++)
        {
            switch (StageReader.GetInstance().GetStage().missionArray[i].type)
            {
                case (int)MISSION.MISSION_AC:
                    result += "AC";
                    break;
                case (int)MISSION.MISSION_CB:
                    result += "CB";
                    switch (StageReader.GetInstance().GetStage().missionArray[i].optionArray[0])
                    {
                        case 0:
                            result += "R,";
                            break;
                        case 1:
                            result += "Y,";
                            break;
                        case 2:
                            result += "G,";
                            break;
                        case 3:
                            result += "B,";
                            break;
                        case 4:
                            result += "P,";
                            break;
                        case 5:
                            result += "N,";
                            break;
                        default:
                            Debug.Log("Mission Parsing Error");
                            break;
                    }
                    result += StageReader.GetInstance().GetStage().missionArray[i].optionArray[1].ToString();
                    break;
                case (int)MISSION.MISSION_DB:
                    result += "DB";
                    result += StageReader.GetInstance().GetStage().missionArray[i].optionArray[0].ToString();
                    result += ",";
                    result += StageReader.GetInstance().GetStage().missionArray[i].optionArray[1].ToString();
                    break;
                case (int)MISSION.MISSION_ML:
                    result += "ML";
                    result += StageReader.GetInstance().GetStage().missionArray[i].optionArray[0].ToString();
                    break;
                case (int)MISSION.MISSION_PM:
                    result += "PM";
                    switch (StageReader.GetInstance().GetStage().missionArray[i].optionArray[0])
                    {
                        case 0:
                            result += "R,";
                            break;
                        case 1:
                            result += "Y,";
                            break;
                        case 2:
                            result += "G,";
                            break;
                        case 3:
                            result += "B,";
                            break;
                        case 4:
                            result += "P,";
                            break;
                        case 5:
                            result += "N,";
                            break;
                        default:
                            Debug.Log("Mission Parsing Error");
                            break;
                    }
                    result += StageReader.GetInstance().GetStage().missionArray[i].optionArray[1].ToString();
                    break;
                case (int)MISSION.MISSION_SC:
                    result += "SC";
                    result += StageReader.GetInstance().GetStage().missionArray[i].optionArray[0].ToString();
                    break;
                case (int)MISSION.MISSION_TL:
                    result += "TL";
                    result += StageReader.GetInstance().GetStage().missionArray[i].optionArray[0].ToString();
                    break;
                default:
                    Debug.Log("Mission Parsing Error");
                    break;
            }
            result += "!";
        }
        result = result.Remove(result.Length - 1);
        result += "\",\"";
        result += "height\":" + height + "}#";

        System.Collections.Generic.Stack<int> timeStack = new System.Collections.Generic.Stack<int>();

        int t = worldPoint;
        //timeStack.Push(worldPoint);
        while (true)
        {
            timeStack.Push(t);
            t = snapShotList[t].beforeWorldPoint;
            if (t == -1)
                break;
        }

        int count = 1;
        while (timeStack.Count > 0)
        {
            result += PrintSnapShotForReplayIndex(timeStack.Pop(), count) + "#";
            count++;
        }
        timeStack.Clear();
        timeStack = null;
        result = result.Remove(result.Length - 1);
//        Debug.Log(result);
        return result;
    }

    public string PrintSnapShotForReplayIndex(int point, int index)
    {
        string result = "";
        for (int i = inGameStartY; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                string charSet;
                int type;
                int option = 0;
                int item = 0;
                if (snapShotList[point].snapshot[i * width + j] == null)
                {
                    type = (int)BLOCKTYPE.EMPTY;
                }
                else
                {
                    type = snapShotList[point].snapshot[i * width + j].type;
                    option = snapShotList[point].snapshot[i * width + j].option;
                    item = snapShotList[point].snapshot[i * width + j].item;
                }
                switch (type)
                {
                    case (int)BLOCKTYPE.COLORED:
                        if (option == 0) charSet = "R,";
                        else if (option == 1) charSet = "Y,";
                        else if (option == 2) charSet = "G,";
                        else if (option == 3) charSet = "B,";
                        else if (option == 4) charSet = "P,";
                        else charSet = "0,";
                        break;

                    case (int)BLOCKTYPE.NUI:
                        charSet = "N" + option.ToString() + ",";
                        break;

                    case (int)BLOCKTYPE.WALL:
                        charSet = "W,";
                        break;

                    case (int)BLOCKTYPE.EMPTY:
                        charSet = "0,";
                        break;
                    case (int)BLOCKTYPE.ITEM:
                        charSet = "I";
                        switch (item)
                        {
                            case (int)ITEMTYPE.GARO:
                                charSet += "H,";
                                break;
                            case (int)ITEMTYPE.SERO:
                                charSet += "V,";
                                break;
                            case (int)ITEMTYPE.COLORBOMBER_RED:
                                charSet += "R,";
                                break;
                            case (int)ITEMTYPE.COLORBOMBER_YELLOW:
                                charSet += "Y,";
                                break;
                            case (int)ITEMTYPE.COLORBOMBER_GREEN:
                                charSet += "G,";
                                break;
                            case (int)ITEMTYPE.COLORBOMBER_BLUE:
                                charSet += "B,";
                                break;
                            case (int)ITEMTYPE.COLORBOMBER_PURPLE:
                                charSet += "P,";
                                break;
                            case (int)ITEMTYPE.KING:
                                charSet += "3,";
                                break;
                            default:
                                Debug.Log("REPLAY ERROR");
                                break;
                        }
                        break;
                    default:
                        charSet = "0,";
                        Debug.Log("REPLAY ERROR");
                        break;
                }
                result += charSet;
            }
            result = result.Remove(result.Length - 1);
            result += "|";
        }
        result = result.Remove(result.Length - 1);
        result += "@" + (snapShotList[point].point * 10).ToString() + "@" + snapShotList[point].touchPoint.x.ToString() +
            "," + (snapShotList[point].touchPoint.y - inGameStartY).ToString() + "@" + index.ToString() + "@" + ((int)snapShotList[point].deltaTime).ToString()
            + "@" + ((int)snapShotList[point].deltaTime * 1000f).ToString();
        return result;
    }

    public Block CreateBlock(int type, int option, int item, int x, int y)
    {

        GameObject instance = null;
        GameObject fontInstance = null;
        Block block = null;
        GameObject parent = blockParent;
        switch (type)
        {
            case (int)BLOCKTYPE.COLORED:
                switch (option)
                {
                    case 0:
                        instance = MonoBehaviour.Instantiate(red) as GameObject;
                        break;
                    case 1:
                        instance = MonoBehaviour.Instantiate(yellow) as GameObject;
                        break;
                    case 2:
                        instance = MonoBehaviour.Instantiate(green) as GameObject;
                        break;
                    case 3:
                        instance = MonoBehaviour.Instantiate(blue) as GameObject;
                        break;
                    case 4:
                        instance = MonoBehaviour.Instantiate(purple) as GameObject;
                        break;
                }
                break;
            case (int)BLOCKTYPE.NUI:
                instance = MonoBehaviour.Instantiate(nui) as GameObject;
                fontInstance = MonoBehaviour.Instantiate(font) as GameObject;
                fontInstance.GetComponent<TextMesh>().text = option.ToString();
                break;
            case (int)BLOCKTYPE.WALL:
                //???
                break;
            case (int)BLOCKTYPE.EMPTY:
                //
                break;
            case (int)BLOCKTYPE.ITEM:


                switch (item)
                {
                    case (int)ITEMTYPE.COLORBOMBER_RED:
                        instance = MonoBehaviour.Instantiate(red) as GameObject;
                        break;
                    case (int)ITEMTYPE.COLORBOMBER_YELLOW:
                        instance = MonoBehaviour.Instantiate(yellow) as GameObject;
                        break;
                    case (int)ITEMTYPE.COLORBOMBER_GREEN:
                        instance = MonoBehaviour.Instantiate(green) as GameObject;
                        break;
                    case (int)ITEMTYPE.COLORBOMBER_BLUE:
                        instance = MonoBehaviour.Instantiate(blue) as GameObject;
                        break;
                    case (int)ITEMTYPE.COLORBOMBER_PURPLE:
                        instance = MonoBehaviour.Instantiate(purple) as GameObject;
                        break;
                    case (int)ITEMTYPE.KING:
                        instance = MonoBehaviour.Instantiate(king) as GameObject;
                        break;
                    case (int)ITEMTYPE.GARO:
                        instance = MonoBehaviour.Instantiate(garo) as GameObject;
                        break;
                    case (int)ITEMTYPE.SERO:
                        instance = MonoBehaviour.Instantiate(sero) as GameObject;
                        break;
                    default:
                        instance = MonoBehaviour.Instantiate(nui) as GameObject;
                        break;
                }
                /*
                block = instance.GetComponent<Block>();
                block.type = (int)BLOCKTYPE.ITEM;
                block.option = item;
                block.item = item;
                */
                // not
                break;
            default:
                break;
        }
        block = instance.GetComponent<Block>();
        block.type = type;
        block.option = option;
        block.item = item;
        if (instance != null && block != null)
        {
            instance.name = "block";
            instance.transform.parent = parent.transform;
            instance.transform.localPosition = new Vector3(originX + (float)x * interval, originY - (float)y * interval, 0f);
            block.correctY = instance.transform.localPosition.y;
            block.x = x;
            block.y = y;
            block.speed = 0f;
            blockArr[y, x] = block;
            if (block.y < inGameStartY)
            {
                instance.renderer.enabled = false;
                instance.collider.enabled = false;
            }

            if (fontInstance != null)
            {
                fontInstance.transform.parent = instance.transform;
                fontInstance.name = "health";
                fontInstance.transform.localPosition = new Vector3(0f, 0f, 0f);
            }
        }
        return block;
    }

    public void SetItemQTY(int i0,int i1,int i2,int i3,int i4)
    {
        itemQTY[0] = i0;
        itemQTY[1] = i1;
        itemQTY[2] = i2;
        itemQTY[3] = i3;
        itemQTY[4] = i4;
    }

    public int UseItem(int kind)
    {
        if (timerSwitch != true)
        {
            if( itemQTY[kind] < 1)
                return -1;

            if (kind == 0 && mission_turn != -1)
            {
                itemQTY[kind]--;
                TurnAdd(3);
                return itemQTY[kind];
            }
            if (kind == 1)
            {
                if(TimeSlip())
                {
                    itemQTY[kind]--;
                    return itemQTY[kind];
                }
            }
        }
        return -1;
    }

    public bool TimeSlip()
    {
        if (worldPoint == -1)
        {
            return false;
        }

        //TakeSnapShot(new IntPoint(-1, -1), 0f);

        int before = worldPoint;//snapShotList[worldPoint].beforeWorldPoint;
        score = snapShotList[before].point;
        chainColor = snapShotList[before].chainColor;
        chainCombo = snapShotList[before].chainCombo;
        comboMeter = snapShotList[before].comboMeter;




        GameObject.Find("score").GetComponent<UILabel>().text = "Score " + (score * 10).ToString();


        int count = 0;
        for (int i = 1; i < 6; i++)
        {
            for (int j = 0; j < missions[i].Count; j++)
            {

                missions[i][j].completed = snapShotList[before].missions[count].completed;
                missions[i][j].option = snapShotList[before].missions[count].option;

                if (i == 2)
                {
                    GameObject obj = GameObject.Find("LockOn" + missions[i][j].mission.optionArray[0].ToString() + "," + missions[i][j].mission.optionArray[1].ToString());
                    if (obj != null)
                    {
                        if (missions[i][j].completed == true)
                        {
                            obj.renderer.enabled = false;
                        }
                        else
                        {
                            obj.renderer.enabled = true;
                        }
                    }
                }
                else
                {
                    missions[i][j].text.GetComponent<UILabel>().text = GetMissionString(missions[i][j]);
                }
                count++;
            }
        }
        TurnAdd(1);
        count = 0;
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                if (snapShotList[before].snapshot[count] == null)
                {
                    if (blockArr[i, j] != null)
                    {
                        blockArr[i, j].PopBlock();
                        blockArr[i, j] = null;
                    }
                }
                else if (snapShotList[before].snapshot[count].type == (int)BLOCKTYPE.COLORED)
                {
                    if (blockArr[i, j] != null)
                    {
                        if (blockArr[i, j].type != (int)BLOCKTYPE.COLORED || blockArr[i, j].option != snapShotList[before].snapshot[count].option)
                        {
                            blockArr[i, j].PopBlock();
                            blockArr[i, j] = null;
                            CreateBlock(snapShotList[before].snapshot[count].type, snapShotList[before].snapshot[count].option, snapShotList[before].snapshot[count].item, j, i);
                        }
                    }
                    else
                    {
                        CreateBlock(snapShotList[before].snapshot[count].type, snapShotList[before].snapshot[count].option, snapShotList[before].snapshot[count].item, j, i);
                    }
                }
                else if (snapShotList[before].snapshot[count].type == (int)BLOCKTYPE.NUI)
                {
                    if (blockArr[i, j] != null)
                    {
                        if (blockArr[i, j].type != (int)BLOCKTYPE.NUI)
                        {
                            blockArr[i, j].PopBlock();
                            blockArr[i, j] = null;

                            CreateBlock(snapShotList[before].snapshot[count].type, snapShotList[before].snapshot[count].option, snapShotList[before].snapshot[count].item, j, i);
                        }
                        else
                        {
                            blockArr[i, j].option = snapShotList[before].snapshot[count].option;
                            blockArr[i, j].item = snapShotList[before].snapshot[count].item;
                            blockArr[i, j].gameObject.transform.FindChild("health").GetComponent<TextMesh>().text = blockArr[i, j].option.ToString();
                        }
                    }
                    else
                    {
                        CreateBlock(snapShotList[before].snapshot[count].type, snapShotList[before].snapshot[count].option, snapShotList[before].snapshot[count].item, j, i);
                    }
                }
                else if (snapShotList[before].snapshot[count].type == (int)BLOCKTYPE.ITEM)
                {
                    if (blockArr[i, j] != null)
                    {
                        if (blockArr[i, j].type != (int)BLOCKTYPE.ITEM || blockArr[i, j].option != snapShotList[before].snapshot[count].option || blockArr[i, j].item != snapShotList[before].snapshot[count].item)
                        {
                            blockArr[i, j].PopBlock();
                            blockArr[i, j] = null;

                            CreateBlock(snapShotList[before].snapshot[count].type, snapShotList[before].snapshot[count].option, snapShotList[before].snapshot[count].item, j, i);
                        }
                    }
                    else
                    {
                        CreateBlock(snapShotList[before].snapshot[count].type, snapShotList[before].snapshot[count].option, snapShotList[before].snapshot[count].item, j, i);
                    }
                }
                else
                {
                    //Debug.Log(snapShotList[before].snapshot[count].type.ToString());
                    //Debug.LogError("Error. Game is Dead, mismatch");
                }
                count++;
            }
        }

        StarBarCalculate();

        worldPoint = snapShotList[worldPoint].beforeWorldPoint;
        return true;
    }

    private void StarBarCalculate()
    {
        float star = CalculateStarScore(score * 10);
        star = star / 3f;
        if (star > 1f)
            star = 1f;
        //scoreFrame.GetComponent<UITexture>().fillAmount = star;
        scoreFrameRealValue = star;
        scoreFrameFlag = true;
        if (missions[5].Count != 0)
        {
            int tempScore = (score * 10) + GetBonusScore(missions[5][0].mission.optionArray[0], play_turn + bonus_turn + 1);
            star = CalculateStarScore(tempScore);
            star = star / 3f;
            if (star > 1f)
                star = 1f;
            //bonusFrame.GetComponent<UITexture>().fillAmount = star;
            bonusFrameRealValue = star;
            bonusFrameFlag = true;
        }
    }

    private void StarBarRefresh()
    {
        if(scoreFrameFlag == true)
        {
            if (Mathf.Abs(scoreFrameRealValue - scoreFrameValue) < 0.02f)
            {
                scoreFrameValue = scoreFrameRealValue;
                scoreFrameFlag = false;
            }
            scoreFrameValue += (scoreFrameRealValue - scoreFrameValue) * 0.15f;
            scoreFrame.GetComponent<UITexture>().fillAmount = scoreFrameValue;
        }

        if(bonusFrameFlag == true)
        {
            if (Mathf.Abs(bonusFrameRealValue - bonusFrameValue) < 0.02f)
            {
                bonusFrameValue = bonusFrameRealValue;
                bonusFrameFlag = false;
            }
            bonusFrameValue += (bonusFrameRealValue - bonusFrameValue) * 0.15f;
            bonusFrame.GetComponent<UITexture>().fillAmount = bonusFrameValue;
        }
    }

    private float CalculateStarScore(int value)
    {
        int starEarned = 0;
        float result;
        if (value >= grade_score[2])
        {
            starEarned = 3;
        }
        else if (value >= grade_score[1])
        {
            starEarned = 2;
        }
        else if (value >= grade_score[0])
        {
            starEarned = 1;
        }

        if (starEarned == 0)
        {
            result = (float)(value) / grade_score[0];
        }
        else if (starEarned == 1)
        {
            result = (float)(value - grade_score[0]) / (grade_score[1] - grade_score[0]);
            result += 1f;
        }
        else
        {
            result = (float)(value - grade_score[1]) / (grade_score[2] - grade_score[1]);
            result += 2f;
        }
        return result;
    }

    public void Init()
    {
        StageReader.Stage reader = StageReader.GetInstance().GetStage();
        red = Resources.Load("Image/red") as GameObject;
        yellow = Resources.Load("Image/yellow") as GameObject;
        green = Resources.Load("Image/green") as GameObject;
        blue = Resources.Load("Image/blue") as GameObject;
        purple = Resources.Load("Image/purple") as GameObject;
        nui = Resources.Load("Image/nui") as GameObject;
        wall = Resources.Load("Image/wall") as GameObject;
        font = Resources.Load("font") as GameObject;
        mark = Resources.Load("Image/lockon") as GameObject;
        garo = Resources.Load("Image/garoItem") as GameObject;
        sero = Resources.Load("Image/seroItem") as GameObject;
        king = Resources.Load("Image/kingItem") as GameObject;


        noMore = false;
        //
        isItemStage = true;
        //

        int temp;
        itemQTY = new int[5];
        for (int i = 0; i < 5; i++ )
        {
            int.TryParse(SecurityIO.GetString("item" + i.ToString()) , out temp);
            itemQTY[i] = temp;
        }

            scoreFrameValue = 0f;
        scoreFrameRealValue = 0f;
        bonusFrameValue = 0f;
        bonusFrameRealValue = 0f;

        redWhiteTimer = 0f;
        redWhiteFlag = false;
        redWhiteColor = false;

        kindRedWhiteRemainTime = 0;
        kindRedWhiteFlag = false;
        kindRedWhiteColor = false;
        kindRedWhiteProgress = 0f;

        bonusFrameFlag = false;
        scoreFrameFlag = false;

        limitFlag = false;
        limitScale = 1f;
        generateFlag = new bool[5];
        grade_score = new int[3];
        generateKind = 0;
        generateList = new System.Collections.Generic.List<BlockCompact>();
        blockArr = new Block[reader.height, reader.width];
        isInfinite = reader.infinite;
        worldPoint = -1;
        mission_turn = -1;
        chainCombo = 0;
        chainColor = -1;
        comboMeter = 0;

        grade_score[0] = reader.grade[0];
        grade_score[1] = reader.grade[1];
        grade_score[2] = reader.grade[2];

        for (int i = 0; i < 5; i++)
        {
            if (reader.colorFlag[i] == true)
            {
                generateFlag[i] = true;
                generateKind++;
            }
        }

        generateColor = new int[generateKind];
        temp = 0;
        for (int i = 0; i < 5; i++)
        {
            if (generateFlag[i] == true)
            {
                generateColor[temp] = i;
                temp++;
            }
        }

        width = reader.width;
        height = reader.height;

        if (height > 9)
        {
            inGameStartY = height - 9;
        }
        else
        {
            inGameStartY = 0;
        }

        snapShotList = new System.Collections.Generic.List<SnapShot>();

        GameObject parent = new GameObject("Blocks");
        blockParent = parent;
        parent.transform.localPosition = new Vector3(0f, 0.17f, 0f);
        parent.transform.localScale = new Vector3(0.85f, 0.85f, 1f);
        originX = interval * -0.5f * (width - 1); // 1 = 0, 2 = 0.375 = 3 = 0.75
        originY = -0.4f + interval * 0.5f * (height + inGameStartY - 1);


        limitKind = GameObject.Find("limitKind");

        missions = new System.Collections.Generic.List<MissionAdapter>[7];
        for (int i = 0; i < missions.Length; i++)
        {
            missions[i] = new System.Collections.Generic.List<MissionAdapter>();
        }
        int ea = 0;
        bool limit = false;
        foreach (Mission mission in reader.missionArray)
        {
            MissionAdapter adapter = new MissionAdapter();
            adapter.mission = mission;
            if (mission.type == (int)MISSION.MISSION_ML || mission.type == (int)MISSION.MISSION_TL)
            {
                if (mission.type == (int)MISSION.MISSION_ML)
                {
                    limitKind.GetComponent<UILabel>().text = "Move";
                    mission_turn = mission.optionArray[0];
                }
                else
                {
                    limitKind.GetComponent<UILabel>().text = "Time";
                    kindRedWhiteFlag = true;
                    kindRedWhiteRemainTime = 5;
                }
                adapter.completed = true;
                adapter.option = mission.optionArray[0];
                adapter.text = GameObject.Find("limit");
                limitObject = adapter.text;
                //adapter.text.transform.localPosition = new Vector3(0.15f, 0.94f, 0f);
                adapter.text.GetComponent<UILabel>().text = GetMissionString(adapter);
                missions[mission.type].Add(adapter);
                limit = true;
            }
            else
            {
                if (mission.type == (int)MISSION.MISSION_DB)
                {
                    GameObject obj = MonoBehaviour.Instantiate(mark) as GameObject;
                    obj.name = "LockOn" + adapter.mission.optionArray[0].ToString() + "," + adapter.mission.optionArray[1].ToString();
                    obj.transform.parent = parent.transform;
                    obj.transform.localPosition = new Vector3(originX + (float)(adapter.mission.optionArray[0]) * interval, originY - (float)(adapter.mission.optionArray[1]) * interval, 0f);
                }
                adapter.completed = false;
                adapter.option = 0;
                if (ea == 0)
                {
                    adapter.text = GameObject.Find("firstMission");
                }
                else if (ea == 1)
                {
                    adapter.text = GameObject.Find("secondMission");
                }
                else
                {
                    Debug.LogError("mission overflow!");
                }
                //adapter.text.transform.localPosition = new Vector3(0.5f, 0.1f + 0.05f * ea);
                adapter.text.GetComponent<UILabel>().text = GetMissionString(adapter);
                ea++;
                missions[mission.type].Add(adapter);
            }
        }
        if (limit == false)
        {
            limitKind.GetComponent<UILabel>().text = "Time";
            GameObject.Find("limit").GetComponent<UILabel>().text = "∞";
        }
        if (ea == 1)
        {
            GameObject.Find("firstMission").transform.localPosition = new Vector3(0f, 395f, 0f);
        }
        GameObject.Find("score").GetComponent<UILabel>().text = "Score " + (0).ToString();

        StarBarCalculate();

        int nuiCount = 0;
        int itemCount = 0;
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                int value = reader.blockArray[i * width + j];
                GameObject instance = null;
                GameObject fontInstance = null;
                Block block;
                switch (value)
                {
                    case (int)BLOCK.BLOCK_RED:
                        instance = MonoBehaviour.Instantiate(red) as GameObject;
                        block = instance.GetComponent<Block>();
                        block.type = (int)BLOCKTYPE.COLORED;
                        block.option = (int)BLOCK.BLOCK_RED;
                        break;
                    case (int)BLOCK.BLOCK_YEL:
                        instance = MonoBehaviour.Instantiate(yellow) as GameObject;
                        block = instance.GetComponent<Block>();
                        block.type = (int)BLOCKTYPE.COLORED;
                        block.option = (int)BLOCK.BLOCK_YEL;
                        break;
                    case (int)BLOCK.BLOCK_GRN:
                        instance = MonoBehaviour.Instantiate(green) as GameObject;
                        block = instance.GetComponent<Block>();
                        block.type = (int)BLOCKTYPE.COLORED;
                        block.option = (int)BLOCK.BLOCK_GRN;
                        break;
                    case (int)BLOCK.BLOCK_BLU:
                        instance = MonoBehaviour.Instantiate(blue) as GameObject;
                        block = instance.GetComponent<Block>();
                        block.type = (int)BLOCKTYPE.COLORED;
                        block.option = (int)BLOCK.BLOCK_BLU;
                        break;
                    case (int)BLOCK.BLOCK_PURPLE:
                        instance = MonoBehaviour.Instantiate(purple) as GameObject;
                        block = instance.GetComponent<Block>();
                        block.type = (int)BLOCKTYPE.COLORED;
                        block.option = (int)BLOCK.BLOCK_PURPLE;
                        break;
                    case (int)BLOCK.BLOCK_NUI:
                        instance = MonoBehaviour.Instantiate(nui) as GameObject;
                        block = instance.GetComponent<Block>();
                        block.type = (int)BLOCKTYPE.NUI;
                        block.option = reader.nuiArray[nuiCount];
                        nuiCount++;

                        fontInstance = MonoBehaviour.Instantiate(font) as GameObject;
                        fontInstance.GetComponent<TextMesh>().text = block.option.ToString();

                        break;
                    case (int)BLOCK.BLOCK_WALL:
                        instance = MonoBehaviour.Instantiate(wall) as GameObject;
                        block = instance.GetComponent<Block>();
                        block.type = (int)BLOCKTYPE.WALL;
                        block.option = (int)BLOCK.BLOCK_WALL;
                        break;
                    case (int)BLOCK.BLOCK_EMPTY:
                        instance = null;
                        block = null;
                        blockArr[i, j] = null;
                        break;
                    case (int)BLOCK.BLOCK_RANDOM:
                        int color = Random.Range(0, generateKind);
                        switch (generateColor[color])
                        {
                            case 0:
                                instance = MonoBehaviour.Instantiate(red) as GameObject;
                                break;
                            case 1:
                                instance = MonoBehaviour.Instantiate(yellow) as GameObject;
                                break;
                            case 2:
                                instance = MonoBehaviour.Instantiate(green) as GameObject;
                                break;
                            case 3:
                                instance = MonoBehaviour.Instantiate(blue) as GameObject;
                                break;
                            case 4:
                                instance = MonoBehaviour.Instantiate(purple) as GameObject;
                                break;
                        }
                        block = instance.GetComponent<Block>();
                        block.type = (int)BLOCKTYPE.COLORED;
                        block.option = generateColor[color];

                        break;
                    case (int)BLOCK.BLOCK_ITEM:
                        int itemKind = reader.itemArray[itemCount];

                        switch (itemKind)
                        {
                            case (int)ITEMTYPE.COLORBOMBER_RED:
                                instance = MonoBehaviour.Instantiate(red) as GameObject;
                                break;
                            case (int)ITEMTYPE.COLORBOMBER_YELLOW:
                                instance = MonoBehaviour.Instantiate(yellow) as GameObject;
                                break;
                            case (int)ITEMTYPE.COLORBOMBER_GREEN:
                                instance = MonoBehaviour.Instantiate(green) as GameObject;
                                break;
                            case (int)ITEMTYPE.COLORBOMBER_BLUE:
                                instance = MonoBehaviour.Instantiate(blue) as GameObject;
                                break;
                            case (int)ITEMTYPE.COLORBOMBER_PURPLE:
                                instance = MonoBehaviour.Instantiate(purple) as GameObject;
                                break;
                            case (int)ITEMTYPE.GARO:
                                instance = MonoBehaviour.Instantiate(garo) as GameObject;
                                break;
                            case (int)ITEMTYPE.SERO:
                                instance = MonoBehaviour.Instantiate(sero) as GameObject;
                                break;
                            case (int)ITEMTYPE.KING:
                                instance = MonoBehaviour.Instantiate(king) as GameObject;
                                break;
                            default:
                                instance = MonoBehaviour.Instantiate(nui) as GameObject;
                                break;
                        }
                        block = instance.GetComponent<Block>();
                        block.type = (int)BLOCKTYPE.ITEM;
                        block.option = itemKind;
                        block.item = itemKind;
                        itemCount++;
                        break;
                    default:
                        instance = null;
                        block = null;
                        blockArr[i, j] = null;
                        break;
                }
                if (instance != null && block != null)
                {
                    instance.name = "block";
                    instance.transform.parent = parent.transform;
                    instance.transform.localPosition = new Vector3(originX + (float)j * interval, originY - (float)i * interval, 0f);
                    block.correctY = instance.transform.localPosition.y;
                    block.x = j;
                    block.y = i;
                    block.speed = 0f;
                    blockArr[i, j] = block;
                    if (block.y < inGameStartY)
                    {
                        instance.renderer.enabled = false;
                        instance.collider.enabled = false;
                    }

                    if (fontInstance != null)
                    {
                        fontInstance.transform.parent = instance.transform;
                        fontInstance.name = "health";
                        fontInstance.transform.localPosition = new Vector3(0f, 0f, 0f);
                    }
                }
            }
        }
        //parent.transform.localScale = new Vector3(0.85f, 0.85f, 1f);
        score = 0;
        play_turn = 0;
        play_time = 0f;
        timerSwitch = false;
        cleared = false;
        gameWasOver = false;
    }

    public int TryTouch(int x, int y)
    {
        if (gameWasOver == true)
            return -1;
        if (gamePaused == true)
            return -1;

        int result = 0;

        if (blockArr[y, x].type == (int)BLOCKTYPE.COLORED)
        {
            #region COLORED_PROCESS
            ArrayList popList = PopTest(x, y);
            if (popList.Count >= 2)
            {
                //result = 1;
                TakeSnapShot(new IntPoint(x, y), 0f);
                play_turn += 1;

                #region CHAIN_PROCESS
                if (chainColor != blockArr[y, x].option)
                    chainCombo = 0;
                chainColor = blockArr[y, x].option;
                result = chainCombo % 4;
                chainCombo++;
                result++;

                #endregion

                #region INIT_PROCESS
                bool[,] destroyArray = new bool[height, width];
                foreach (IntPoint block in popList)
                {
                    destroyArray[block.y, block.x] = true;
                }
                #endregion

                #region NUI_PROCESS
                foreach (IntPoint block in popList)
                {
                    if (block.x != 0)
                    {
                        if (blockArr[block.y, block.x - 1] != null && blockArr[block.y, block.x - 1].type == (int)BLOCKTYPE.NUI && blockArr[block.y, block.x - 1].option > 0)
                        {
                            //Debug.Log(block.x.ToString()+","+block.y.ToString()+"-"+blockArr[block.y, block.x - 1].option);
                            blockArr[block.y, block.x - 1].option--;
                            blockArr[block.y, block.x - 1].UpdateHealth();
                            if (blockArr[block.y, block.x - 1].option == 0)
                                destroyArray[block.y, block.x - 1] = true;
                        }
                    }
                    if (block.y > inGameStartY)// != 0)
                    {
                        if (blockArr[block.y - 1, block.x] != null && blockArr[block.y - 1, block.x].type == (int)BLOCKTYPE.NUI && blockArr[block.y - 1, block.x].option > 0)
                        {
                            //Debug.Log(block.x.ToString() + "," + block.y.ToString() + "-" + blockArr[block.y-1, block.x].option);
                            blockArr[block.y - 1, block.x].option--;
                            blockArr[block.y - 1, block.x].UpdateHealth();
                            if (blockArr[block.y - 1, block.x].option == 0)
                                destroyArray[block.y - 1, block.x] = true;
                        }
                    }
                    if (block.x + 1 != width)
                    {
                        if (blockArr[block.y, block.x + 1] != null && blockArr[block.y, block.x + 1].type == (int)BLOCKTYPE.NUI && blockArr[block.y, block.x + 1].option > 0)
                        {
                            //Debug.Log(block.x.ToString() + "," + block.y.ToString() + "-" + blockArr[block.y, block.x + 1].option);
                            blockArr[block.y, block.x + 1].option--;
                            blockArr[block.y, block.x + 1].UpdateHealth();
                            if (blockArr[block.y, block.x + 1].option == 0)
                                destroyArray[block.y, block.x + 1] = true;
                        }
                    }
                    if (block.y + 1 != height)
                    {
                        if (blockArr[block.y + 1, block.x] != null && blockArr[block.y + 1, block.x].type == (int)BLOCKTYPE.NUI && blockArr[block.y + 1, block.x].option > 0)
                        {
                            //Debug.Log(block.x.ToString() + "," + block.y.ToString() + "-" + blockArr[block.y+1, block.x].option);
                            blockArr[block.y + 1, block.x].option--;
                            blockArr[block.y + 1, block.x].UpdateHealth();
                            if (blockArr[block.y + 1, block.x].option == 0)
                                destroyArray[block.y + 1, block.x] = true;
                        }
                    }
                }
                #endregion

                #region ITEM_PROCESS
                /*
                foreach (IntPoint block in popList)
                {
                    if (blockArr[block.y, block.x].type == (int)BLOCKTYPE.COLORED)
                    {
                        switch ((ITEMTYPE)blockArr[block.y, block.x].item)
                        {
                            case ITEMTYPE.NOITEM:
                                break;
                            case ITEMTYPE.GARO:
                                for (int i = 0; i < width; i++)
                                {
                                    destroyArray[block.y, block.x] = true;
                                }
                                break;
                            case ITEMTYPE.SERO:
                                for (int i = inGameStartY; i < height; i++)
                                {
                                    destroyArray[block.y, block.x] = true;
                                }
                                break;
                        }
                    }
                }*/
                #endregion

                #region CHECK_PROCESS
                for (int i = inGameStartY; i < height; i++)
                {
                    for (int j = 0; j < width; j++)
                    {
                        if (destroyArray[i, j] == true)
                        {
                            if (blockArr[i, j] == null || blockArr[i, j].type == (int)BLOCKTYPE.EMPTY || blockArr[i, j].type == (int)BLOCKTYPE.WALL)
                            {
                                destroyArray[i, j] = false;
                            }
                        }
                    }
                }
                #endregion

                #region SCORE_PROCESS
                score += popList.Count * popList.Count * chainCombo;
                GameObject.Find("score").GetComponent<UILabel>().text = "Score " + (score * 10).ToString();
                StarBarCalculate();
                #endregion

                #region MISSION_PROCESS
                int[] colorCount = new int[6];
                for (int i = inGameStartY; i < height; i++)
                {
                    for (int j = 0; j < width; j++)
                    {
                        if (destroyArray[i, j] == true)
                        {
                            switch ((BLOCKTYPE)blockArr[i, j].type)
                            {
                                case BLOCKTYPE.COLORED:
                                    colorCount[blockArr[i, j].option]++;
                                    break;
                                case BLOCKTYPE.NUI:
                                    colorCount[(int)BLOCK.BLOCK_NUI]++;
                                    break;
                            }
                        }
                    }
                }

                foreach (MissionAdapter adapter in missions[1]) //clear colored
                {
                    if (adapter.completed == true)
                        continue;
                    if (colorCount[adapter.mission.optionArray[0]] != 0)
                    {
                        adapter.option += colorCount[adapter.mission.optionArray[0]];
                        if (adapter.option >= adapter.mission.optionArray[1])
                        {
                            // #REPLAY ISSUE
                            adapter.option = adapter.mission.optionArray[1];
                            adapter.completed = true;
                        }
                        adapter.text.GetComponent<UILabel>().text = GetMissionString(adapter);
                    }
                }

                foreach (MissionAdapter adapter in missions[2])
                {
                    if (adapter.completed == true)
                        continue;
                    if (destroyArray[adapter.mission.optionArray[1], adapter.mission.optionArray[0]] == true)
                    {
                        GameObject obj = GameObject.Find("LockOn" + adapter.mission.optionArray[0].ToString() + "," + adapter.mission.optionArray[1].ToString());
                        if (obj != null)
                        {
                            // #REPLAY ISSUE
                            obj.renderer.enabled = false;
                        }
                        adapter.completed = true;
                    }
                }

                foreach (MissionAdapter adapter in missions[3]) // turn limit
                {
                    if (timerSwitch == true)
                        break;
                    adapter.option = adapter.mission.optionArray[0] - play_turn;
                    adapter.text.GetComponent<UILabel>().text = GetMissionString(adapter);
                    if (adapter.option <= 5)
                    {
                        LimitScaleApply();
                        SetRedWhite();
                    }
                    if (play_turn >= adapter.mission.optionArray[0])
                    {
                        // #REPLAY ISSUE
                        gameWasOver = true;
                        timerSwitch = true;
                        timer = play_time + 1.5f;
                    }
                }

                foreach (MissionAdapter adapter in missions[4]) // one time, one color clear
                {
                    if (adapter.completed == true)
                        continue;
                    if (colorCount[adapter.mission.optionArray[0]] >= adapter.mission.optionArray[1])
                    {
                        adapter.option = adapter.mission.optionArray[1];
                        adapter.completed = true;
                    }
                    else if (colorCount[adapter.mission.optionArray[0]] > adapter.option)
                    {
                        // #REPLAY ISSUE
                        adapter.option = colorCount[adapter.mission.optionArray[0]];
                    }
                    adapter.text.GetComponent<UILabel>().text = GetMissionString(adapter);
                }

                foreach (MissionAdapter adapter in missions[5]) // need score
                {
                    if (adapter.completed == true)
                        continue;
                    // #REPLAY ISSUE
                    adapter.option = score * 10;
                    adapter.text.GetComponent<UILabel>().text = GetMissionString(adapter);
                    if (score * 10 >= adapter.mission.optionArray[0])
                    {
                        adapter.completed = true;
                    }
                }
                #endregion

                #region CLEAR_PROCESS
                for (int i = inGameStartY; i < height; i++)
                {
                    for (int j = 0; j < width; j++)
                    {
                        if (destroyArray[i, j] == true)
                        {
                            comboMeter++;
                            blockArr[i, j].PopBlock();
                            blockArr[i, j] = null;
                        }
                    }
                }
                #endregion

                #region ITEM_GENERATE_PROCESS

                if( chainCombo % 4 == 0)
                {
                    blockArr[y,x] = CreateBlock((int)BLOCKTYPE.ITEM, (int)ITEMTYPE.KING, (int)ITEMTYPE.KING, x, y);
                }
                else if( comboMeter >= 100)
                {
                    comboMeter -= 100;
                    if( Random.Range(0,2) == 0)
                    {
                        blockArr[y, x] = CreateBlock((int)BLOCKTYPE.ITEM, (int)ITEMTYPE.GARO, (int)ITEMTYPE.GARO, x, y);
                    }
                    else
                    {
                        blockArr[y, x] = CreateBlock((int)BLOCKTYPE.ITEM, (int)ITEMTYPE.SERO, (int)ITEMTYPE.SERO, x, y);
                    }
                }


                #endregion

                #region DROP_PROCESS
                for (int i = height - 1; i >= 0; i--)
                {
                    for (int j = 0; j < width; j++)
                    {

                        if (blockArr[i, j] == null)
                        {
                            for (int k = i - 1; k >= 0; k--)
                            {
                                if (blockArr[k, j] != null)
                                {
                                    if (blockArr[k, j].type == (int)BLOCKTYPE.COLORED || blockArr[k, j].type == (int)BLOCKTYPE.NUI || blockArr[k, j].type == (int)BLOCKTYPE.ITEM)
                                    {
                                        blockArr[i, j] = blockArr[k, j];
                                        blockArr[k, j] = null;
                                        blockArr[i, j].y = i;
                                        blockArr[i, j].correctY = originY - (float)i * interval;
                                        blockArr[i, j].DropBlock();
                                        if (inGameStartY <= i)
                                        {
                                            blockArr[i, j].gameObject.renderer.enabled = true;
                                            blockArr[i, j].gameObject.collider.enabled = true;
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                #region GENERATE_PROCESS
                if (isInfinite == true)
                {
                    for (int j = 0; j < width; j++)
                    {
                        bool upFlag = false;
                        float prevPosition = 0f;
                        for (int i = height - 1; i >= inGameStartY; i--)
                        {
                            if (blockArr[i, j] == null)
                            {
                                int color = Random.Range(0, generateKind);
                                GameObject instance = null;
                                switch (generateColor[color])
                                {
                                    case 0:
                                        instance = Resources.Load("Image/red") as GameObject;
                                        break;
                                    case 1:
                                        instance = Resources.Load("Image/yellow") as GameObject;
                                        break;
                                    case 2:
                                        instance = Resources.Load("Image/green") as GameObject;
                                        break;
                                    case 3:
                                        instance = Resources.Load("Image/blue") as GameObject;
                                        break;
                                    case 4:
                                        instance = Resources.Load("Image/purple") as GameObject;
                                        break;
                                }
                                instance = MonoBehaviour.Instantiate(instance) as GameObject;
                                Block temp = instance.GetComponent<Block>();
                                //float genY = blockArr[1, block.x].gameObject.transform.position.y + 0.75f;
                                instance.name = "block";
                                instance.transform.parent = blockParent.transform;
                                instance.transform.localPosition = new Vector3(originX + (float)j * interval, 0f, 0f);
                                if (upFlag == false)
                                {
                                    instance.transform.localPosition += new Vector3(0f, originY + interval, 0f);
                                    prevPosition = originY + interval;
                                    upFlag = true;
                                }
                                else
                                {
                                    instance.transform.localPosition += new Vector3(0f, prevPosition + interval, 0f);
                                    prevPosition += interval;
                                }
                                temp.correctY = originY - (float)i * interval;
                                temp.x = j;
                                temp.y = i;
                                temp.option = generateColor[color];
                                temp.type = (int)BLOCKTYPE.COLORED;
                                temp.speed = 0f;
                                temp.item = 0;
                                blockArr[i, j] = temp;
                                /*
                                if (i < inGameStartY)
                                {
                                    temp.renderer.enabled = false;
                                    temp.collider.enabled = false;
                                }*/
                                temp.DropBlock();
                            }
                        }
                    }
                }
                else
                {
                    bool allClear = true;
                    for (int i = 0; i < height; i++)
                    {
                        for (int j = 0; j < width; j++)
                        {
                            if (blockArr[i, j] != null)
                            {
                                if (blockArr[i, j].type == (int)BLOCKTYPE.COLORED || blockArr[i, j].type == (int)BLOCKTYPE.ITEM || blockArr[i, j].type == (int)BLOCKTYPE.NUI)
                                {
                                    allClear = false;
                                }
                            }
                        }
                    }
                    if (allClear == true)
                    {
                        foreach (MissionAdapter adapter in missions[0])
                        {
                            if (adapter.completed == true)
                                continue;
                            adapter.completed = true;
                        }
                    }
                }
                #endregion

            }
            #endregion
        }
        else if (blockArr[y, x].type == (int)BLOCKTYPE.NUI)
        {

        }
        else if (blockArr[y, x].type == (int)BLOCKTYPE.WALL)
        {

        }
        else if (blockArr[y, x].type == (int)BLOCKTYPE.EMPTY)
        {

        }
        else if (blockArr[y, x].type == (int)BLOCKTYPE.ITEM)
        {
            result = 1;
            TakeSnapShot(new IntPoint(x, y), 0f);
            play_turn += 1;

            #region CHAIN_PROCESS
            chainCombo = 0;
            chainColor = -1;
            #endregion

            #region INIT_PROCESS
            bool[,] destroyArray = new bool[height, width];
            destroyArray[y, x] = true;
            #endregion

            #region ITEM_PROCESS

            System.Collections.Generic.Queue<IntPoint> itemQueue = new System.Collections.Generic.Queue<IntPoint>();
            itemQueue.Enqueue(new IntPoint(x, y));

            while (itemQueue.Count > 0)
            {
                IntPoint item = itemQueue.Dequeue();
                if (blockArr[item.y, item.x].item == (int)ITEMTYPE.USED)
                    continue;
                ArrayList popList = ItemPop(item.x, item.y, blockArr[item.y, item.x].item);
                blockArr[item.y, item.x].item = (int)ITEMTYPE.USED;
                foreach (IntPoint block in popList)
                {
                    if (blockArr[block.y, block.x].type == (int)BLOCKTYPE.COLORED)
                    {
                        destroyArray[block.y, block.x] = true;
                    }
                    else if (blockArr[block.y, block.x].type == (int)BLOCKTYPE.NUI)
                    {
                        if (blockArr[block.y, block.x].option > 0)
                        {
                            blockArr[block.y, block.x].option--;
                            blockArr[block.y, block.x].UpdateHealth();
                            if (blockArr[block.y, block.x].option == 0)
                                destroyArray[block.y, block.x] = true;
                        }
                    }
                    else if (blockArr[block.y, block.x].type == (int)BLOCKTYPE.ITEM)
                    {
                        if (blockArr[block.y, block.x].item != (int)ITEMTYPE.USED)
                        {
                            itemQueue.Enqueue(new IntPoint(block.x, block.y));
                            destroyArray[block.y, block.x] = true;
                        }
                    }
                }
            }
            #endregion

            #region CHECK_PROCESS
            for (int i = inGameStartY; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (destroyArray[i, j] == true)
                    {
                        if (blockArr[i, j] == null || blockArr[i, j].type == (int)BLOCKTYPE.EMPTY || blockArr[i, j].type == (int)BLOCKTYPE.WALL)
                        {
                            destroyArray[i, j] = false;
                        }
                    }
                }
            }
            #endregion

            #region SCORE_PROCESS

            int count = 0;
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (destroyArray[i, j] == true)
                        count++;
                }
            }

            score += count * count;
            GameObject.Find("score").GetComponent<UILabel>().text = "Score " + (score * 10).ToString();
            StarBarCalculate();
            #endregion

            #region MISSION_PROCESS
            int[] colorCount = new int[6];
            for (int i = inGameStartY; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (destroyArray[i, j] == true)
                    {
                        switch ((BLOCKTYPE)blockArr[i, j].type)
                        {
                            case BLOCKTYPE.COLORED:
                                colorCount[blockArr[i, j].option]++;
                                break;
                            case BLOCKTYPE.NUI:
                                colorCount[(int)BLOCK.BLOCK_NUI]++;
                                break;
                        }
                    }
                }
            }

            foreach (MissionAdapter adapter in missions[1]) //clear colored
            {
                if (adapter.completed == true)
                    continue;
                if (colorCount[adapter.mission.optionArray[0]] != 0)
                {
                    adapter.option += colorCount[adapter.mission.optionArray[0]];
                    if (adapter.option >= adapter.mission.optionArray[1])
                    {
                        // #REPLAY ISSUE
                        adapter.option = adapter.mission.optionArray[1];
                        adapter.completed = true;
                    }
                    adapter.text.GetComponent<UILabel>().text = GetMissionString(adapter);
                }
            }

            foreach (MissionAdapter adapter in missions[2])
            {
                if (adapter.completed == true)
                    continue;
                if (destroyArray[adapter.mission.optionArray[1], adapter.mission.optionArray[0]] == true)
                {
                    GameObject obj = GameObject.Find("LockOn" + adapter.mission.optionArray[0].ToString() + "," + adapter.mission.optionArray[1].ToString());
                    if (obj != null)
                    {
                        // #REPLAY ISSUE
                        obj.renderer.enabled = false;
                    }
                    adapter.completed = true;
                }
            }

            foreach (MissionAdapter adapter in missions[3]) // turn limit
            {
                if (timerSwitch == true)
                    break;
                adapter.option = adapter.mission.optionArray[0] - play_turn;
                adapter.text.GetComponent<UILabel>().text = GetMissionString(adapter);
                if (adapter.option <= 5)
                {
                    LimitScaleApply();
                    SetRedWhite();
                }
                if (play_turn >= adapter.mission.optionArray[0])
                {
                    // #REPLAY ISSUE
                    gameWasOver = true;
                    timerSwitch = true;
                    timer = play_time + 1.5f;
                }
            }

            foreach (MissionAdapter adapter in missions[4]) // one time, one color clear
            {
                if (adapter.completed == true)
                    continue;
                if (colorCount[adapter.mission.optionArray[0]] >= adapter.mission.optionArray[1])
                {
                    adapter.option = adapter.mission.optionArray[1];
                    adapter.completed = true;
                }
                else if (colorCount[adapter.mission.optionArray[0]] > adapter.option)
                {
                    // #REPLAY ISSUE
                    adapter.option = colorCount[adapter.mission.optionArray[0]];
                }
                adapter.text.GetComponent<UILabel>().text = GetMissionString(adapter);
            }

            foreach (MissionAdapter adapter in missions[5]) // need score
            {
                if (adapter.completed == true)
                    continue;
                // #REPLAY ISSUE
                adapter.option = score * 10;
                adapter.text.GetComponent<UILabel>().text = GetMissionString(adapter);
                if (score * 10 >= adapter.mission.optionArray[0])
                {
                    adapter.completed = true;
                }
            }
            #endregion

            #region CLEAR_PROCESS
            for (int i = inGameStartY; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (destroyArray[i, j] == true)
                    {
                        comboMeter++;
                        blockArr[i, j].PopBlock();
                        blockArr[i, j] = null;
                    }
                }
            }
            #endregion


            #region ITEM_GENERATE_PROCESS
            if (comboMeter >= 100)
            {
                comboMeter -= 100;
                if (Random.Range(0, 2) == 0)
                {
                    blockArr[y, x] = CreateBlock((int)BLOCKTYPE.ITEM, (int)ITEMTYPE.GARO, (int)ITEMTYPE.GARO, x, y);
                }
                else
                {
                    blockArr[y, x] = CreateBlock((int)BLOCKTYPE.ITEM, (int)ITEMTYPE.SERO, (int)ITEMTYPE.SERO, x, y);
                }
            }
            #endregion

            #region DROP_PROCESS
            for (int i = height - 1; i >= 0; i--)
            {
                for (int j = 0; j < width; j++)
                {

                    if (blockArr[i, j] == null)
                    {
                        for (int k = i - 1; k >= 0; k--)
                        {
                            if (blockArr[k, j] != null)
                            {
                                if (blockArr[k, j].type == (int)BLOCKTYPE.COLORED || blockArr[k, j].type == (int)BLOCKTYPE.NUI || blockArr[k, j].type == (int)BLOCKTYPE.ITEM)
                                {
                                    blockArr[i, j] = blockArr[k, j];
                                    blockArr[k, j] = null;
                                    blockArr[i, j].y = i;
                                    blockArr[i, j].correctY = originY - (float)i * interval;
                                    blockArr[i, j].DropBlock();
                                    if (inGameStartY <= i)
                                    {
                                        blockArr[i, j].gameObject.renderer.enabled = true;
                                        blockArr[i, j].gameObject.collider.enabled = true;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            #region GENERATE_PROCESS
            if (isInfinite == true)
            {
                for (int j = 0; j < width; j++)
                {
                    bool upFlag = false;
                    float prevPosition = 0f;
                    for (int i = height - 1; i >= inGameStartY; i--)
                    {
                        if (blockArr[i, j] == null)
                        {
                            int color = Random.Range(0, generateKind);
                            GameObject instance = null;
                            switch (generateColor[color])
                            {
                                case 0:
                                    instance = Resources.Load("Image/red") as GameObject;
                                    break;
                                case 1:
                                    instance = Resources.Load("Image/yellow") as GameObject;
                                    break;
                                case 2:
                                    instance = Resources.Load("Image/green") as GameObject;
                                    break;
                                case 3:
                                    instance = Resources.Load("Image/blue") as GameObject;
                                    break;
                                case 4:
                                    instance = Resources.Load("Image/purple") as GameObject;
                                    break;
                            }
                            instance = MonoBehaviour.Instantiate(instance) as GameObject;
                            Block temp = instance.GetComponent<Block>();
                            //float genY = blockArr[1, block.x].gameObject.transform.position.y + 0.75f;
                            instance.name = "block";
                            instance.transform.parent = blockParent.transform;
                            instance.transform.localPosition = new Vector3(originX + (float)j * interval, 0f, 0f);
                            if (upFlag == false)
                            {
                                instance.transform.localPosition += new Vector3(0f, originY + interval, 0f);
                                prevPosition = originY + interval;
                                upFlag = true;
                            }
                            else
                            {
                                instance.transform.localPosition += new Vector3(0f, prevPosition + interval, 0f);
                                prevPosition += interval;
                            }
                            temp.correctY = originY - (float)i * interval;
                            temp.x = j;
                            temp.y = i;
                            temp.option = generateColor[color];
                            temp.type = (int)BLOCKTYPE.COLORED;
                            temp.speed = 0f;
                            temp.item = 0;
                            blockArr[i, j] = temp;
                            /*
                            if (i < inGameStartY)
                            {
                                temp.renderer.enabled = false;
                                temp.collider.enabled = false;
                            }*/
                            temp.DropBlock();
                        }
                    }
                }
            }
            else
            {
                bool allClear = true;
                for ( int i = 0; i<height; i++)
                {
                    for ( int j=0; j<width; j++)
                    {
                        if( blockArr[i,j] != null)
                        {
                            if (blockArr[i, j].type == (int)BLOCKTYPE.COLORED || blockArr[i, j].type == (int)BLOCKTYPE.ITEM || blockArr[i, j].type == (int)BLOCKTYPE.NUI)
                            {
                                allClear = false;
                            }
                        }
                    }
                }
                if( allClear == true)
                {
                    foreach (MissionAdapter adapter in missions[0])
                    {
                        if (adapter.completed == true)
                            continue;
                        adapter.completed = true;
                    }
                }
            }
            #endregion

        }
        else
        {
            //exception
        }


        #region ALLCLEAR_PROCESS
        /*
        if (timerSwitch == true)
        {
            if (timer < play_time)
            {
                cleared = false;
                Application.LoadLevel("ResultScene");
                return result;
            }
        }*/

        #endregion


        #region MISSION_CHECK
        bool stageComplete = true;
        for (int i = 0; i < missions.Length; i++)
        {
            foreach (MissionAdapter adapt in missions[i])
            {
                if (adapt.completed == false)
                {
                    stageComplete = false;
                    break;
                }
            }
        }
        if (stageComplete == true)
        {
            cleared = true;
            Application.LoadLevel("ResultScene");
            return result;
        }
        #endregion


        if ( DieTest() == false)
        {
            cleared = false;
            Application.LoadLevel("ResultScene");
            noMore = true;
            return result;
        }
        return result;
    }

    private bool DieTest()
    {
        for( int i = inGameStartY; i<height; i++)
        {
            for ( int j=0; j<width; j++)
            {
                if( blockArr[i,j] != null && blockArr[i,j].type == (int)BLOCKTYPE.COLORED)
                {
                    int ea = PopTest(j, i).Count;
                    if (ea > 1)
                        return true;
                }
                else if( blockArr[i,j] != null && blockArr[i,j].type == (int)BLOCKTYPE.ITEM)
                {
                    return true;
                }
            }
        }
        return false;
    }

    private ArrayList PopTest(int x, int y)
    {
        ArrayList result = new ArrayList();
        ArrayList nextSearch = new ArrayList();
        bool[,] checkMap = new bool[height, width];
        int type = blockArr[y, x].type;
        int color = blockArr[y, x].option;
        if (type != (int)BLOCKTYPE.COLORED)
            return result;
        nextSearch.Add(new IntPoint(x, y));
        checkMap[y, x] = true;
        while (true)
        {
            #region CompactFF_PROCESS
            ArrayList nowSearch = new ArrayList(nextSearch);
            nextSearch.Clear();
            foreach (IntPoint block in nowSearch)
            {
                result.Add(block);
                if (block.y + 1 != height && blockArr[block.y + 1, block.x] != null &&
                    blockArr[block.y + 1, block.x].type == type &&
                    blockArr[block.y + 1, block.x].option == color && checkMap[block.y + 1, block.x] == false)
                {
                    nextSearch.Add(new IntPoint(block.x, block.y + 1));
                    checkMap[block.y + 1, block.x] = true;
                }
                if (block.y > inGameStartY && blockArr[block.y - 1, block.x] != null &&
                    blockArr[block.y - 1, block.x].type == type &&
                    blockArr[block.y - 1, block.x].option == color && checkMap[block.y - 1, block.x] == false)
                {
                    nextSearch.Add(new IntPoint(block.x, block.y - 1));
                    checkMap[block.y - 1, block.x] = true;
                }
                if (block.x + 1 != width && blockArr[block.y, block.x + 1] != null &&
                    blockArr[block.y, block.x + 1].type == type &&
                    blockArr[block.y, block.x + 1].option == color && checkMap[block.y, block.x + 1] == false)
                {
                    nextSearch.Add(new IntPoint(block.x + 1, block.y));
                    checkMap[block.y, block.x + 1] = true;
                }
                if (block.x != 0 && blockArr[block.y, block.x - 1] != null &&
                    blockArr[block.y, block.x - 1].type == type
                    && blockArr[block.y, block.x - 1].option == color && checkMap[block.y, block.x - 1] == false)
                {
                    nextSearch.Add(new IntPoint(block.x - 1, block.y));
                    checkMap[block.y, block.x - 1] = true;
                }
            }
            if (nextSearch.Count == 0)
                break;
            #endregion
        }
        return result;
    }

    public void Update(float deltatime)
    {
        if (gamePaused == true)
            return;
        play_time += deltatime;
        //        secondCounter += deltatime;

        StarBarRefresh();

        /*
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (timerSwitch == false)
                TimeSlip();
        }*/

        if (limitFlag == true)
        {
            limitScale -= Time.deltaTime * 4;
            if (limitScale < 1f)
            {
                limitScale = 1f;
                limitFlag = false;
            }
            limitObject.transform.localScale = new Vector3(limitScale, limitScale, 1f);
            //
        }

        if (redWhiteFlag == true)
        {
            redWhiteTimer += Time.deltaTime * 2f;
            if (redWhiteTimer > 1f)
            {
                redWhiteColor = !redWhiteColor;
                redWhiteTimer -= 1f;
                if (redWhiteColor == true)
                {// red
                    limitObject.GetComponent<UILabel>().color = new Color(1f, 0.2f, 0.2f, 1f);
                }
                else
                {// white
                    limitObject.GetComponent<UILabel>().color = new Color(1f, 1f, 1f, 1f);
                }
            }
        }

        if (kindRedWhiteFlag == true)
        {
            kindRedWhiteProgress += Time.deltaTime;
            if (kindRedWhiteProgress > 0.5f)
            {
                kindRedWhiteColor = !kindRedWhiteColor;
                kindRedWhiteProgress -= 0.5f;
                if (kindRedWhiteColor == true)
                {// red
                    limitKind.GetComponent<UILabel>().color = new Color(1f, 0.2f, 0.2f, 1f);
                }
                else
                {// white
                    limitKind.GetComponent<UILabel>().color = new Color(1f, 1f, 1f, 1f);
                    kindRedWhiteRemainTime--;
                    if( kindRedWhiteRemainTime <= 0)
                    {
                        kindRedWhiteFlag = false;
                        kindRedWhiteProgress = 0f;
                    }
                }
            }
        }



        if (timerSwitch == true && timer < play_time)
        {
            cleared = false;
            Application.LoadLevel("ResultScene");
            return;
        }

        #region TIMEOUT_PROCESS
        foreach (MissionAdapter adapter in missions[6])
        {
            if (adapter.completed == false)
                continue;
            if (play_time > (float)adapter.mission.optionArray[0])
            {
                adapter.completed = false;
                cleared = false;
                Application.LoadLevel("ResultScene");
                return;
            }
            else
            {
                adapter.option = adapter.mission.optionArray[0] - (int)play_time;
                string time = GetMissionString(adapter);
                if (adapter.text.GetComponent<UILabel>().text != time)
                {
                    adapter.text.GetComponent<UILabel>().text = time;
                    if( adapter.option < 6)
                    {
                        LimitScaleApply();
                        SetRedWhite();
                    }
                    else if (adapter.option < 10)
                    {
                        SetRedWhite();
                    }

                }
            }
        }
        #endregion

        /*
        #region MISSION_CHECK
        bool stageComplete = true;
        for (int i = 0; i < missions.Length; i++)
        {
            foreach (MissionAdapter adapt in missions[i])
            {
                if (adapt.completed == false)
                {
                    stageComplete = false;
                    break;
                }
            }
        }
        if (stageComplete == true)
        {
            cleared = true;
            Application.LoadLevel("ResultScene");
            return;
        }
        #endregion
        */
    }

    public void GamePause()
    {
        gamePaused = true;
        GameObject root = GameObject.Find("pauseContent");
        for (int i = 0; i < root.transform.childCount; i++)
        {
            SpriteRenderer renderer = root.transform.GetChild(i).GetComponent<SpriteRenderer>();
            BoxCollider collider = root.transform.GetChild(i).GetComponent<BoxCollider>();

            if (renderer != null)
            {
                renderer.enabled = true;
            }
            if (collider != null)
            {
                collider.enabled = true;
            }
        }
    }

    public void GameUnpause()
    {
        gamePaused = false;
        GameObject root = GameObject.Find("pauseContent");
        for (int i = 0; i < root.transform.childCount; i++)
        {
            SpriteRenderer renderer = root.transform.GetChild(i).GetComponent<SpriteRenderer>();
            BoxCollider collider = root.transform.GetChild(i).GetComponent<BoxCollider>();

            if (renderer != null)
            {
                renderer.enabled = false;
            }
            if (collider != null)
            {
                collider.enabled = false;
            }
        }
    }

    public void TurnAdd(int num)
    {
        play_turn -= num;
        bonus_turn += num;
        foreach (MissionAdapter adapter in missions[3]) // turn limit
        {
            adapter.option = adapter.mission.optionArray[0] - play_turn;
            if (adapter.option > 5)
            {
                UnSetRedWhite();
            }
            adapter.text.GetComponent<UILabel>().text = GetMissionString(adapter);
        }
    }
}
