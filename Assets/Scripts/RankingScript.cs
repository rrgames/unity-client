﻿using UnityEngine;
using System.Collections;
using LitJson;

public class RankingScript : MonoBehaviour {

    bool loaded;
    bool load1;
    bool load2;
    public bool nowLoading;
    bool playerExist;
    bool waiting;

    GameObject line;
    GameObject other1;
    GameObject other1Text;
    GameObject player;
    GameObject playerText;
    GameObject other2;
    GameObject other2Text;

    void Awake()
    {

    }

	void Start()
    {
	}

    public void Init()
    {
        line = gameObject.transform.FindChild("line").gameObject;
        other1 = gameObject.transform.FindChild("other1").gameObject;
        other1Text = gameObject.transform.FindChild("other1Text").gameObject;
        player = gameObject.transform.FindChild("player").gameObject;
        playerText = gameObject.transform.FindChild("playerText").gameObject;
        other2 = gameObject.transform.FindChild("other2").gameObject;
        other2Text = gameObject.transform.FindChild("other2Text").gameObject;
    }
	
	void Update ()
    {
	
	}

    public void ShowScreen()
    {
        if( loaded == false || nowLoading == true)
        {
            waiting = true;
            return;
        }
        Debug.Log(playerExist.ToString());

        if (other1Text.GetComponent<TextMesh>().text != "")
        {
            Debug.Log("text1 : " + other1Text.GetComponent<TextMesh>().text);
            other1.renderer.enabled = true;
            other1Text.renderer.enabled = true;
            line.renderer.enabled = true;
        }
        if (other2Text.GetComponent<TextMesh>().text != "")
        {
            Debug.Log("text3 : " + other2Text.GetComponent<TextMesh>().text);
            other2.renderer.enabled = true;
            other2Text.renderer.enabled = true;
            line.renderer.enabled = true;
        }
        if( playerExist == true)
        {
            Debug.Log("text2 : " + playerText.GetComponent<TextMesh>().text);
            player.renderer.enabled = true;
            playerText.renderer.enabled = true;
            line.renderer.enabled = true;
        }
        if(line.renderer.enabled == false)
        {
            Debug.Log("empty result");
            playerText.GetComponent<TextMesh>().text = "표시할 정보가 없습니다";
            playerText.transform.localPosition = new Vector3(0f, 0f, 0f);
            playerText.renderer.enabled = true;
        }
        waiting = false;
    }

    public void UnShowScreen()
    {
        waiting = false;
        for( int i=0; i<gameObject.transform.childCount; i++)
        {
            gameObject.transform.GetChild(i).renderer.enabled = false;
        }
    }

    public void ApplyRanking(System.Collections.Generic.List<RankData> rankList)
    {
        if (nowLoading == true)
            return;

        for (int i = 0; i < rankList.Count; i++ )
        {
//            Debug.Log("Apply List :"+ rankList[i].score.ToString());
        }

            nowLoading = true;
        int myId;
        if (int.TryParse(SecurityIO.GetString("id"), out myId))
        {
            for (int i = 0; i < rankList.Count; i++)
            {
                if (rankList[i].id == myId)
                {
//                    Debug.Log(rankList[i].id + "+" + rankList[i].score);
                    playerExist = true;
                    GameManager.GetInstance().SetPreviousScore(rankList[i].score);
                    if( i != 0)
                    {
                        load1 = false;
                        StartCoroutine(WaitForUsername(new WWW("https://rrgames.me:3000/users/"+rankList[i-1].id.ToString()+"/?token="+SecurityIO.GetString("token")),0));
                    }
                    else
                    {
                        load1 = true;
                    }
                    if (rankList.Count > i + 1)
                    {
                        load2 = false;
                        StartCoroutine(WaitForUsername(new WWW("https://rrgames.me:3000/users/"+rankList[i+1].id.ToString()+"/?token="+SecurityIO.GetString("token")), 1));
                    }
                    else
                    {
                        load2 = true;
                    }
                }
            }
            if( playerExist == false)
            {
                GameManager.GetInstance().SetPreviousScore(0);
                if( rankList.Count > 0)
                {
                    load1 = false;
                    StartCoroutine(WaitForUsername(new WWW("https://rrgames.me:3000/users/" + rankList[0].id.ToString() + "/?token=" + SecurityIO.GetString("token")), 0));
                }
                else
                {
                    load1 = true;
                }

                if (rankList.Count > 1)
                {
                    load2 = false;
                    StartCoroutine(WaitForUsername(new WWW("https://rrgames.me:3000/users/" + rankList[1].id.ToString() + "/?token=" + SecurityIO.GetString("token")), 1));
                }
                else
                {
                    load2 = true;
                }
            }
            if( load1 == true && load2 == true)
            {
                loaded = true;
                nowLoading = false;
                if( waiting == true)
                {
                    ShowScreen();
                }
            }
        }
    }

    IEnumerator WaitForUsername(WWW www, int position)
    {
        yield return www;
        if(www.error == null)
        {
            string username = "";
            string imageURL = "";
            JsonReader reader = new JsonReader(www.text);
            while (reader.Read())
            {
                if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "name")
                {
                    reader.Read();
                    username = reader.Value.ToString();
                }
                else if( reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "profile_image")
                {
                    reader.Read();
                    imageURL = reader.Value.ToString();
                }
            }
            if (position == 0)
            {
                other1Text.GetComponent<TextMesh>().text = username;
                load1 = true;
            }
            else if (position == 1)
            {
                other2Text.GetComponent<TextMesh>().text = username;
                load2 = true;
            }
            if (load1 == true && load2 == true)
            {
                nowLoading = false;
                loaded = true;
                if (waiting == true)
                    ShowScreen();
            }
        }
    }


}
