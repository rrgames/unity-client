﻿using UnityEngine;
using System.Collections;

public class StarAnimation : MonoBehaviour {

    public Sprite renderingTarget;
    public float delayTime;
    public float interval;
    public float animationTime;
    public float fromScale;
    public float toScale;
    public float fromAngle;
    public float toAngle;

    public float animationTimeOut;
    public float fromScaleOut;
    public float toScaleOut;

 //   private SpriteRenderer instanceOfTarget;
    private GameObject instance;
    private GameObject layer1;
    private GameObject layer2;
    private GameObject layer3;
    private GameObject layer4;

    private IEnumerator StartAnimationIn(float alpha, bool noDelete)
    {
        float startTime = Time.time;
        GameObject texture = new GameObject("Star");// MonoBehaviour.Instantiate(instance) as GameObject;
        texture.AddComponent<SpriteRenderer>();
        texture.transform.parent = gameObject.transform;
        texture.transform.localPosition = Vector3.zero;
        texture.transform.localScale = new Vector3(fromScale, fromScale);
        texture.transform.localRotation = Quaternion.Euler(new Vector3(0f,0f,fromAngle));
        texture.GetComponent<SpriteRenderer>().sprite = renderingTarget;
        texture.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, alpha);
        texture.GetComponent<SpriteRenderer>().sortingOrder = -1;
        yield return null;

        while(true)
        {
            float time = Time.time - startTime;
            if( animationTime < time)
            {
                texture.transform.localScale = new Vector3(toScale, toScale);
                texture.transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, toAngle));
                break;
            }
            else
            {
                float progress = time / animationTime;
                float scale = Mathf.Lerp(fromScale, toScale, progress);
                texture.transform.localScale = new Vector3(scale,scale);
                texture.transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, Mathf.Lerp(fromAngle, toAngle, progress)));
            }
            yield return null;
        }
        if( !noDelete)
            GameObject.Destroy(texture);
        yield break;
    }

    private IEnumerator StartAnimationOut()
    {
        float startTime = Time.time;
        GameObject texture = new GameObject("Star");// MonoBehaviour.Instantiate(instance) as GameObject;
        texture.AddComponent<SpriteRenderer>();
        texture.transform.parent = gameObject.transform;
        texture.transform.localPosition = Vector3.zero;
        texture.transform.localScale = new Vector3(fromScaleOut, fromScaleOut);
        texture.transform.localRotation = Quaternion.Euler(Vector3.zero);
        texture.GetComponent<SpriteRenderer>().sprite = renderingTarget;
        texture.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
        yield return null;

        while (true)
        {
            float time = Time.time - startTime;
            if (animationTimeOut < time)
            {
                texture.transform.localScale = new Vector3(toScaleOut, toScaleOut);
                texture.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0f);
                break;
            }
            else
            {
                float progress = time / animationTimeOut;
                float scale = Mathf.Lerp(fromScaleOut, toScaleOut, progress);
                texture.transform.localScale = new Vector3(scale, scale);
                texture.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, Mathf.Lerp(1f,0f,progress));
            }
            yield return null;
        }
        GameObject.Destroy(texture);
        yield break;
    }

    void Awake()
    {/*
        instance = new GameObject();
        instance.AddComponent<SpriteRenderer>();
        instance.GetComponent<SpriteRenderer>().sprite = renderingTarget;
        instance.GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f,1f);*/
    }
	// Use this for initialization
	IEnumerator Start () {
        yield return new WaitForSeconds(delayTime);
        StartCoroutine(StartAnimationIn(1f, true));
        yield return new WaitForSeconds(interval);
        StartCoroutine(StartAnimationIn(0.1f, false));
        yield return new WaitForSeconds(interval);
        StartCoroutine(StartAnimationIn(0.1f, false));
        yield return new WaitForSeconds(animationTime - interval - interval);
        StartCoroutine(StartAnimationOut());

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
