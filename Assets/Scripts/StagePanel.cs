﻿using UnityEngine;
using System.Collections;
using LitJson;

public class StagePanel : MonoBehaviour {
    public GameObject missionParent;
    public GameObject rankingParent;
    public UILabel rankingText;
    public UIGrid rankingGrid;
    public string id {get; set;}
    private bool rankLoaded;
    private bool isStarted;
    private bool isCompleted;
    private int state;
    private System.Collections.Generic.List<RankPanel> rankList;
    private bool loading;

    void Awake()
    {
        rankList = new System.Collections.Generic.List<RankPanel>();
    }

	// Use this for initialization
	void Start () {
        MissionButton();    
	}
	
	// Update is called once per frame
    void Update()
    {
        if (isStarted == true && isCompleted == false)
        {
            bool complete = true;
            for (int i = 0; i < rankList.Count; i++)
            {
                if (rankList[i].loaded == false)
                {
                    complete = false;
                    break;
                }
            }
            if (complete == true)
            {
                loading = false;
                isCompleted = true;
                if (rankList.Count != 0)
                    rankingText.text = "";
                for (int i = 0; i < rankList.Count; i++)
                {
                    rankList[i].UnHide();
                }
            }
        }
	}

    public bool MissionButton()
    {
        if( loading == true)
        {
            return false;
        }
        rankingParent.SetActive(false);
        missionParent.SetActive(true);
        GameObject button = gameObject.transform.FindChild("mask").gameObject;
        button.transform.localPosition = gameObject.transform.FindChild("mission").localPosition;// new Vector3(-1.1f, -0.1f, 0f);
        if (state == 0)
            return false;
        state = 0;
        return true;
    }

    public bool RankingButton()
    {
        if( loading == true)
        {
            return false;
        }
        missionParent.SetActive(false);
        rankingParent.SetActive(true);
        GameObject button = gameObject.transform.FindChild("mask").gameObject;
        button.transform.localPosition = gameObject.transform.FindChild("ranking").localPosition;// new Vector3(-1.1f, -0.1f, 0f);
        if( isCompleted == false)
        {
            loading = true;
            StartCoroutine(WaitForRecord(new WWW("https://rrgames.me:3000/stages/" + id + "/records?token=" + SecurityIO.GetString("token") + "&location=" + SecurityIO.GetString("id"))));
        }
        if (state == 1)
            return false;
        state = 1;
        return true;
    }

    private IEnumerator WaitForRecord(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            Debug.Log(www.text);
            System.Collections.Generic.List<RankData> list = new System.Collections.Generic.List<RankData>();
            JsonReader reader = new JsonReader(www.text);
            int id = 0;
            float score = 0f;
            RankData rankData = null;
            while (reader.Read())
            {
                if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "user_id")
                {
                    reader.Read();
                    int.TryParse(reader.Value.ToString(), out id);
                    rankData = new RankData();
                    rankData.id = id;
                }
                else if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "score")
                {
                    reader.Read();
                    float.TryParse(reader.Value.ToString(), out score);
                    rankData.score = (int)score;// score;
                    list.Add(rankData);
                }
            }
            list.Sort(new ScoreComparer());
            StageReader.GetInstance().GetStage().rankArray = list;
            GameObject rankPanel = Resources.Load("Prefab/RankNGUI") as GameObject;

            int beforeRank = -1;
            int beforeScore = -1;
            for (int i = 0; i < list.Count; i++)
            {
                GameObject instance = MonoBehaviour.Instantiate(rankPanel) as GameObject;
                string objectName = i.ToString().Length.ToString() + i.ToString();
                int rank;
                if (list[i].score == beforeScore)
                    rank = beforeRank;
                else
                    rank = i + 1;
                instance.transform.parent = rankingGrid.transform;
                instance.transform.localScale = new Vector3(1f, 1f, 1f);
                instance.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
                instance.transform.localPosition = new Vector3(0f,0f,0f);
                instance.name = objectName;
                RankPanel compo = instance.GetComponent<RankPanel>();
                compo.Hide();
                compo.SetRankingData(list[i].id.ToString(), rank.ToString(), list[i].score.ToString());
                rankList.Add(compo);
                beforeScore = list[i].score;
                beforeRank = rank;
            }
            rankingGrid.enabled = true;
            if (list.Count == 0)
            {
                loading = false;
                rankingText.text = "표시할 정보가 없습니다.";
            }
            isStarted = true;
        }
        else
        {
            Debug.Log(www.error);
        }
    }

}
