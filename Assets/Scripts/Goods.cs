﻿using UnityEngine;
using System.Collections;

public class Goods : MonoBehaviour {
	public string id;
	public UILabel price;
	public UILabel title;
    public UITexture itemIcon;
    public UITexture priceFader;
    public UIButton button;
    private ShopController shop;
	private bool isCash;
    private int index;
    private int value;
    private bool animateFlag;
    private bool imageLoaded;
    private string url;


    public IEnumerator WaitForImage(WWW www)
    {
        if (imageLoaded == true)
            yield break;
        yield return www;
        if (www.error == null)
        {
            Texture2D profileImage = new Texture2D(64, 64, TextureFormat.DXT1, false);
            www.LoadImageIntoTexture(profileImage);
            itemIcon.mainTexture = profileImage;
            imageLoaded = true;
        }
        else
        {
            Debug.Log(www.error);
        }
    }

    public void ImageLoad()
    {
        StartCoroutine(WaitForImage(new WWW(url)));
    }

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
    
	public void SetId(string input_id)
	{
		id = input_id;
	}
	
	public void SetIndex(int value_)
	{
        index = value_;
	}
	
	public void PriceModify(int number, bool isCash)
	{
        value = number;
		price.text = number.ToString ();
	}

	public void TitleModify(string str)
	{
		title.text = str;
	}

    public void SetImage(string imageUrl)
    {
        url = imageUrl;
    }

    public void ButtonInit(ShopController shopcontroller)
    {
        shop = shopcontroller;
        button.onClick.Add(new EventDelegate(this, "ButtonTouched"));
    }

    public void ButtonTouched()
    {
        shop.ProductTouched(index);
    }

    public void OpenPurchase()
    {
        StartCoroutine(WaitForFade("Purchase"));
        //price.text = "구매하기";
    }

    public void ClosePurchase()
    {
        StartCoroutine(WaitForFade(value.ToString()));
        //price.text = value.ToString();
    }

    IEnumerator WaitForFade(string txt)
    {
        animateFlag = !animateFlag;
        bool myFlag = animateFlag;
        float progress = 0.0f;
        while (true)
        {
            if (myFlag != animateFlag)
                yield break;
            priceFader.invert = true;
            progress += Time.deltaTime * 8;
            if (progress > 1f)
            {
                progress = 1f;
                priceFader.fillAmount = progress;
                break;
            }
            priceFader.fillAmount = progress;
            yield return null;
        }
        price.text = txt;
        yield return null;
        while (true)
        {
            if (myFlag != animateFlag)
                yield break;
            priceFader.invert = false;
            progress -= Time.deltaTime * 8;
            if (progress < 0f)
            {
                progress = 0f;
                priceFader.fillAmount = progress;
                break;
            }
            priceFader.fillAmount = progress;
            yield return null;
        }
        yield break;
    }
}
