﻿using UnityEngine;
using System.Collections;
using System.Xml;

public enum BLOCK
{
    BLOCK_RED = 0,
    BLOCK_YEL,
    BLOCK_GRN,
    BLOCK_BLU,
    BLOCK_PURPLE,
    BLOCK_NUI,
    BLOCK_WALL,
    BLOCK_EMPTY,
    BLOCK_RANDOM,
    BLOCK_ITEM,
}


public enum MISSION
{
    MISSION_AC = 0,   //All clear
    MISSION_CB,     //
    MISSION_DB,
    MISSION_ML,
    MISSION_PM,
    MISSION_SC,
    MISSION_TL
}

public class Mission
{
    public int type;
    public System.Collections.Generic.List<int> optionArray;

    public Mission()
    {
        optionArray = new System.Collections.Generic.List<int>();
    }
    ~Mission()
    {
        optionArray.Clear();
        optionArray = null;
    }
}

public class StageReader
{
	private static StageReader instance;
    public System.Collections.Generic.List<Stage> stageArray;
	public int lastStageIndex;
    public class Stage
    {
        public int highScore { get; set; }
        public string id { get; private set; }
        public int[] grade { get; private set; }
        public bool infinite { get; private set; }
        public bool[] colorFlag { get; private set; }
        public System.Collections.Generic.List<Mission> missionArray;
        public int width { get; private set; }
        public int height { get; private set; }
        public System.Collections.Generic.List<int> blockArray;
        public System.Collections.Generic.List<int> nuiArray;
        public System.Collections.Generic.List<int> itemArray;
        public System.Collections.Generic.List<RankData> rankArray;

        public Stage()
        {
            missionArray = new System.Collections.Generic.List<Mission>();
            blockArray = new System.Collections.Generic.List<int>();
            nuiArray = new System.Collections.Generic.List<int>();
            itemArray = new System.Collections.Generic.List<int>();
            rankArray = new System.Collections.Generic.List<RankData>();
            grade = new int[3];
            colorFlag = new bool[5];
        }
        ~Stage()
        {
            blockArray.Clear();
            blockArray = null;
            nuiArray.Clear();
            nuiArray = null;
            itemArray.Clear();
            itemArray = null;
            rankArray.Clear();
            rankArray = null;
            missionArray.Clear();
            missionArray = null;
        }

        public void SetId(string id)
        {
            this.id = id;
        }

        public void LoadStage(string data)
        {
            blockArray.Clear();
            string xmlParsePath = "stage";

            int tryOut_int;
            bool tryOut_bool;
            string tryOut_string;
            XmlDocument doc = new XmlDocument();

            doc.LoadXml(data);
            if (int.TryParse(doc.SelectSingleNode(xmlParsePath + @"/grade/L1").InnerText, out tryOut_int))
                grade[0] = tryOut_int;
            else
                Debug.LogError("XML Parse Error");
            if (int.TryParse(doc.SelectSingleNode(xmlParsePath + @"/grade/L2").InnerText, out tryOut_int))
                grade[1] = tryOut_int;
            else
                Debug.LogError("XML Parse Error");
            if (int.TryParse(doc.SelectSingleNode(xmlParsePath + @"/grade/L3").InnerText, out tryOut_int))
                grade[2] = tryOut_int;
            else
                Debug.LogError("XML Parse Error");

            if (bool.TryParse(doc.SelectSingleNode(xmlParsePath + @"/rule/infinite").InnerText, out tryOut_bool))
                infinite = tryOut_bool;
            else
                Debug.LogError("XML Parse Error");
            if( infinite == true)
            {
                tryOut_string = doc.SelectSingleNode(xmlParsePath + @"/rule/colorflag").InnerText;
                for (int i = 0; i < tryOut_string.Length; i++)
                {
                    if (tryOut_string[i] == '1')
                        colorFlag[i] = true;
                    else
                        colorFlag[i] = false;
                }
            }

            foreach (XmlNode node in doc.SelectNodes(xmlParsePath + @"/missions/mission"))
            {
                Mission mission = new Mission();
                string[] args;
                switch(node.InnerText.Substring(0,2))
                {
                    case "AC":
                        mission.type = (int)MISSION.MISSION_AC;
                        break;
                    case "CB":
                        mission.type = (int)MISSION.MISSION_CB;
                        args = node.InnerText.Substring(2).Split(',');
                        switch(args[0][0])
                        {
                            case 'R':
                                mission.optionArray.Add((int)BLOCK.BLOCK_RED);
                                break;
                            case 'Y':
                                mission.optionArray.Add((int)BLOCK.BLOCK_YEL);
                                break;
                            case 'G':
                                mission.optionArray.Add((int)BLOCK.BLOCK_GRN);
                                break;
                            case 'B':
                                mission.optionArray.Add((int)BLOCK.BLOCK_BLU);
                                break;
                            case 'P':
                                mission.optionArray.Add((int)BLOCK.BLOCK_PURPLE);
                                break;
                            case 'N':
                                mission.optionArray.Add((int)BLOCK.BLOCK_NUI);
                                break;
                            default:
                                break;
                        }
                        int.TryParse(args[1], out tryOut_int);
                        mission.optionArray.Add(tryOut_int);
                        break;
                    case "DB":
                        mission.type = (int)MISSION.MISSION_DB;
                        args = node.InnerText.Substring(2).Split(',');
                        int.TryParse(args[0], out tryOut_int);
                        mission.optionArray.Add(tryOut_int);
                        int.TryParse(args[1], out tryOut_int);
                        mission.optionArray.Add(tryOut_int);
                        break;
                    case "ML":
                        mission.type = (int)MISSION.MISSION_ML;
                        int.TryParse(node.InnerText.Substring(2), out tryOut_int);
                        mission.optionArray.Add(tryOut_int);
                        break;
                    case "PM":
                        mission.type = (int)MISSION.MISSION_PM;
                        args = node.InnerText.Substring(2).Split(',');
                        switch(args[0][0])
                        {
                            case 'R':
                                mission.optionArray.Add((int)BLOCK.BLOCK_RED);
                                break;
                            case 'Y':
                                mission.optionArray.Add((int)BLOCK.BLOCK_YEL);
                                break;
                            case 'G':
                                mission.optionArray.Add((int)BLOCK.BLOCK_GRN);
                                break;
                            case 'B':
                                mission.optionArray.Add((int)BLOCK.BLOCK_BLU);
                                break;
                            case 'P':
                                mission.optionArray.Add((int)BLOCK.BLOCK_PURPLE);
                                break;
                            case 'N':
                                mission.optionArray.Add((int)BLOCK.BLOCK_NUI);
                                break;
                            default:
                                break;
                        }
                        int.TryParse(args[1], out tryOut_int);
                        mission.optionArray.Add(tryOut_int);

                        break;
                    case "SC":
                        mission.type = (int)MISSION.MISSION_SC;
                        int.TryParse(node.InnerText.Substring(2), out tryOut_int);
                        mission.optionArray.Add(tryOut_int);
                        break;
                    case "TL":
                        mission.type = (int)MISSION.MISSION_TL;
                        int.TryParse(node.InnerText.Substring(2), out tryOut_int);
                        mission.optionArray.Add(tryOut_int);
                        break;
                    default:
                        Debug.Log(node.InnerText.Substring(0,2));
                        Debug.LogError(doc.SelectSingleNode(xmlParsePath + "/name").InnerText + "XML Parse Error");
                        break;
                }
                missionArray.Add(mission);
            }

            if (int.TryParse(doc.SelectSingleNode(xmlParsePath + @"/width").InnerText, out tryOut_int))
                width = tryOut_int;
            else
                Debug.LogError("XML Parse Error");
            if (int.TryParse(doc.SelectSingleNode(xmlParsePath + @"/height").InnerText, out tryOut_int))
                height = tryOut_int;
            else
                Debug.LogError("XML Parse Error");

            tryOut_string = doc.SelectSingleNode(xmlParsePath + @"/map").InnerText;
            int length = tryOut_string.Length;
            for (int i = 0; i < length; i++)
            {
                if (tryOut_string[i] == 'R')
                    blockArray.Add((int)BLOCK.BLOCK_RED);
                else if (tryOut_string[i] == 'Y')
                    blockArray.Add((int)BLOCK.BLOCK_YEL);
                else if (tryOut_string[i] == 'G')
                    blockArray.Add((int)BLOCK.BLOCK_GRN);
                else if (tryOut_string[i] == 'B')
                    blockArray.Add((int)BLOCK.BLOCK_BLU);
                else if (tryOut_string[i] == 'P')
                    blockArray.Add((int)BLOCK.BLOCK_PURPLE);
                else if (tryOut_string[i] == 'N')
                {
                    blockArray.Add((int)BLOCK.BLOCK_NUI);
                    int nuiNumber = 0;
                    int it = 1;
                    while(true)
                    {
                        if( length > i+it && int.TryParse(tryOut_string[i+it].ToString(), out tryOut_int))
                        {
//                            Debug.Log(tryOut_int.ToString());
                            nuiNumber *= 10;
                            nuiNumber += tryOut_int;
                        }
                        else
                        {
                            break;
                        }
                        it++;
                    }
                    nuiArray.Add(nuiNumber);
                    i += it-1;
                    //length++;
                }
                else if (tryOut_string[i] == 'W')
                    blockArray.Add((int)BLOCK.BLOCK_WALL);
                else if (tryOut_string[i] == 'E')
					blockArray.Add((int)BLOCK.BLOCK_EMPTY);
				else if (tryOut_string[i] == 'S')
					blockArray.Add((int)BLOCK.BLOCK_RANDOM);
                else if(tryOut_string[i] == 'I')
                {
                    blockArray.Add((int)BLOCK.BLOCK_ITEM);
                    if( tryOut_string[i+1] == 'V')
                        itemArray.Add((int)ITEMTYPE.SERO);
                    else if (tryOut_string[i + 1] == 'H')
                        itemArray.Add((int)ITEMTYPE.GARO);
                    else if (tryOut_string[i + 1] == '3')
                        itemArray.Add((int)ITEMTYPE.KING);
                    else if (tryOut_string[i + 1] == 'R')
                        itemArray.Add((int)ITEMTYPE.COLORBOMBER_RED);
                    else if (tryOut_string[i + 1] == 'Y')
                        itemArray.Add((int)ITEMTYPE.COLORBOMBER_YELLOW);
                    else if (tryOut_string[i + 1] == 'G')
                        itemArray.Add((int)ITEMTYPE.COLORBOMBER_GREEN);
                    else if (tryOut_string[i + 1] == 'B')
                        itemArray.Add((int)ITEMTYPE.COLORBOMBER_BLUE);
                    else if (tryOut_string[i + 1] == 'P')
                        itemArray.Add((int)ITEMTYPE.COLORBOMBER_PURPLE);
                    else
                        Debug.LogError("XML Parse Error. (item)");
                }
                else
				{
                    Debug.LogError(tryOut_string[i].ToString() + "XML Parse Error");
				}
            }
        }
    }
    public int LockedStage;
    public StageReader()
    {
        stageArray = new System.Collections.Generic.List<Stage>();
        LockedStage = 0;
	}
    ~StageReader()
    {
        stageArray.Clear();
        stageArray = null;
    }
    public void LoadStage(string xml, string id)
    {
        Stage stage = new Stage();
        stage.LoadStage(xml);
        stage.SetId(id);
        stageArray.Add(stage);
    }
    public void LockStage(int num)
    {
        LockedStage = num;
    }
    public Stage GetStage()
    {
//        Debug.Log("Loaded Stage : "+LockedStage.ToString());
//        Debug.Log(stageArray.Count.ToString());
        return stageArray[LockedStage];
    }
	public static StageReader GetInstance()
	{
		if(instance == null)
		{
			instance = new StageReader();
		}
		return instance;
	}
}
