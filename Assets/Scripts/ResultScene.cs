﻿using UnityEngine;
using System.Collections;
using LitJson;

public class ResultScene : MonoBehaviour
{
    public float testTime;

    public UILabel score;
    public UILabel shareLabel;
    public UILabel nextLabel;
    public UIButton shareButton;
    public UIButton nextButton;
    public UIButton sfxButton;
    public UIButton bgmButton;
    public UISprite nextPingPong;
    public UILabel clearLabel;
    public UIGrid rankParent;
	public MoneyScript money;
	public MoneyScript cash;
    public UILabel loadingLabel;

    public AudioClip scoreSound;
    public AudioClip buttonSound;
    public AudioClip soundButtonSound;
    public AudioClip bgm;
	
	public UITexture star1;
	public UITexture star2;
	public UITexture star3;

    public Texture sfxOn;
    public Texture sfxOff;
    public Texture bgmOn;
    public Texture bgmOff;

    private bool isBGM;
    private bool isSFX;

    float waitTime;

    private System.Collections.Generic.List<RankPanel> rankList;

    bool isClear;
    bool isLoaded;
    bool isStarted;

	int realScore;
    bool lastStage;
	bool isGoToNextStage;
    string url;
    WWWForm form;

    private IEnumerator WaitForSend(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            Debug.Log(www.text);
            money.SyncToServer();
            StartCoroutine(WaitForRecord(new WWW("https://rrgames.me:3000/stages/" + StageReader.GetInstance().GetStage().id + "/records?token=" + SecurityIO.GetString("token") + "&location=" + SecurityIO.GetString("id"))));
        }
        else
        {
            Debug.Log(www.error);
        }
    }

    public void BGMButtonTouched()
    {
        isBGM = !isBGM;
        SecurityIO.SetString("bgm", isBGM.ToString());

        if (isBGM == true)
        {
            bgmButton.GetComponent<UITexture>().mainTexture = bgmOn;
            audio.Play();
        }
        else
        {
            bgmButton.GetComponent<UITexture>().mainTexture = bgmOff;
            audio.Stop();
        }
        if( isSFX == true)
        {
            audio.PlayOneShot(soundButtonSound);
        }
    }

    public void SFXButtonTouched()
    {
        isSFX = !isSFX;
        SecurityIO.SetString("sfx", isSFX.ToString());
        if (isSFX == true)
        {
            sfxButton.GetComponent<UITexture>().mainTexture = sfxOn;
            audio.PlayOneShot(soundButtonSound);
        }
        else
        {
            sfxButton.GetComponent<UITexture>().mainTexture = sfxOff;
        }
    }

    private IEnumerator WaitForRecord(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            rankList.Clear();
            System.Collections.Generic.List<RankData> list = new System.Collections.Generic.List<RankData>();
            JsonReader reader = new JsonReader(www.text);
            int id = 0;
            float score = 0f;
            RankData rankData = null;
            while (reader.Read())
            {
                if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "user_id")
                {
                    reader.Read();
                    int.TryParse(reader.Value.ToString(), out id);
                    rankData = new RankData();
                    rankData.id = id;
                }
                else if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "score")
                {
                    reader.Read();
                    float.TryParse(reader.Value.ToString(), out score);
                    rankData.score = (int)score;// score;
                    list.Add(rankData);
                }
            }
            list.Sort(new ScoreComparer());

            StageReader.GetInstance().GetStage().rankArray = list;

            GameObject rankPanel = Resources.Load("Prefab/RankNGUI") as GameObject;

            int beforeRank = -1;
            int beforeScore = -1;
            for ( int i=0; i<list.Count; i++)
            {
                GameObject instance = MonoBehaviour.Instantiate(rankPanel) as GameObject;
                string objectName = i.ToString().Length.ToString() + i.ToString();
                int rank;
                if (list[i].score == beforeScore)
                    rank = beforeRank;
                else
                    rank = i+1;
                instance.transform.parent = rankParent.transform;
                instance.transform.localScale = new Vector3(1f, 1f, 1f);
                instance.name = objectName;
                RankPanel compo = instance.GetComponent<RankPanel>();
                compo.Hide();
                compo.SetRankingData(list[i].id.ToString(), rank.ToString(), list[i].score.ToString());
                rankList.Add(compo);
                beforeScore = list[i].score;
                beforeRank = rank;
            }
            rankParent.enabled = true;
            if( list.Count == 0)
            {
                isLoaded = true;
                loadingLabel.text = "No one could complete";
            }
            isStarted = true;
            /*
            for (int i = 0; i < rankList.Count; i++ )
            {
                    StartCoroutine(WaitForProfile(new WWW("https://rrgames.me:3000/users/" + list[rankList[i]].id.ToString() + "/?token=" + SecurityIO.GetString("token")), i));
                }
            }*/
        }
        else
        {
            Debug.Log(www.error);
        }
    }
    /*
    private IEnumerator WaitForImage(WWW www, int order)
    {
        yield return www;
        if (www.error == null)
        {
            GameObject obj = null;
            Texture2D profileImage = new Texture2D(128, 128, TextureFormat.DXT1, false);
            www.LoadImageIntoTexture(profileImage);
            switch (order)
            {
                case 0:
                    obj = GameObject.Find("top");
                    break;
                case 1:
                    obj = GameObject.Find("mid");
                    break;
                case 2:
                    obj = GameObject.Find("bot");
                    break;
            }
            obj.GetComponent<SpriteRenderer>().sprite = Sprite.Create(profileImage, new Rect(0, 0, profileImage.width - 9, profileImage.height - 9), new Vector2(0.5f, 0.5f));
            obj.renderer.enabled = true;
        }
    }*/
    /*
    private IEnumerator WaitForProfile(WWW www, int order)
    {
        yield return www;
        if (www.error == null)
        {
            JsonReader reader = new JsonReader(www.text);
            while (reader.Read())
            {
                if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "name")
                {
                    reader.Read();
                    string name = reader.Value.ToString();
                    switch (order)
                    {
                        case 0:
                            break;
                        case 1:
                            break;
                        case 2:
                            break;
                    }
                    continue;
                }
                if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "profile_image")
                {
                    reader.Read();
                    string imageUrl = reader.Value.ToString();
                    StartCoroutine(WaitForImage(new WWW(imageUrl), order));
                    continue;
                }
            }
        }
    }
    */



    public string GenerateReplayString()
    {
        string username = SecurityIO.GetString("name");
        string imageURL = SecurityIO.GetString("image");
        string result = "0.1.0.0," + username + "," + imageURL + "," + GameManager.GetInstance().PrintMapForReplayIndex();
        result = SecurityIO.ReplayCompress(result);
        return result;
    }


	public IEnumerator ScoreAnimation()
	{
        float count = 0f;
		int displayScore = 0;
		float time = 0;
		float endTime = realScore / 10000;
		if (endTime > 3f)
			endTime = 3f;
		else if (endTime < 1f)
			endTime = 1f;

		while (time < endTime)
		{

			time += Time.deltaTime;
			displayScore = (int)Mathf.Lerp(0, realScore, time/endTime);
			score.text = displayScore.ToString();

            count+= Time.deltaTime;
            if( count > testTime)
            {
                if( isSFX == true)
                {
                    audio.PlayOneShot(scoreSound);
                }
                count -= testTime;
            }
			yield return null;
		}
		displayScore = realScore;
		score.text = displayScore.ToString ();
//		Debug.Log (displayScore.ToString ());
	}

	public IEnumerator NextAnimation()
	{
		bool isReverted = false;
		float progress = 0f;
		while (true)
		{
			progress += Time.deltaTime;
			if( progress > 1f)
			{
				progress -= 1f;
				isReverted = !isReverted;
			}
			if( isReverted == false)
				nextPingPong.alpha = Mathf.Lerp(0f, 0.75f, progress);
			else
				nextPingPong.alpha = Mathf.Lerp(0.75f, 0f, progress);
			yield return null;
		}
	}

    void Awake()
	{
		//FB.Feed ("","https://rrgames.me/kkutu/");
        waitTime = 0f;
        rankList = new System.Collections.Generic.List<RankPanel>();
        float targetaspect = 9f / 16f;
        float windowaspect = (float)Screen.width / (float)Screen.height;
        float scaleheight = windowaspect / targetaspect;
        Rect rect = new Rect();
        if (scaleheight < 1.0f)
        {
            rect.width = 1.0f;
            rect.height = scaleheight;
            rect.x = 0;
            rect.y = (1.0f - scaleheight) / 2.0f;
        }
        else
        {
            float scalewidth = 1.0f / scaleheight;
            rect.width = scalewidth;
            rect.height = 1.0f;
            rect.x = (1.0f - scalewidth) / 2.0f;
            rect.y = 0;
        }
        for (int i = 0; i < Camera.allCamerasCount; i++)
        {
            Camera camera = Camera.allCameras[i];
            if (camera.name == "ClearCamera")
                continue;
            camera.rect = rect;
        }

        if (bool.TryParse(SecurityIO.GetString("bgm"), out isBGM) == false)
        {
            SecurityIO.SetString("bgm", "true");
            isBGM = true;
        }
        if (bool.TryParse(SecurityIO.GetString("sfx"), out isSFX) == false)
        {
            SecurityIO.SetString("sfx", "true");
            isSFX = true;
        }

        if (isBGM == true)
        {
            bgmButton.GetComponent<UITexture>().mainTexture = bgmOn;
        }
        else
        {
            bgmButton.GetComponent<UITexture>().mainTexture = bgmOff;
        }
        if (isSFX == true)
        {
            sfxButton.GetComponent<UITexture>().mainTexture = sfxOn;
        }
        else
        {
            sfxButton.GetComponent<UITexture>().mainTexture = sfxOff;
        }


		//isGoToNextStage = false;
		shareButton.onClick.Add (new EventDelegate (this, "ShareButtonTouched"));
        nextButton.onClick.Add(new EventDelegate(this, "NextButtonTouched"));
        bgmButton.onClick.Add(new EventDelegate(this, "BGMButtonTouched"));
        sfxButton.onClick.Add(new EventDelegate(this, "SFXButtonTouched"));
		int EarnStars;
		bool isHighScore = false;
        bool cleared = GameManager.GetInstance().cleared;
        bool isNomore = GameManager.GetInstance().noMore;
        isClear = cleared;
        int score = GameManager.GetInstance().score;
        int turn = GameManager.GetInstance().play_turn + GameManager.GetInstance().bonus_turn;
        int giveTurn = GameManager.GetInstance().mission_turn;
        float time = GameManager.GetInstance().play_time;
        float starScore = 0f;
        string id = SecurityIO.GetString("id");

		realScore = score * 10;


        if( cleared == true && giveTurn != -1)
        {
            /*
            int bonus = giveTurn - turn;
            if( bonus > 0)
            {*/
                float a = ((float)giveTurn) / turn - 1;
                if( a > 0f)
                    realScore += (int)(a * 416f * giveTurn);
                /*
                float r = ((float)(giveTurn) / bonus)-1;
                realScore += (int)(r * giveTurn * 416f);
                */
            /*
            }*/
        }

        if( isClear == false)
        {
            nextLabel.text = "Retry";
        }
        
		StartCoroutine (ScoreAnimation());
		StageReader.Stage stage = StageReader.GetInstance().GetStage();
		url = "https://rrgames.me:3000/stages/" + stage.id + "/records?token=" + SecurityIO.GetString("token");
        form = new WWWForm();
        form.AddField("play_time", (int)time);
        form.AddField("played_turns", turn);
        form.AddField("score", realScore);
        if (cleared == true && stage.highScore < realScore)
        {
			isHighScore = true;
            stage.highScore = realScore;
            form.AddField("refresh", "true");
        }
        else
        {
            form.AddField("refresh", "false");
        }

        //Debug.Log(GenerateReplayString());

		if (cleared == false)
		{
            clearLabel.text = "Stage Fail";
			EarnStars = 0;
		}
        else if (realScore >= stage.grade[2])
        {
			EarnStars = 3;
        }
        else if (realScore >= stage.grade[1])
        {
			EarnStars = 2;
        }
        else if (realScore >= stage.grade[0])
        {
			EarnStars = 1;
        }
        else
        {
			EarnStars = 0;
		}

        if (isNomore == true)
        {
            clearLabel.text = "Touchable block not exist";
        }

		if (EarnStars < 3)
		{
			star3.GetComponent<UITexture>().enabled = false;
		}
		if (EarnStars < 2)
		{
			star2.GetComponent<UITexture>().enabled = false;
		}
		if (EarnStars < 1)
		{
			star1.GetComponent<UITexture>().enabled = false;
		}

        //GameObject.Find("Score").GetComponent<TextMesh>().text = (score * 10).ToString() + "점";

        if( cleared == true)
        {
            if (EarnStars == 0)
            {
                starScore = (float)(realScore) / stage.grade[0];
                Debug.Log(starScore.ToString());
            }
            else if (EarnStars == 1)
            {
                //starScore -= stage.grade[0];
                starScore = (float)(realScore - stage.grade[0]) / (stage.grade[1] - stage.grade[0]);
                starScore += 1f;
            }
            else if (EarnStars == 2)
            {
                //starScore -= stage.grade[0];
                starScore = (float)(realScore - stage.grade[1]) / (stage.grade[2] - stage.grade[1]);
                starScore += 2f;
            }
            else if (EarnStars == 3)
            {
                //starScore -= stage.grade[0];
                starScore = (float)(realScore - stage.grade[1]) / (stage.grade[2] - stage.grade[1]);
                starScore += 2f;
            }
            form.AddField("money_ping", (int)(starScore * 100));
        }

        System.Collections.Generic.List<RankData> rankArray = StageReader.GetInstance().GetStage().rankArray;
        //Debug.Log("prevScore = " + GameManager.GetInstance().prevScore);
        //Debug.Log("score = " + score * 10);
        if (/*GameManager.GetInstance().prevScore < score * 10 && */cleared == true)
        {
			if (StageReader.GetInstance ().lastStageIndex == StageReader.GetInstance ().LockedStage)
			{
                //Debug.Log("lastStage");
				StageReader.GetInstance().lastStageIndex++;
                form.AddField("top_rank", "true");
				isGoToNextStage = true;
			}

            if (GameManager.GetInstance().isRanked == true)
            {
                for (int i = 0; i < rankArray.Count; i++)
                {
                    if (rankArray[i].id.ToString() == id)
                    {
                        rankArray[i].score = realScore;
                        break;
                    }
                }
            }
            else
            {
                int id_int;
                int.TryParse(id, out id_int);
                rankArray.Add(new RankData(id_int, score * 10));
            }
            rankArray.Sort(new ScoreComparer());
        }

        WWW www = new WWW(url, form);
        StartCoroutine(WaitForSend(www));
		/*
        GameObject.Find("rankingContent").GetComponent<RankingScript>().Init();
        GameObject.Find("rankingContent").GetComponent<RankingScript>().ApplyRanking(rankArray);
        GameObject.Find("rankingContent").GetComponent<RankingScript>().ShowScreen();
        *//*
        if (GameManager.GetInstance().isRanked == true && cleared == true && GameManager.GetInstance().prevScore < score * 10)  //add data
        {
            int myRank = 0;
            int upRank = 0;
            for (int i = 0; i < rankArray.Count; i++)
            {
                if (rankArray[i].id.ToString() == SecurityIO.GetString("id"))
                {
                    rankArray[i].score = score * 10;
                    myRank = i;
                    break;
                }
            }
            rankArray.Sort(new ScoreComparer());

            for (int i = 0; i < rankArray.Count; i++)
            {
                if (rankArray[i].id.ToString() == SecurityIO.GetString("id"))
                {
                    upRank = myRank - i;
                    myRank = i;
                    break;
                }
            }
            if( upRank == 0)
            {
                GameObject.Find("second").guiText.text = "- " + (myRank + 1).ToString() + "위 " + rankArray[myRank].score.ToString();
                StartCoroutine(WaitForProfile(new WWW("https://rrgames.me:3000/users/" + rankArray[myRank].id.ToString() + "/?token=" + SecurityIO.GetString("token")), 1));
                if( rankArray.Count > myRank+1)
                {
                    GameObject.Find("third").guiText.text = "-1 " + myRank + 2.ToString() + "위 " + rankArray[myRank + 1].score.ToString();
                    StartCoroutine(WaitForProfile(new WWW("https://rrgames.me:3000/users/" + rankArray[myRank + 1].id.ToString() + "/?token=" + SecurityIO.GetString("token")), 2));
                }
            }
            if(myRank != 0)
            {
                GameObject.Find("first").guiText.text = "- " + myRank.ToString() + "위 " + rankArray[myRank - 1].score.ToString();
                StartCoroutine(WaitForProfile(new WWW("https://rrgames.me:3000/users/" + rankArray[myRank-1].id.ToString() + "/?token=" + SecurityIO.GetString("token")), 0));
            }

        }*/
        /*
    else if (GameManager.GetInstance().isRanked == false && cleared == false)   //unknown
    {
        if( rankArray.Count != 0)
        {
            GameObject.Find("first").guiText.text = "- " + rankArray.Count + "위 " + rankArray[rankArray.Count - 1].score.ToString();
            StartCoroutine(WaitForProfile(new WWW("https://rrgames.me:3000/users/" + rankArray[rankArray.Count - 1].id.ToString() + "/?token=" + SecurityIO.GetString("token")), 0));
        }
        else
        {
            GameObject.Find("first").guiText.text = "";
        }
        GameObject.Find("second").guiText.text = "- Fail";
        StartCoroutine(WaitForProfile(new WWW("https://rrgames.me:3000/users/" + SecurityIO.GetString("id") + "/?token=" + SecurityIO.GetString("token")), 1));
        GameObject.Find("third").guiText.text = "";

    }*/
        /*
    else if (GameManager.GetInstance().isRanked == false && cleared == true)//show data
    {
        int id;
        int.TryParse(SecurityIO.GetString("id"), out id);
        rankArray.Add(new RankData(id, score*10));
        rankArray.Sort(new ScoreComparer());

        for (int i = 0; i < rankArray.Count; i++)
        {
            if (rankArray[i].id.ToString() == SecurityIO.GetString("id"))
            {
                id = i;
                break;
            }
        }
        if( id != 0)
        {
            GameObject.Find("first").guiText.text = "- " + id + "위 " + rankArray[id - 1].score.ToString();
            StartCoroutine(WaitForProfile(new WWW("https://rrgames.me:3000/users/" + rankArray[id - 1].id.ToString() + "/?token=" + SecurityIO.GetString("token")), 0));
        }
        else
        {
            GameObject.Find("first").guiText.text = "";
        }
        GameObject.Find("second").guiText.text = "N " + (id + 1).ToString() + "위 " + (score * 10).ToString();
        StartCoroutine(WaitForProfile(new WWW("https://rrgames.me:3000/users/" + rankArray[id].id.ToString() + "/?token=" + SecurityIO.GetString("token")), 1));
        if( rankArray.Count > id + 1)
        {
            GameObject.Find("third").guiText.text = "-1 " + (id + 2).ToString() + "위 " + rankArray[id + 1].score.ToString();
            StartCoroutine(WaitForProfile(new WWW("https://rrgames.me:3000/users/" + rankArray[id + 1].id.ToString() + "/?token=" + SecurityIO.GetString("token")), 2));
        }
        else
        {
            GameObject.Find("third").guiText.text = "";
        }
    }*/
        /*
    else
    {
        int myRank = 0;
        for (int i = 0; i < rankArray.Count; i++)
        {
            if (rankArray[i].id.ToString() == SecurityIO.GetString("id"))
            {
                myRank = i;
                break;
            }
        }
        if (myRank != 0)
        {
            GameObject.Find("first").guiText.text = "- " + myRank + "위 " + rankArray[myRank - 1].score.ToString();
            StartCoroutine(WaitForProfile(new WWW("https://rrgames.me:3000/users/" + rankArray[myRank - 1].id.ToString() + "/?token=" + SecurityIO.GetString("token")), 0));
        }
        GameObject.Find("second").guiText.text = "- " + (myRank + 1).ToString() + "위 " + rankArray[myRank].score.ToString();
        StartCoroutine(WaitForProfile(new WWW("https://rrgames.me:3000/users/" + rankArray[myRank].id.ToString() + "/?token=" + SecurityIO.GetString("token")), 1));
        if (rankArray.Count > myRank + 1)
        {
            GameObject.Find("third").guiText.text = "- " + (myRank + 2).ToString() + "위 " + rankArray[myRank + 1].score.ToString();
            StartCoroutine(WaitForProfile(new WWW("https://rrgames.me:3000/users/" + rankArray[myRank + 1].id.ToString() + "/?token=" + SecurityIO.GetString("token")), 2));
        }
    }*/
        /*
        if (GameManager.GetInstance().isRanked)
        {
            if (GameManager.GetInstance().prevScore <= score * 10 && cleared == true)
            {
                int myRank = 0;
                int upRank = 0;
                for (int i = 0; i < StageReader.GetInstance().GetStage().rankArray.Count; i++)
                {
                    if (StageReader.GetInstance().GetStage().rankArray[i].id.ToString() == SecurityIO.GetString("id"))
                    {
                        StageReader.GetInstance().GetStage().rankArray[i].score = score * 10;
                        myRank = i;
                        break;
                    }
                }
                StageReader.GetInstance().GetStage().rankArray.Sort(new ScoreComparer());

                for (int i = 0; i < StageReader.GetInstance().GetStage().rankArray.Count; i++)
                {
                    if (StageReader.GetInstance().GetStage().rankArray[i].id.ToString() == SecurityIO.GetString("id"))
                    {
                        upRank = myRank - i;
                        myRank = i;
                        break;
                    }
                }

                if (upRank == 0)
                {
                    GameObject.Find("second").guiText.text = "-";
                }
                else
                {
                    GameObject.Find("second").guiText.text = "+" + upRank.ToString();
                    if (myRank + 1 < StageReader.GetInstance().GetStage().rankArray.Count)
                    {
                        GameObject.Find("third").guiText.text = "-1 "+myRank+2.ToString()+"위 "+ StageReader.GetInstance().GetStage().rankArray[myRank+1];
                    }
                }

                Debug.Log(myRank.ToString());
                GameObject.Find("second").guiText.text = GameObject.Find("second").guiText.text + " " + (myRank + 1).ToString() + "위 " + StageReader.GetInstance().GetStage().rankArray[myRank].score.ToString();

                if (myRank != 0)
                {
                    GameObject.Find("first").guiText.text = "- " + myRank.ToString() + "위 " + StageReader.GetInstance().GetStage().rankArray[myRank-1];
                }
            }
            else if( cleared == false && GameManager.GetInstance().isRanked == false)
            {

            }
            else
            {
                int myRank = 0;
                for (int i = 0; i < StageReader.GetInstance().GetStage().rankArray.Count; i++)
                {
                    if (StageReader.GetInstance().GetStage().rankArray[i].id == -1)
                    {
                        myRank = i;
                        break;
                    }
                }
                if( myRank != 0)
                {
                    GameObject.Find("first").guiText.text = "- " + (myRank).ToString() + "위 " + StageReader.GetInstance().GetStage().rankArray[myRank - 1].score.ToString();
                }
                GameObject.Find("second").guiText.text = "- " + (myRank + 1).ToString() + "위 " + StageReader.GetInstance().GetStage().rankArray[myRank].score.ToString();
                if(myRank+1 < StageReader.GetInstance().GetStage().rankArray.Count)
                {
                    GameObject.Find("third").guiText.text = "- " + (myRank + 2).ToString() + "위 " + StageReader.GetInstance().GetStage().rankArray[myRank + 1].score.ToString();
                }
                
                int myRank = 0;
                for (int i = 0; i < StageReader.GetInstance().GetStage().rankArray.Count; i++)
                {
                    if (StageReader.GetInstance().GetStage().rankArray[i].id.ToString() == SecurityIO.GetString("id"))
                    {
                        StageReader.GetInstance().GetStage().rankArray[i].score = score * 10;
                        myRank = i;
                        break;
                    }
                }
                if( myRank != 0)
                {
                    GameObject.Find("first").guiText.text = "- " + (myRank).ToString() + "위 " + StageReader.GetInstance().GetStage().rankArray[myRank-1].score.ToString();
                }
                GameObject.Find("second").guiText.text = "- " + (myRank + 1).ToString() + "위 " + StageReader.GetInstance().GetStage().rankArray[myRank].score.ToString();
                if (myRank + 1 < StageReader.GetInstance().GetStage().rankArray.Count)
                {
                    GameObject.Find("third").guiText.text = "- " + (myRank + 2).ToString() + "위 " + StageReader.GetInstance().GetStage().rankArray[myRank+1].score.ToString();
                }
            }
        }
    
        else
        {
            if( cleared == true)
            {
                StageReader.GetInstance().GetStage().rankArray.Add(new RankData(-1, score * 10));
                StageReader.GetInstance().GetStage().rankArray.Sort(new ScoreComparer());
                int myRank = 0;
                for (int i = 0; i < StageReader.GetInstance().GetStage().rankArray.Count; i++)
                {
                    if (StageReader.GetInstance().GetStage().rankArray[i].id == -1)
                    {
                        myRank = i;
                        break;
                    }
                }
                if( myRank != 0)
                {
                    GameObject.Find("first").guiText.text = "- " + (myRank).ToString() + "위 " + StageReader.GetInstance().GetStage().rankArray[myRank - 1].score.ToString();
                }
                GameObject.Find("second").guiText.text = "N " + (myRank + 1).ToString() + "위 " + StageReader.GetInstance().GetStage().rankArray[myRank].score.ToString();
                if(myRank+1 < StageReader.GetInstance().GetStage().rankArray.Count)
                {
                    GameObject.Find("third").guiText.text = "-1 " + (myRank + 2).ToString() + "위 " + StageReader.GetInstance().GetStage().rankArray[myRank + 1].score.ToString();
                }
            }
            else
            {

            }
        }*/

    }
    // Use this for initialization
    void Start()
    {
        
        audio.clip = bgm;
        audio.loop = true;
        if (isBGM == true)
        {
            audio.Play();
        }
    }

    // Update is called once per frame
    void Update()
    {
        waitTime += Time.deltaTime;
        if( isStarted == true && isLoaded == false)
        {
            bool complete = true;
            for( int i=0; i<rankList.Count; i++)
            {
                if(rankList[i].loaded == false)
                {
                    complete = false;
                    break;
                }
            }
            if(complete == true)
            {
                isLoaded = true;
                if (rankList.Count != 0)
                    loadingLabel.text = "";
                for (int i = 0; i < rankList.Count; i++)
                {
                    rankList[i].UnHide();
                }
            }
        }
    }

	public void ShareButtonTouched()
    {
        if (isLoaded != true && waitTime < 3f)
            return;
        if( isSFX == true)
        {
            audio.PlayOneShot(buttonSound);
        }
        shareLabel.color = new Color(0.5f, 0.5f, 0.5f, 1f);
        Application.LoadLevel("TitleScene");
        /*
		if (uploadRecord && uploaded == false)
		{
			uploaded = true;
			url += "true";
			WWW www = new WWW(url, form);
			StartCoroutine(WaitForSend(www));
			Debug.Log(url);
		}*/
	}

	public void NextButtonTouched()
    {
        if (isLoaded != true && waitTime < 3f)
            return;
        if (isSFX == true)
        {
            audio.PlayOneShot(buttonSound);
        }
        nextLabel.color = new Color(0.5f, 0.5f, 0.5f, 1f);
        /*
		if (uploadRecord && uploaded == false)
		{
			uploaded = true;
			url += "false";
			WWW www = new WWW(url, form);
			StartCoroutine(WaitForSend(www));
			Debug.Log(url);
		}*/
		if( isGoToNextStage == true)
		{
            //Debug.Log("FIRST");
			if( StageReader.GetInstance().stageArray.Count > StageReader.GetInstance().lastStageIndex)
            {
                //Debug.Log("SECOND");
                StageReader.GetInstance().LockStage(StageReader.GetInstance().lastStageIndex);
                Application.LoadLevel("GameScene");
                //Application.LoadLevel("TitleScene");
                return;
			}
            else
            {
                Application.LoadLevel("TitleScene");
                return;
            }
        }
        else if( isClear == true)
        {
            if (StageReader.GetInstance().stageArray.Count > StageReader.GetInstance().LockedStage + 1)
            {
                StageReader.GetInstance().LockStage(StageReader.GetInstance().LockedStage + 1);
                Application.LoadLevel("GameScene");
                return;
            }
            else
            {
                Application.LoadLevel("TitleScene");
                return;
            }
        }
        else
        {
            Application.LoadLevel("GameScene");
            return;
        }
	}

    void OnGUI()
    {
		/*
        if (GUI.Button(new Rect(Screen.width / 40f * 30.7f, Screen.height / 7f * 4f, Screen.width / 7f, Screen.height / 17f), "공유"))
        {
        }
        if (GUI.Button(new Rect(Screen.width / 40f * 24.7f, Screen.height / 7f * 4f, Screen.width / 7f, Screen.height / 17f), "확인"))
        {
        }*/
    }
}
