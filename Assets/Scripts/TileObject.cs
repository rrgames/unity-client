﻿using UnityEngine;
using System.Collections;

public abstract class Tile
{
	public enum TileType{ Unuse, Common, Wall};

	public TileType type{ get; protected set;}
	public bool canHaveBlock{ get; protected set;}
	public bool nowHaveBlock{ get; protected set;}

	protected Tile()
	{

	}
}

public abstract class GoodTile : Tile
{
	public Block block{ get; protected set; }

	protected GoodTile() : base()
	{
		canHaveBlock = true;
		nowHaveBlock = false;
	}
}

public abstract class BadTile : Tile
{
	protected BadTile() : base()
	{
		canHaveBlock = false;
		nowHaveBlock = false;
	}
}

public class CommonTile : GoodTile
{
	public CommonTile() : base()
	{
		type = TileType.Common;
	}
}
