﻿using UnityEngine;
using System.Collections;
using System.Security.Cryptography;

public class StageInfo
{
    public string fileName;
    public string hash;
    public string updateTime;
    public bool updated;

    public StageInfo()
    {
        fileName = "";
        hash = "";
        updateTime = "";
        updated = false;
    }
}

public class SecurityIO
{
    static System.Collections.Generic.List<StageInfo> result;
    public static System.Collections.Generic.List<StageInfo> GetStageList()
    {
        if( result != null)
        {
            return result;
        }
        bool error = false;
        result = new System.Collections.Generic.List<StageInfo>();
        System.Xml.XmlDocument doc = new System.Xml.XmlDocument();


        if (System.IO.File.Exists(Application.persistentDataPath + "/dump.xml"))
        {
            doc.Load(Application.persistentDataPath + "/dump.xml");
        }
        else
        {
            error = true;
        }
        /*
        try
        {
            doc.Load(Application.persistentDataPath + "/dump.xml");
        }
        catch (System.IO.FileNotFoundException)
        {
            error = true;
        }*/

        if( error == false)
        {
            if(doc.SelectSingleNode("data").InnerText.Equals("loading"))
            {
                error = true;
            }
        }

        if( error == true)
        {
            doc.LoadXml("<data></data>");
            doc.Save(Application.persistentDataPath + "/MD5.xml");
        }
        else
        {

            if (System.IO.File.Exists(Application.persistentDataPath + "/MD5.xml"))
            {
                doc.Load(Application.persistentDataPath + "/MD5.xml");
            }
            else
            {
                doc.LoadXml("<data></data>");
                doc.Save(Application.persistentDataPath + "/MD5.xml");
            }
            /*
            try
            {
                doc.Load(Application.persistentDataPath + "/MD5.xml");
            }
            catch (System.IO.FileNotFoundException)
            {
                doc.LoadXml("<data></data>");
                doc.Save(Application.persistentDataPath + "/MD5.xml");
            }*/
        }

        System.Xml.XmlNode root = doc.SelectSingleNode("data");
        foreach(System.Xml.XmlNode node in root.ChildNodes)
        {
            StageInfo stage = new StageInfo();
            stage.fileName = node.SelectSingleNode("./name").InnerText;
            stage.hash = node.SelectSingleNode("./hash").InnerText;
            stage.updateTime = node.SelectSingleNode("./time").InnerText;
            result.Add(stage);
        }
        return result;
    }

    public static void SaveStageFile(string content, string updateTime)
    {
        string filename;
        System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
        doc.LoadXml(content);
        filename = doc.SelectSingleNode("stage/name").InnerText;
        doc.Save(Application.persistentDataPath + "/" + filename);

        Debug.Log(filename);

        doc = new System.Xml.XmlDocument();

        if (System.IO.File.Exists(Application.persistentDataPath + "/MD5.xml"))
        {
            doc.Load(Application.persistentDataPath + "/MD5.xml");
        }
        else
        {
            doc.LoadXml("<data></data>");
            doc.Save(Application.persistentDataPath + "/MD5.xml");
        }
        /*
        try
        {
            doc.Load(Application.persistentDataPath + "/MD5.xml");
        }
        catch (System.IO.FileNotFoundException)
        {
            doc.LoadXml("<data></data>");
            doc.Save(Application.persistentDataPath + "/MD5.xml");
        }*/

        System.Xml.XmlNode root = doc.SelectSingleNode("data");
        bool sw = false;
        foreach (System.Xml.XmlNode node in root.ChildNodes)
        {
            if(node.SelectSingleNode("./name").InnerText.Equals(filename))
            {
                node.SelectSingleNode("./hash").InnerText = GetHash(content);
                sw = true;
            }
        }
        if( sw == false)
        {
            Debug.Log(filename);
            Debug.Log(content);
            Debug.LogError("Exception");
        }
        doc.Save(Application.persistentDataPath + "/MD5.xml");
    }

    public static void UpdateComplete()
    {
        System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
        doc.LoadXml("<data>loaded</data>");
        doc.Save(Application.persistentDataPath + "/dump.xml");
    }

    public static System.Collections.Generic.List<StageInfo> UpdateStageList(System.Collections.Generic.List<StageInfo> list)
    {
        GetStageList();
        bool sw;
        for (int i = 0; i < list.Count; i++ )
        {
            sw = false;
            for (int j = 0; j< result.Count; j++)
            {
                
                if( result[j].updated == true)
                {
                    continue;
                }
                
                else
                {
                    if(result[j].fileName.Equals(list[i].fileName))
                    {
                        sw = true;
                        if( result[j].updateTime.Equals(list[i].updateTime))
                        {

                            //Debug.Log(result[j].updateTime + " " + list[i].updateTime);
                            //Debug.Log("EQUAL");
                            result[j].updated = true;
                            list[i].updated = true;
                            list[i].hash = result[j].hash;
                        }
                        else
                        {
                            Debug.Log("NO");
                            result[j].updated = false;
                            list[i].updated = false;
                        }
                        break;
                    }
                }
            }
            if( sw == false)
            {
                list[i].updated = false;
            }
        }

        result = list;
        
        System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
        doc.LoadXml("<data></data>");
        System.Xml.XmlNode root = doc.SelectSingleNode("data");
        for (int i = 0; i < result.Count; i++ )
        {
            System.Xml.XmlNode stage = doc.CreateElement("stage");

            System.Xml.XmlNode name = doc.CreateElement("name");
            System.Xml.XmlNode hash = doc.CreateElement("hash");
            System.Xml.XmlNode time = doc.CreateElement("time");

            name.InnerText = result[i].fileName;
            hash.InnerText = result[i].hash;
            time.InnerText = result[i].updateTime;

            stage.AppendChild(name);
            stage.AppendChild(hash);
            stage.AppendChild(time);

            root.AppendChild(stage);
        }
        doc.Save(Application.persistentDataPath + "/MD5.xml");
        

        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].updated == true)
            {
//                Debug.Log("DELETED");
                list.RemoveAt(i);
                i--;
            }
        }

        if( list.Count > 0)
        {
            doc.LoadXml("<data>loading</data>");
            doc.Save(Application.persistentDataPath + "/dump.xml");
        }
        else
        {
            doc.LoadXml("<data>loaded</data>");
            doc.Save(Application.persistentDataPath + "/dump.xml");
        }

//        Debug.Log(list.Count.ToString());
//        Debug.Log(result.Count.ToString());
        return list;
    }

    public static string GetHash(string content)
    {
        MD5 hash = MD5.Create();
        byte[] hashData = hash.ComputeHash(System.Text.Encoding.UTF8.GetBytes(content));
        return System.Text.Encoding.UTF8.GetString(hashData);
    }


    public static void StageRead()
    {
        Reload();
        for( int i=0; i<result.Count; i++)
        {
            string content = System.IO.File.ReadAllText(Application.persistentDataPath + "/"+result[i].fileName);
            StageReader.GetInstance().LoadStage(content, result[i].fileName);
        }
    }

    public static System.Collections.Generic.List<StageInfo> Reload()
    {
        if (result != null)
            result.Clear();
        result = new System.Collections.Generic.List<StageInfo>();
        System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
        if( System.IO.File.Exists(Application.persistentDataPath + "/dump.xml"))
        {
            doc.Load(Application.persistentDataPath + "/dump.xml");
            if (doc.SelectSingleNode("data").InnerText.Equals("loading"))
            {
                return result;
            }
        }
        else
        {
            return result;
        }
        /*
        try
        {
        }
        catch (System.IO.FileNotFoundException)
        {
            return result;
        }*/

        if (System.IO.File.Exists(Application.persistentDataPath + "/MD5.xml"))
        {
            doc.Load(Application.persistentDataPath + "/MD5.xml");
        }
        else
        {
            return result;
        }
        /*
        try
        {
            doc.Load(Application.persistentDataPath + "/MD5.xml");
        }
        catch (System.IO.FileNotFoundException)
        {
            return result;
        }*/

        System.Xml.XmlNode root = doc.SelectSingleNode("data");
        foreach (System.Xml.XmlNode node in root.ChildNodes)
        {
            StageInfo stage = new StageInfo();
            stage.fileName = node.SelectSingleNode("./name").InnerText;
            stage.hash = node.SelectSingleNode("./hash").InnerText;
            stage.updateTime = node.SelectSingleNode("./time").InnerText;
            result.Add(stage);
        }
        return result;
    }
    
    public static void SetString(string _key, string _value)
    {
        MD5 md5Hash = MD5.Create();
        string userName = "dlalsgur";
        MD5 nameHash = new MD5CryptoServiceProvider();
        byte[] secret = nameHash.ComputeHash(System.Text.Encoding.UTF8.GetBytes(userName));
        byte[] hashData = md5Hash.ComputeHash(System.Text.Encoding.UTF8.GetBytes(_key));
        string hashKey = System.Text.Encoding.UTF8.GetString(hashData);
        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(_value);
        TripleDES des = new TripleDESCryptoServiceProvider();
        des.Key = secret;
        des.Mode = CipherMode.ECB;
        ICryptoTransform xform = des.CreateEncryptor();
        byte[] encrypted = xform.TransformFinalBlock(bytes, 0, bytes.Length);
        string encryptedString = System.Convert.ToBase64String(encrypted);
        PlayerPrefs.SetString(hashKey, encryptedString);
    }

    public static string GetString(string _key)
    {
        /*
        if (_key == "token")
            return "360d1fa7995ddfac74a1315ae439e88d9aa6ca2c";
        else if (_key == "id")
            return "3";*/
#if UNITY_EDITOR
        if (_key == "token")
            return "1c204b6e0ff657b5dbffab0eed9081210ce2b1a6";
            //return "64c9b6f4102bfc17e70754a66e5627aa4b82cb87";
        else if (_key == "id")
            return "3";
#endif
        MD5 md5Hash = MD5.Create();
        string userName = "dlalsgur";
        MD5 nameHash = new MD5CryptoServiceProvider();
        byte[] secret = nameHash.ComputeHash(System.Text.Encoding.UTF8.GetBytes(userName));
        byte[] hashData = md5Hash.ComputeHash(System.Text.Encoding.UTF8.GetBytes(_key));
        string hashKey = System.Text.Encoding.UTF8.GetString(hashData);
        string _value = PlayerPrefs.GetString(hashKey);
        if (_value.Length == 0)
            return "";
        byte[] bytes = System.Convert.FromBase64String(_value);
        TripleDES des = new TripleDESCryptoServiceProvider();
        des.Key = secret;
        des.Mode = CipherMode.ECB;
        ICryptoTransform xform = des.CreateDecryptor();
        byte[] decrypted = xform.TransformFinalBlock(bytes, 0, bytes.Length);
        string decryptedString = System.Text.Encoding.UTF8.GetString(decrypted);
        return decryptedString;
    }
    public static string ReplayCompress(string source)
    {
        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(source);
        using (System.IO.MemoryStream outMemoryStream = new System.IO.MemoryStream())
        using (zlib.ZOutputStream outZStream = new zlib.ZOutputStream(outMemoryStream, zlib.zlibConst.Z_DEFAULT_COMPRESSION))
        using (System.IO.Stream inMemoryStream = new System.IO.MemoryStream(bytes))
        {
            CopyStream(inMemoryStream, outZStream);
            outZStream.finish();
            bytes = outMemoryStream.ToArray();
        }
        return System.Convert.ToBase64String(bytes);
    }

    public static string ReplayDecompress(string source)
    {
        byte[] bytes = System.Convert.FromBase64String(source);
        using (System.IO.MemoryStream outMemoryStream = new System.IO.MemoryStream())
        using (zlib.ZOutputStream outZStream = new zlib.ZOutputStream(outMemoryStream))
        using (System.IO.Stream inMemoryStream = new System.IO.MemoryStream(bytes))
        {
            CopyStream(inMemoryStream, outZStream);
            outZStream.finish();
            bytes = outMemoryStream.ToArray();
        }
        return System.Text.Encoding.UTF8.GetString(bytes);
    }

    public static void CopyStream(System.IO.Stream input, System.IO.Stream output)
    {
        byte[] buffer = new byte[2000];
        int len;
        while ((len = input.Read(buffer, 0, 2000)) > 0)
        {
            output.Write(buffer, 0, len);
        }
        output.Flush();
    }
}

// http://unitystudy.net/bbs/board.php?bo_table=newwriting&wr_id=355