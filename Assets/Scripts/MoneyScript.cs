﻿using UnityEngine;
using System.Collections;
using LitJson;

public class MoneyScript : MonoBehaviour
{
    public int moneyKind;

	public UILabel money;
	//public UILabel cash;

    void Awake()
    {
        money.text = SecurityIO.GetString("money");
        //cash.text = SecurityIO.GetString("cash");
    }

    // Use this for initialization
    void Start()
    {
        SyncToServer();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SyncToServer()
    {
		StartCoroutine(WaitForMoney(new WWW("https://rrgames.me:3000/users/" + SecurityIO.GetString("id") + "/?token=" + SecurityIO.GetString("token"))));
    }

	/*
    void DecreaseMoney()
    {

    }

    void IncreaseMoney()
    {

    }
*/
    private IEnumerator WaitForMoney(WWW www)
    {
		yield return www;
		if(www.error == null)
		{
//			Debug.Log(www.text);
			string moneyStr = "0";
			//string cashStr = "0";
			JsonReader reader = new JsonReader(www.text);
			while (reader.Read())
			{
				if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "money_ping")
				{
					reader.Read();
					moneyStr = reader.Value.ToString();
				}
				//else if( reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "money_jjo")
				//{
				//	reader.Read();
				//	cashStr = reader.Value.ToString();
				//}
			}

			money.text = moneyStr;
			//cash.text = cashStr;

			RequestManager.GetInstance().RegisterMoney(moneyStr, "0");
		}
        yield break;
    }
}
