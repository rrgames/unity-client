﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour {
	public int x{ get; set;}
	public int y{ get; set;}
	public int option{ get; set;}
    public int type { get; set; }
    public int item { get; set; }
	public float speed{ get; set;}
    public bool isDie;

    public bool isDrop;
    public float correctY;

    void Awake()
    {
        isDrop = false;
    }

	void Start () {

	}
	
	void Update () {
	
	}

    public void PopBlock()
    {
        gameObject.GetComponent<BoxCollider>().enabled = false;
        if(gameObject.transform.childCount != 0)
        {
            for( int i=0; i<gameObject.transform.childCount; i++)
            {
                GameObject.Destroy(gameObject.transform.GetChild(i).gameObject);
            }
        }
        StartCoroutine(WaitForGone());
    }

    public void DropBlock()
    {
        if (isDrop == false)
        {
            StartCoroutine(WaitForDrop());
        }
    }

    public void UpdateHealth()
    {
        GameObject healthFont = gameObject.transform.FindChild("health").gameObject;
        if( healthFont != null)
        {
            healthFont.GetComponent<TextMesh>().text = option.ToString();
        }
    }

    private IEnumerator WaitForDrop()
    {
        isDrop = true;
        float speed = 2.7f;
        while(true)
        {
            float position = gameObject.transform.localPosition.y + speed * Time.deltaTime;
            if(position < correctY)
            {
                gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x, correctY);
                break;
            }
            else
            {
                gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x, position);
            }
            speed -= Time.deltaTime * 15f;
            yield return null;
        }
        isDrop = false;
        yield break;
    }

    private IEnumerator WaitForGone()
    {
        isDie = true;
        gameObject.collider.enabled = false;
        float startTime = Time.time;
        yield return null;
        while(true)
        {
            float time = Time.time - startTime;
            if (0.5f < time)
            {

                MonoBehaviour.Destroy(gameObject);
                break;
            }
            else
            {
                float progress = time / 0.5f;
                gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, Mathf.Lerp(1f, 0f, progress));
            }
            yield return null;
        }
        yield break;
    }
}
