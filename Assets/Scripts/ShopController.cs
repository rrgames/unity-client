﻿using UnityEngine;
using System.Collections;
using LitJson;

public class ShopController : MonoBehaviour {

    public UIPanel goodsPanel;
    public UITexture background;
    public UITexture closeButton;
    public UITexture closeButtonFrame;
    public UILabel windowTitle;
    public UIGrid goodsParent;
    public AudioClip purchaseSound;

    public UILabel completeTexture1;
    public UILabel completeTexture2;
    public UILabel completeTexture3;

    public MoneyScript money;

	private System.Collections.Generic.List<Goods> goodsList;

    public bool isEnabled;

    private int gatlingIndex;
    private UILabel[] gatlingLabel;
    private bool[] gatlingFlag;

    private bool isInit;
    private bool isPopupOpened;
    private int selectedItem;
    private TitleScene parentScene;

	void Awake()
	{

	}

	void Start () {
        selectedItem = -1;
	}
	
	// Update is called once per frame
	void Update ()
    {

	}

	public void Init(System.Collections.Generic.List<ShopItem> shopList, TitleScene scene)
    {
        gatlingLabel = new UILabel[3];
        gatlingLabel[0] = completeTexture1;
        gatlingLabel[1] = completeTexture2;
        gatlingLabel[2] = completeTexture3;
        gatlingFlag = new bool[3];
        gatlingIndex = 0;
        isInit = true;
        parentScene = scene;
		goodsList = new System.Collections.Generic.List<Goods> ();
		GameObject goodsPrefab = Resources.Load("Prefab/GoodsNGUI") as GameObject;
		for( int i=0; i<shopList.Count; i++)
		{
			GameObject instance = MonoBehaviour.Instantiate(goodsPrefab) as GameObject;
            instance.name = i.ToString();
            instance.transform.parent = goodsParent.transform;
            instance.transform.localScale = new Vector3(1f, 1f, 1f);
            
			Goods component = instance.GetComponent<Goods>();
            component.TitleModify(shopList[i].name);
            component.PriceModify(shopList[i].price, shopList[i].isCash);
            component.SetId(shopList[i].id);
            component.SetIndex(i);
            component.SetImage(shopList[i].image);
            component.ButtonInit(this);

            /*
			component.PriceModify(shopList[i].price, shopList[i].isCash);
			component.TitleModify(shopList[i].name);
			component.GreenLightModify(false);
			component.SetEnable(false);
			component.SetId(shopList[i].id);
            component.index = i;
            instance.name = "Goods";
//            instance.transform.parent = parent.transform;
			instance.transform.localPosition = new Vector3(-1.75f + (i * 1.75f),0f,0f);
            */
			goodsList.Add(component);
        }
        goodsParent.Reposition();
	}

    private IEnumerator WaitForPurchase(WWW www)
    {
        yield return www;

        if( www.error == null)
        {
            money.SyncToServer();
        }
        else
        {
            Debug.Log(www.error);
        }
    }

    private void PurchaseCompleteGatling()
    {
        StartCoroutine(Gatling(gatlingIndex));
        gatlingIndex++;
        if (gatlingIndex > 2)
            gatlingIndex = 0;
    }

    private IEnumerator Gatling(int index)
    {
        gatlingFlag[index] = !gatlingFlag[index];
        bool temp = gatlingFlag[index];
        float time = 0f;
        gatlingLabel[index].alpha = 0f;
        gatlingLabel[index].enabled = true;
        while(temp == gatlingFlag[index])
        {
            time += Time.deltaTime;
            if( time > 0.2f)
            {
                gatlingLabel[index].alpha = 1f - (time - 0.2f) * 2.5f;
                gatlingLabel[index].transform.localPosition = new Vector3(0f, -180f, 0f);
            }
            else
            {
                gatlingLabel[index].alpha = time * 5f;
                gatlingLabel[index].transform.localPosition = new Vector3(0f, -210f + time * 150f, 0f);
            }
            if( time > 0.6f)
            {
                gatlingLabel[index].enabled = false;
                break;
            }
            yield return null;
        }
        yield break;
    }

    public void ProductTouched(int index)
    {
        if( isEnabled == false)
        {
            return;
        }
        if( isPopupOpened == true && selectedItem == index)
        {
            WWWForm form = new WWWForm();
            form.AddField("item_id", goodsList[index].id);
            form.AddField("quantity", "1");
           // Debug.Log(goodsList[index].id.ToString());
            PurchaseCompleteGatling();
            StartCoroutine(WaitForPurchase(new WWW("https://rrgames.me:3000/market/buy?token="+SecurityIO.GetString("token"),form)));
            parentScene.audio.PlayOneShot(purchaseSound);//purchase
            CloseWindow();
        }
        else
        {
            if( selectedItem != -1)
            {
                CloseWindow();
            }
            selectedItem = index;
            OpenWindow();
        }
    }

    private void OpenWindow()
    {
        goodsList[selectedItem].OpenPurchase();
        isPopupOpened = true;
    }

    private void CloseWindow()
    {
        goodsList[selectedItem].ClosePurchase();
        isPopupOpened = false;
        selectedItem = -1;
    }
    private void Purchase()
    {

    }
	public void ShopEnable()
    {
        if (isEnabled == true)
            return;
        goodsPanel.gameObject.SetActive(true);
        this.GetComponent<UITexture>().enabled = true;
        windowTitle.enabled = true;
        background.enabled = true;
        closeButton.enabled = true;
        closeButtonFrame.enabled = true;
        closeButton.collider.enabled = true;
        completeTexture1.enabled = false;
        completeTexture2.enabled = false;
        completeTexture3.enabled = false;
        isEnabled = true;

        for( int i=0; i<goodsList.Count; i++)
        {
            goodsList[i].ImageLoad();
        }
	}

	public void ShopDisable()
	{
        if (isEnabled == false)
            return;
        this.GetComponent<UITexture>().enabled = false;
        windowTitle.enabled = false;
        background.enabled = false;
        closeButton.enabled = false;
        closeButton.collider.enabled = false;
        closeButtonFrame.enabled = false;
        completeTexture1.enabled = false;
        completeTexture2.enabled = false;
        completeTexture3.enabled = false;
        isEnabled = false;
        goodsPanel.gameObject.SetActive(false);
	}
}
