﻿using UnityEngine;
using System.Collections;
using LitJson;

public class IntroScene : MonoBehaviour {

	
	public enum State
	{
		FB_INIT = 0,
		FB_LOGIN,
		SV_SESSION,
		SV_RECORD,
		SV_MONEY,
		COMPLETE,
		ERROR,
		TESTING,
	}


	public UIProgressBar progressBar;
	public UILabel progressLabel;
	public UIButton facebookConnect;

    private System.Collections.Generic.List<StageInfo> stageList;

	private int loadingCount;
	private int loadedCount;
	private State nowState;

    private float timer;
    private bool isStageInit;

//    private int progress;

    private float barProgress;
    private float realProgress;


	public void FacebookLoginButtonTouched()
	{
		if (nowState == State.ERROR)
		{
			nowState = State.TESTING;
			StartCoroutine(WaitForNetworkTest(new WWW("http://www.google.com")));
		}

		else
		{
			if( FB.IsInitialized == true && FB.IsLoggedIn == false)
				FB.Login();
		}
	}

	private IEnumerator WaitForMoney(WWW www)
	{
		yield return www;
		if(www.error == null)
		{
//			Debug.Log(www.text);
			string moneyStr = "0";
			string cashStr = "0";
			string lastStage = "1-1";
			JsonReader reader = new JsonReader(www.text);
			while (reader.Read())
			{
				if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "money_ping")
				{
					reader.Read();
					moneyStr = reader.Value.ToString();
                }
                else if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "money_jjo")
                {
                    reader.Read();
                    cashStr = reader.Value.ToString();
                }
                else if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "name")
                {
                    reader.Read();
                    SecurityIO.SetString("name", reader.Value.ToString());
                }
                else if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "profile_image")
                {
                    reader.Read();
                    SecurityIO.SetString("image", reader.Value.ToString());
                }
				else if( reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "top_rank")
				{
					reader.Read();
					lastStage = reader.Value.ToString();
				}
            }
            StageReader.GetInstance().lastStageIndex = 0;
			for( int i=0; i<StageReader.GetInstance().stageArray.Count; i++)
			{
				if( StageReader.GetInstance().stageArray[i].id == lastStage)
                {
                    StageReader.GetInstance().lastStageIndex = i + 1;
                }
			}
            StageReader.GetInstance().LockStage(StageReader.GetInstance().lastStageIndex);
			RequestManager.GetInstance().RegisterMoney(moneyStr, cashStr);
			nowState = State.COMPLETE;
			StartCoroutine(GoNextScene());
//            Debug.Log("Last" + StageReader.GetInstance().lastStageIndex.ToString());
		}

		else
		{
            StartCoroutine(WaitForMoney(new WWW(www.url)));
			Debug.Log(www.error);
		}

		yield break;
	}


    private IEnumerator WaitForStages(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            string updateTime = "";
            stageList = new System.Collections.Generic.List<StageInfo>();
            JsonReader reader = new JsonReader(www.text);
            while (reader.Read())
            {
                if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "updated_at")
                {
                    reader.Read();
                    updateTime = reader.Value.ToString();
                }
                if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "file_name")
                {
                    StageInfo node = new StageInfo();
                    reader.Read();
                    node.fileName = reader.Value.ToString();
                    node.updateTime = updateTime;
                    node.hash = "";
                    stageList.Add(node);
                }
            }
            stageList = SecurityIO.UpdateStageList(stageList);

            if( stageList.Count > 0)
            {
				loadingCount += stageList.Count;
				StartCoroutine(WaitForStage(new WWW("https://rrgames.me:3000/stages/"+stageList[0].fileName+"/xml_data/"), stageList[0].fileName));
            }
            else
            {
                SecurityIO.UpdateComplete();
                SecurityIO.StageRead();
                isStageInit = true;
                stageList.Clear();
                stageList = null;
            }
        }
        else
        {
            StartCoroutine(WaitForStages(new WWW("https://rrgames.me:3000/stages?field=file_name,updated_at")));
            Debug.Log(www.error);
        }
    }
    private IEnumerator WaitForStage(WWW www, string id)
    {
        yield return www;
        if (www.error == null)
        {
			loadedCount++;
            SecurityIO.SaveStageFile(www.text, stageList[0].updateTime);
            Debug.Log(id);
            stageList.RemoveAt(0);
            if (stageList.Count > 0)
            {
                string id_ = stageList[0].fileName;
                StartCoroutine(WaitForStage(new WWW("https://rrgames.me:3000/stages/" + id_ + "/xml_data/"), id_));
            }
            else
            {
                SecurityIO.UpdateComplete();
                SecurityIO.StageRead();
                isStageInit = true;
                stageList.Clear();
                stageList = null;
            }
        }
        else
        {
            StartCoroutine(WaitForStage(new WWW(www.url), id));
            Debug.Log(www.error);
        }
    }


    private IEnumerator WaitForRecords()
    {
		while (isStageInit == false)
		{
			yield return null;
		}
        nowState = State.SV_RECORD;
		loadingCount += StageReader.GetInstance ().stageArray.Count;
        StartCoroutine(WaitForRecord(new WWW("https://rrgames.me:3000/stages/" + StageReader.GetInstance().stageArray[0].id + 
		                                     "/records?uid="+SecurityIO.GetString("id")+"&token=" + SecurityIO.GetString("token")), 0, StageReader.GetInstance().stageArray[0].id));
        yield break;
    }

    private IEnumerator WaitForRecord(WWW www, int i, string id)
    {
        yield return www;
        if (www.error == null)
        {
            i++;
			loadedCount++;
			//Debug.Log(loadedCount.ToString()+"/"+loadingCount.ToString() +"," +((float)loadedCount/loadingCount).ToString());
            JsonReader reader = new JsonReader(www.text);
            while (reader.Read())
            {
                if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "score")
                {
                    reader.Read();
                    int tryOut_int;
                    if(int.TryParse(reader.Value.ToString(), out tryOut_int))
                    {
                        StageReader.GetInstance().stageArray[i - 1].highScore = tryOut_int;
                    }
                    else
                    {
                        StageReader.GetInstance().stageArray[i - 1].highScore = 0;
                    }
                }
            }

            if( i < StageReader.GetInstance().stageArray.Count)
            {
                StartCoroutine(WaitForRecord(new WWW("https://rrgames.me:3000/stages/" + StageReader.GetInstance().stageArray[i].id +
				                                     "/records?uid=" + SecurityIO.GetString("id") + "&token=" + SecurityIO.GetString("token")), i, StageReader.GetInstance().stageArray[i].id));
            }
            else
			{
				nowState = State.SV_MONEY;
				StartCoroutine(WaitForMoney(new WWW("https://rrgames.me:3000/users/" + SecurityIO.GetString("id") + "/?token=" + SecurityIO.GetString("token"))));
				//Application.LoadLevel("TitleScene");
            }
        }
        else
        {
            StartCoroutine(WaitForRecord(new WWW(www.url), i , id));
        }
    }

	private IEnumerator GoNextScene()
	{
		yield return new WaitForSeconds (1);
		Application.LoadLevel ("TitleScene");
        yield break;
	}

	private IEnumerator WaitForNetworkTest(WWW www)
	{
		progressBar.value = 1f;
		progressLabel.text = "Connect to server";
		yield return www;
		if (nowState != State.TESTING)
		{
			yield break;
		}

		if (www.error == null)
		{
			progressBar.value = 1f;
			progressLabel.text = "Connected!";
			StartCoroutine(WaitForStages(new WWW("https://rrgames.me:3000/stages?field=file_name,updated_at")));
			nowState = State.FB_INIT;
			FB.Init(OnFBInitComplete);
		}
		else
		{
			nowState = State.ERROR;
			progressBar.value = 1f;
			progressLabel.text = "Connect Retry";
			StartCoroutine(ProgressBarPingPongOnError());
		}
	}


    private IEnumerator WaitForSession(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
			//Debug.Log(www.text);
            string token = null;
            string id = null;
            JsonReader reader = new JsonReader(www.text);

            while (reader.Read())
            {
                if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "user_id")
                {
                    reader.Read();
                    id = reader.Value.ToString();
                }
                else if(reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "session_token")
                {
                    reader.Read();
                    token = reader.Value.ToString();
                }
            }
            Debug.Log(token);
            RequestManager.GetInstance().RegisterSession(token, id);
			//nowState = State.SV_RECORD;
			StartCoroutine(WaitForRecords());
        }
        else
        {
            StartCoroutine(WaitForSession(new WWW(www.url)));
            Debug.Log(www.error);
        }
    }


    void OnFBInitComplete()
    {
		nowState = State.FB_LOGIN;
		if (FB.IsLoggedIn == false)
		{
			nowState = State.FB_LOGIN;
			progressBar.value = 1f;
			progressLabel.text = "Connect with Facebook";
			StartCoroutine(ProgressBarPingPongOnFacebook());
		}
		else
		{
			nowState = State.SV_SESSION;
			progressBar.value = 0f;
			progressLabel.text = "0%";
			StartCoroutine(WaitForSession(new WWW("https://rrgames.me:3000/session?code=" + FB.AccessToken + "&platform=unity")));
		}
    }

    void Awake()
    {
        float targetaspect = 9f / 16f;
        float windowaspect = (float)Screen.width / (float)Screen.height;
        float scaleheight = windowaspect / targetaspect;
        Rect rect = new Rect();
        if (scaleheight < 1.0f)
        {
            rect.width = 1.0f;
            rect.height = scaleheight;
            rect.x = 0;
            rect.y = (1.0f - scaleheight) / 2.0f;
        }
        else
        {
            float scalewidth = 1.0f / scaleheight;
            rect.width = scalewidth;
            rect.height = 1.0f;
            rect.x = (1.0f - scalewidth) / 2.0f;
            rect.y = 0;
        }
        for( int i=0; i<Camera.allCamerasCount; i++)
        {
            Camera camera = Camera.allCameras[i];
            if (camera.name == "ClearCamera")
                continue;
            camera.rect = rect;
        }

        if (SecurityIO.GetString("bgm") == "")
        {
            SecurityIO.SetString("bgm", "true");
        }
        if (SecurityIO.GetString("sfx") == "")
        {
            SecurityIO.SetString("sfx", "true");
        }



		barProgress = 0f;
		realProgress = 0f;
		loadingCount = 0;
		loadedCount = 0;
		facebookConnect.onClick.Add (new EventDelegate(this, "FacebookLoginButtonTouched"));
		nowState = State.TESTING;
#if !UNITY_EDITOR
		StartCoroutine (WaitForNetworkTest(new WWW("http://www.google.com")));
#endif
#if UNITY_EDITOR
        StartCoroutine(WaitForStages(new WWW("https://rrgames.me:3000/stages?field=file_name,updated_at")));
        nowState = State.SV_RECORD;
        StartCoroutine(WaitForRecords());
#endif

    }
	
	public IEnumerator ProgressBarPingPongOnError()
	{
		bool isReverted = true;
		float progress = 0f;
		while (true)
		{
			if( FB.IsLoggedIn == true)
			{
				StartCoroutine(WaitForSession(new WWW("https://rrgames.me:3000/session?code=" + FB.AccessToken + "&platform=unity")));
				nowState = State.SV_SESSION;
				break;
			}
			progressBar.value = 1f;
			progress += Time.deltaTime;
			if( progress > 1f)
			{
				progress -= 1f;
				isReverted = !isReverted;
			}
			if( isReverted == false)
				progressBar.alpha = Mathf.Lerp(0.5f, 1f, progress);
			else
				progressBar.alpha = Mathf.Lerp(1f, 0.5f, progress);
			yield return null;
		}
		progressBar.alpha = 1f;
		progressBar.value = 0f;
		barProgress = 0f;
		realProgress = 0f;
	}
	
	public IEnumerator ProgressBarPingPongOnFacebook()
	{
		bool isReverted = true;
		float progress = 0f;
		while (true)
		{
			if( FB.IsLoggedIn == true)
			{
				StartCoroutine(WaitForSession(new WWW("https://rrgames.me:3000/session?code=" + FB.AccessToken + "&platform=unity")));
				nowState = State.SV_SESSION;
				break;
			}
			progressBar.value = 1f;
			progress += Time.deltaTime;
			if( progress > 1f)
			{
				progress -= 1f;
				isReverted = !isReverted;
			}
			if( isReverted == false)
				progressBar.alpha = Mathf.Lerp(0.5f, 1f, progress);
			else
				progressBar.alpha = Mathf.Lerp(1f, 0.5f, progress);
			yield return null;
		}
		progressBar.alpha = 1f;
		progressBar.value = 0f;
		barProgress = 0f;
		realProgress = 0f;
	}

    void Start()
	{

    }
	
	void Update ()
    {
        timer += Time.deltaTime;

		if (nowState == State.SV_SESSION || nowState == State.SV_RECORD || nowState == State.COMPLETE)
		{
			if( loadingCount != 0)
			{
				realProgress = (float)loadedCount/loadingCount;
				if( nowState == State.SV_SESSION)
				{
					realProgress /= 2f;
				}
			}
			barProgress = Mathf.Lerp(barProgress, realProgress, 0.3f);
			//Debug.Log(loadedCount.ToString() +"/" + loadingCount.ToString() + ","+realProgress.ToString());
			//Debug.Log(barProgress.ToString());
			if( barProgress > 0.99f)
				progressBar.value = 1f;
			else
				progressBar.value = barProgress;
			progressLabel.text = ((int)(progressBar.value * 100f)).ToString() + "%";
		}
		/*
		if( nowState != State.FB_LOGIN && nowState != State.FB_INIT)
		{
			realProgress = 0.3f + ((((float)(loadedCount))/loadingCount) * 0.7f);
			barProgress += (realProgress - barProgress) * 0.2f;
			progressBar.value = barProgress;
			progressLabel.text = ((int)(barProgress * 100f)).ToString() + "%";
		}*/
		/*
        if (FB.IsInitialized == true && FB.IsLoggedIn == true && isFBInit == false)
        {
            isFBInit = true;
            StartCoroutine(WaitForSession(new WWW("https://rrgames.me:3000/session?code=" + FB.AccessToken + "&platform=unity")));
        }
        else if (FB.IsInitialized == true && FB.IsLoggedIn == true && isFBInit == true && isStageInit == true && isSessioned == true && isNext == false && isRecordInit == true && timer >= 2.0f)
        {
            isNext = true;
            Application.LoadLevel("TitleScene");
        }*/



#if UNITY_EDITOR
		/*
        if (isStageInit == true && isNext == false && timer >= 2.0f)
        {
            isNext = true;
            Application.LoadLevel("TitleScene");
        }*/
#endif
         
	}
    //5:30
    void OnGUI()
    {
		/*
        GUI.DrawTexture(new Rect(Screen.width * 0.285714f, Screen.height * 0.565f, Screen.width * 0.428571f, Screen.height * 0.062f), progressBorder.texture);
        if (FB.IsLoggedIn == true && isSessioned == true && isStageInit == true && isRecordInit == true && progress == 4)
        {
            progress = 5;
            realProgress = 1f;
            loadingText.text = "사실상 다불러옴";
        }
        else if (FB.IsLoggedIn == true && isSessioned == true && isStageInit == true && progress == 3)
        {
            StartCoroutine(WaitForRecords());
            progress = 4;
            realProgress = 0.8f;
            loadingText.text = "기록을 불러오는중";
        }
        else if (FB.IsLoggedIn == true && isSessioned == true && progress == 2)
        {
            progress = 3;
            realProgress = 0.6f;
            loadingText.text = "스테이지를 불러오는중";
        }
        else if (FB.IsLoggedIn == true && progress == 1)
        {
            progress = 2;
            realProgress = 0.4f;
            loadingText.text = "세션을 불러오는중";
        }
        else if (progress == 0)
        {
            progress = 1;
            realProgress = 0.2f;
            loadingText.text = "페이스북에 연결중";
        }
        if (FB.IsInitialized == true && FB.IsLoggedIn == false && GUI.Button(new Rect(Screen.width * 0.285714f, Screen.height * 0.565f, Screen.width * 0.428571f, Screen.height * 0.1f), facebookLoginButton.texture, GUIStyle.none))
        {
            FB.Login();
        }
        else if (FB.IsInitialized == true && FB.IsLoggedIn == true)
        {
            barProgress += (realProgress - barProgress) * 0.3f;
            GUI.DrawTexture(new Rect(Screen.width * 0.285714f, Screen.height * 0.565f, Screen.width * 0.428571f, Screen.height * 0.062f), progressBorder.texture);
            GUI.DrawTexture(new Rect(Screen.width * 0.285714f, Screen.height * 0.565f, Screen.width * 0.428571f * realProgress, Screen.height * 0.062f), progressBar.texture, ScaleMode.StretchToFill);
            GUI.skin.label.alignment = TextAnchor.MiddleCenter;
 //           GUI.Label(new Rect(Screen.width * 0.285714f, Screen.height * 0.62f, Screen.width * 0.428571f, Screen.height * 0.062f), loadingText);
        }*/
    }
}
