﻿using UnityEngine;
using System.Collections;
using LitJson;

public class ScoreComparer : System.Collections.Generic.IComparer<RankData>
{
    Comparer comparer = new Comparer(System.Globalization.CultureInfo.CurrentCulture);
    public int Compare(RankData x, RankData y)
    {
        return comparer.Compare(y.score, x.score);
    }
}

public class RankData
{
    public int id;
    public int score;
    public RankData()
    {

    }
    public RankData(int id, int score)
    {
        this.id = id;
        this.score = score;
    }
}

public class ShopItem
{
    public string id;
    public string name;
    public string desc;
    public string image;
    public bool isCash;
    public int price;
    public int discount;
}


public class TitleScene : MonoBehaviour
{
    public enum SCENE_STATE
    {
        STAGE,
        SHOP,
        OPTION,
        EXIT,
    };

    public ShopController shopCamera;
    public UIButton shopButton;
    public UIButton bgmButton;
    public UIButton sfxButton;
    public UIButton shopCloseButton;
    public UIButton leftButton;
    public UIButton rightButton;
    public UILabel money;
    //public UILabel cash;
    public AudioClip menuPressed;
    public AudioClip startPressed;
    public AudioClip bgm;
    public AudioClip soundButtonSound;
    public AudioClip leftRightSound;

    public GameObject exitParent;
    public UIButton exitButton;
    public UIButton exitNoButton;
   // public AudioClip purchaseSound;


    private SCENE_STATE nowState;

    private bool shopLoaded;

    private int num;
    private int maxNum;

    private bool touchIgnore;

    private float[] scrollHelper;
    private int scrollCount;

    private int startValue;
    private int endValue;

    private float indexX;
    private float indexXTo;
    private bool moveFlag;

    private System.Collections.Generic.List<RankData> rankList;
    private System.Collections.Generic.List<ShopItem> shopList;

    private Vector2 startPoint;
    private int touchIndex;
    private float nowPoint;
    private float panelPoint;

    private bool isBGM;
    private bool isSFX;

    public Texture sfxOn;
    public Texture sfxOff;
    public Texture bgmOn;
    public Texture bgmOff;

    private bool isSwipe;
    private float touchTime;
    private float touchVector;

    private bool isButtonFlag;

    private System.Collections.Generic.List<GameObject> panelList = new System.Collections.Generic.List<GameObject>();

    public void RankLoaded()
    {
//        loaded = true;
    }

    public string GetMissionString(Mission adapter)
    {
        string color = "";
        if ((MISSION)adapter.type == MISSION.MISSION_CB || (MISSION)adapter.type == MISSION.MISSION_PM)
        {
            switch ((BLOCK)adapter.optionArray[0])
            {
                case BLOCK.BLOCK_RED:
                    color = @"Red";
                    break;
                case BLOCK.BLOCK_YEL:
                    color = @"Yellow";
                    break;
                case BLOCK.BLOCK_GRN:
                    color = @"Green";
                    break;
                case BLOCK.BLOCK_BLU:
                    color = @"Blue";
                    break;
                case BLOCK.BLOCK_PURPLE:
                    color = @"Purple";
                    break;
                case BLOCK.BLOCK_NUI:
                    color = @"Black";
                    break;
            }
        }
        switch ((MISSION)adapter.type)
        {
            case MISSION.MISSION_AC:
                return "Remove all blocks";
            case MISSION.MISSION_CB:
				return string.Format("{1} Remove {0} blocks", color, adapter.optionArray[1]);
            case MISSION.MISSION_DB:
                return string.Format("Remove the target block ({0},{1})", adapter.optionArray[0], adapter.optionArray[1]);
            case MISSION.MISSION_ML:
                return string.Format("Complete in {0} turns", adapter.optionArray[0]);
            case MISSION.MISSION_PM:
			return string.Format("{1} Remove {0} blocks, with 1 turn", color, adapter.optionArray[1]);
            case MISSION.MISSION_SC:
                return string.Format("Reach {0}score", adapter.optionArray[0]);
            case MISSION.MISSION_TL:
                return string.Format("Complete in {0} seconds", adapter.optionArray[0]);
            default:
                return "";
        }
    }

    private IEnumerator WaitForProfile(WWW www, int order)
    {
        yield return www;
        if (www.error == null)
        {
            Debug.Log(www.text);
            JsonReader reader = new JsonReader(www.text);
            while (reader.Read())
            {
                if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "name")
                {
                    reader.Read();
                    string name = reader.Value.ToString();
                    switch (order)
                    {
                        case 0:
                            break;
                        case 1:
                            break;
                        case 2:
                            break;
                    }
                    continue;
                }
                else if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "profile_image")
                {
                    reader.Read();
                    string imageUrl = reader.Value.ToString();
                    //                    StartCoroutine(WaitForImage(new WWW(imageUrl), order));
                    //needImage[order] = true;
                    //imageLoaded[order] = false;
                    continue;
                }
            }
        }
    }
    /*
    private IEnumerator WaitForImage(WWW www, int order)
    {
        yield return www;
        if (www.error == null)
        {
            GameObject obj = null;
            Texture2D profileImage = new Texture2D(128, 128, TextureFormat.DXT1, false);
            www.LoadImageIntoTexture(profileImage);
            switch(order)
            {
                case 0:
                    GameObject.Find("first").guiText.enabled = true;
                    obj = GameObject.Find("top");
                    break;
                case 1:
                    GameObject.Find("second").guiText.enabled = true;
                    obj = GameObject.Find("mid");
                    break;
                case 2:
                    GameObject.Find("third").guiText.enabled = true;
                    obj = GameObject.Find("bot");
                    break;
            }

            obj.layer = LayerMask.NameToLayer("Rank");
            obj.GetComponent<SpriteRenderer>().sprite = Sprite.Create(profileImage, new Rect(0, 0, profileImage.width - 9, profileImage.height - 9), new Vector2(0.5f, 0.5f));
//                obj.renderer.enabled = true;
            imageLoaded[order] = true;

            if ( touchIndex == -1 && moveFlag == false && (imageLoaded[0] == true || needImage[0] == false) && (imageLoaded[1] == true || needImage[1] == false) && (imageLoaded[2] == true || needImage[2] == false) )
            {
                if (needImage[0] == true)
                {
                    GameObject.Find("first").guiText.enabled = true;
                    GameObject.Find("top").renderer.enabled = true;
                }
                if (needImage[1] == true)
                {
                    GameObject.Find("second").guiText.enabled = true;
                    GameObject.Find("mid").renderer.enabled = true;
                }
                if (needImage[2] == true)
                {
                    GameObject.Find("third").guiText.enabled = true;
                    GameObject.Find("bot").renderer.enabled = true;
                }
                GameObject.Find("line").renderer.enabled = true;
            }
        }
    }*/

    private IEnumerator WaitForShop(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            ShopItem item = null;
            shopList = new System.Collections.Generic.List<ShopItem>();
            JsonReader reader = new JsonReader(www.text);
            int outInt;
            while (reader.Read())
            {
                if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "id")
                {
                    if (item != null)
                    {
                        shopList.Add(item);
                    }
                    item = new ShopItem();
                    reader.Read();
                    item.id = reader.Value.ToString();
                }
                else if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "name")
                {
                    reader.Read();
                    item.name = reader.Value.ToString();
                }
                else if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "desc")
                {
                    reader.Read();
                    item.desc = reader.Value.ToString();
                }
                else if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "price_jjo")
                {
                    reader.Read();
                    int.TryParse(reader.Value.ToString(), out outInt);
                    if (outInt != 0)
                    {
                        item.price = outInt;
                        item.isCash = true;
                    }
                }
                else if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "price_ping")
                {
                    reader.Read();
                    int.TryParse(reader.Value.ToString(), out outInt);
                    if (outInt != 0)
                    {
                        item.price = outInt;
                        item.isCash = false;
                    }
                }
                else if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "discount")
                {
                    reader.Read();
                    int.TryParse(reader.Value.ToString(), out outInt);
                    item.discount = outInt;
                }
                else if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "image")
                {
                    reader.Read();
                    item.image = reader.Value.ToString();
                }
            }
            if (item != null)
            {
                shopList.Add(item);
            }
            shopCamera.Init(shopList, this);
            shopLoaded = true;
        }
        else
        {
            Debug.Log(www.error);
        }
    }

    private IEnumerator WaitForRank(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
//            Debug.Log(www.text);
            System.Collections.Generic.List<RankData> list = new System.Collections.Generic.List<RankData>();
            JsonReader reader = new JsonReader(www.text);
            int id = 0;
            int score = 0;
            RankData rankData = null;// = new RankData();
            while (reader.Read())
            {
                if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "user_id")
                {
                    reader.Read();
                    int.TryParse(reader.Value.ToString(), out id);
                    rankData = new RankData();
                    rankData.id = id;
                }
                else if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "score")
                {
                    reader.Read();
                    int.TryParse(reader.Value.ToString(), out score);
                    rankData.score = score;
                    list.Add(rankData);
                }
            }
            list.Sort(new ScoreComparer());


//            panelList[num].transform.FindChild("rankingContent").GetComponent<RankingScript>().Init();
//            panelList[num].transform.FindChild("rankingContent").GetComponent<RankingScript>().ApplyRanking(list);

            StageReader.GetInstance().GetStage().rankArray = list;

            /*
            for (int i = 0; i < rankList.Count; i++ )
            {
                    StartCoroutine(WaitForProfile(new WWW("https://rrgames.me:3000/users/" + list[rankList[i]].id.ToString() + "/?token=" + SecurityIO.GetString("token")), i));
                }
            }*/
        }
        else
        {
            Debug.Log(www.error);
        }
    }

    public void ShopButtonTouched()
    {
        if( nowState == SCENE_STATE.STAGE)
        {
            panelList[num].GetComponent<StagePanel>().MissionButton();
            shopCamera.ShopEnable();
            nowState = SCENE_STATE.SHOP;
        }
    }

    public void ShopCloseButtonTouched()
    {
        if (nowState == SCENE_STATE.SHOP)
        {
            shopCamera.ShopDisable();
            nowState = SCENE_STATE.STAGE;
        }
    }

    public void BGMButtonTouched()
    {
        isBGM = !isBGM;
        SecurityIO.SetString("bgm", isBGM.ToString());
        if (isBGM == true)
        {
            bgmButton.GetComponent<UITexture>().mainTexture = bgmOn;
            audio.Play();
        }
        else
        {
            bgmButton.GetComponent<UITexture>().mainTexture = bgmOff;
            audio.Stop();
        }
        if(isSFX == true)
        {
            audio.PlayOneShot(soundButtonSound);
        }
    }

    public void SFXButtonTouched()
    {
        isSFX = !isSFX;
        SecurityIO.SetString("sfx", isSFX.ToString());
        if (isSFX == true)
        {
            sfxButton.GetComponent<UITexture>().mainTexture = sfxOn;
            audio.PlayOneShot(soundButtonSound);
        }
        else
        {
            sfxButton.GetComponent<UITexture>().mainTexture = sfxOff;
        }
    }

    void Awake()
    {
        float targetaspect = 9f / 16f;
        float windowaspect = (float)Screen.width / (float)Screen.height;
        float scaleheight = windowaspect / targetaspect;
        Rect rect = new Rect();
        if (scaleheight < 1.0f)
        {
            rect.width = 1.0f;
            rect.height = scaleheight;
            rect.x = 0;
            rect.y = (1.0f - scaleheight) / 2.0f;
        }
        else
        {
            float scalewidth = 1.0f / scaleheight;
            rect.width = scalewidth;
            rect.height = 1.0f;
            rect.x = (1.0f - scalewidth) / 2.0f;
            rect.y = 0;
        }
        for (int i = 0; i < Camera.allCamerasCount; i++)
        {
            Camera camera = Camera.allCameras[i];
            if (camera.name == "ClearCamera")
                continue;
            camera.rect = rect;
        }

        if (bool.TryParse(SecurityIO.GetString("bgm"), out isBGM) == false)
        {
            SecurityIO.SetString("bgm", "true");
            isBGM = true;
        }
        if (bool.TryParse(SecurityIO.GetString("sfx"), out isSFX) == false)
        {
            SecurityIO.SetString("sfx", "true");
            isSFX = true;
        }

        if (isBGM == true)
        {
            bgmButton.GetComponent<UITexture>().mainTexture = bgmOn;
        }
        else
        {
            bgmButton.GetComponent<UITexture>().mainTexture = bgmOff;
        }
        if (isSFX == true)
        {
            sfxButton.GetComponent<UITexture>().mainTexture = sfxOn;
        }
        else
        {
            sfxButton.GetComponent<UITexture>().mainTexture = sfxOff;
        }

        scrollHelper = new float[10];
        scrollCount = 0;
        shopButton.onClick.Add(new EventDelegate(this, "ShopButtonTouched"));
        shopCloseButton.onClick.Add(new EventDelegate(this, "ShopCloseButtonTouched"));
        bgmButton.onClick.Add(new EventDelegate(this, "BGMButtonTouched"));
        sfxButton.onClick.Add(new EventDelegate(this, "SFXButtonTouched"));
        leftButton.onClick.Add(new EventDelegate(this, "LeftTouched"));
        rightButton.onClick.Add(new EventDelegate(this, "RightTouched"));
        exitButton.onClick.Add(new EventDelegate(this, "GameQuit"));
        exitNoButton.onClick.Add(new EventDelegate(this, "CloseMenuClose"));
        nowState = SCENE_STATE.STAGE;
        shopLoaded = false;
        moveFlag = true;
        touchIndex = -1;
        num = StageReader.GetInstance().LockedStage;
        maxNum = StageReader.GetInstance().lastStageIndex;
        if(StageReader.GetInstance().stageArray.Count <= maxNum)
        {
            maxNum = StageReader.GetInstance().stageArray.Count - 1;
        }
        panelList.Clear();

        GameObject stagePanel;
        stagePanel = Resources.Load("Prefab/stagePanelNGUI") as GameObject;
        GameObject parent = GameObject.Find("Panels");

        for (int i = 0; i < StageReader.GetInstance().stageArray.Count; i++)
        {
            int index = i - num;
            GameObject panel = MonoBehaviour.Instantiate(stagePanel) as GameObject;
            panel.name = "Panel";
            panel.transform.parent = parent.transform;
            panel.transform.localPosition = new Vector3((float)index * 1000f, 0f, 0f);
            panel.transform.localScale = new Vector3(1f, 1f, 1f);
            panel.transform.FindChild("mission").GetComponent<UIButton>().onClick.Add(new EventDelegate(this, "MissionTouched"));
            panel.transform.FindChild("ranking").GetComponent<UIButton>().onClick.Add(new EventDelegate(this, "RankingTouched"));
            panel.transform.FindChild("start").GetComponent<UIButton>().onClick.Add(new EventDelegate(this, "StartTouched"));
            panel.GetComponent<StagePanel>().id = StageReader.GetInstance().stageArray[i].id;
            panelList.Add(panel);
        }
        StartCoroutine(WaitForShop(new WWW("https://rrgames.me:3000/market/goods")));
        //StageReader.GetInstance().LoadStage();

        for (int i = 0; i < panelList.Count; i++)
        {
            StageReader.GetInstance().LockStage(i);
            StageReader.Stage reader = StageReader.GetInstance().GetStage();
            #region StarInit
            if (reader.highScore >= reader.grade[0])
            {
                panelList[i].gameObject.transform.FindChild("star1").GetComponent<UITexture>().enabled = true;
            }
            else
            {
                panelList[i].gameObject.transform.FindChild("star1").GetComponent<UITexture>().enabled = false;
            }
            if (reader.highScore >= reader.grade[1])
            {
                panelList[i].gameObject.transform.FindChild("star2").GetComponent<UITexture>().enabled = true;
            }
            else
            {
                panelList[i].gameObject.transform.FindChild("star2").GetComponent<UITexture>().enabled = false;
            }
            if (reader.highScore >= reader.grade[2])
            {
                panelList[i].gameObject.transform.FindChild("star3").GetComponent<UITexture>().enabled = true;
            }
            else
            {
                panelList[i].gameObject.transform.FindChild("star3").GetComponent<UITexture>().enabled = false;
            }
            #endregion

            #region MapSizeInit
            int width = reader.width;
            int height = reader.height;

            int adjust = 0;
            if (height > 9)
            {
                adjust = height - 9;
            }

            int tempHeight = height;
            if (tempHeight > 9)
            {
                tempHeight = 9 + (adjust) * 2;
            }
            //float originX = -0.375f * (width - 1); // 1 = 0, 2 = 0.375 = 3 = 0.75
            //float originY = 0.375f * (tempHeight - 1);
            #endregion

            #region StringInit
            panelList[i].transform.FindChild("stageName").GetComponent<UILabel>().text = reader.id;
			panelList[i].transform.FindChild("score").GetComponent<UILabel>().text = reader.highScore.ToString() + "pts";

            GameObject missionParent = panelList[i].transform.FindChild("missionContent").gameObject;
            missionParent.transform.localPosition += new Vector3(0f, -50f * (3 - reader.missionArray.Count), 0f);

            if (reader.missionArray.Count > 0)
            {
                missionParent.transform.FindChild("firstMission").GetComponent<UILabel>().text = GetMissionString(reader.missionArray[0]);
            }

            if (reader.missionArray.Count > 1)
            {
                missionParent.transform.FindChild("secondMission").GetComponent<UILabel>().text = GetMissionString(reader.missionArray[1]);
            }

            if (reader.missionArray.Count > 2)
            {
                missionParent.transform.FindChild("thirdMission").GetComponent<UILabel>().text = GetMissionString(reader.missionArray[2]);
            }
            #endregion

            #region MiniMapInit
            GameObject whiteBlock = Resources.Load("Prefab/TitleWhite") as GameObject;
            GameObject minimap = panelList[i].transform.FindChild("MiniMap").gameObject;
            float originX = -22f * (width - 1);
            float originY = 22f * (tempHeight - 1);

            for (int j = adjust; j < height; j++)
            {
                for (int k = 0; k < width; k++)
                {
                    int value = reader.blockArray[j * width + k];
                    GameObject instance;

                    if (value == (int)BLOCK.BLOCK_RED) //red
                    {
                        instance = MonoBehaviour.Instantiate(whiteBlock) as GameObject;
                        instance.GetComponent<UITexture>().color = new Color(1f, 0f, 0f, 1f);
                    }
                    else if (value == (int)BLOCK.BLOCK_YEL) //yellow
                    {
                        instance = MonoBehaviour.Instantiate(whiteBlock) as GameObject;
                        instance.GetComponent<UITexture>().color = new Color(1f, 1f, 0f, 1f);
                    }
                    else if (value == (int)BLOCK.BLOCK_GRN) //green
                    {
                        instance = MonoBehaviour.Instantiate(whiteBlock) as GameObject;
                        instance.GetComponent<UITexture>().color = new Color(0f, 1f, 0f, 1f);
                    }
                    else if (value == (int)BLOCK.BLOCK_BLU) //blue
                    {
                        instance = MonoBehaviour.Instantiate(whiteBlock) as GameObject;
                        instance.GetComponent<UITexture>().color = new Color(0f, 0f, 1f, 1f);
                    }
                    else if (value == (int)BLOCK.BLOCK_PURPLE) //purple
                    {
                        instance = MonoBehaviour.Instantiate(whiteBlock) as GameObject;
                        instance.GetComponent<UITexture>().color = new Color(0.75f, 0f, 1f, 1f);
                    }
                    else if (value == (int)BLOCK.BLOCK_NUI) //nui
                    {
                        instance = MonoBehaviour.Instantiate(whiteBlock) as GameObject;
                        instance.GetComponent<UITexture>().color = new Color(0.2f, 0.2f, 0.2f, 1f);
                    }
                    else if (value == (int)BLOCK.BLOCK_WALL) //wall
                    {
                        instance = MonoBehaviour.Instantiate(whiteBlock) as GameObject;
                        instance.GetComponent<UITexture>().color = new Color(0f, 0f, 0f, 1f);
                    }
                    else if (value == (int)BLOCK.BLOCK_RANDOM) //wall
                    {
                        instance = MonoBehaviour.Instantiate(whiteBlock) as GameObject;
                        instance.GetComponent<UITexture>().color = new Color(0f, 0f, 0f, 1f);
                    }
                    else if (value == (int)BLOCK.BLOCK_ITEM) //wall
                    {
                        instance = MonoBehaviour.Instantiate(whiteBlock) as GameObject;
                        instance.GetComponent<UITexture>().color = new Color(1f, 1f, 1f, 1f);
                    }
                    else
                    {
                        instance = new GameObject();
                        Debug.Log("exception");
                    }
                    instance.transform.parent = minimap.transform;
                    instance.transform.localPosition = new Vector3(originX + (k * 44), originY - (j * 44f), 0f);
                    instance.transform.localScale = new Vector3(1f, 1f, 1f);
                    instance.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
                }
            }
            #endregion

        }
        StageReader.GetInstance().LockStage(num);
        num = -1;
//        Debug.Log(StageReader.GetInstance().LockedStage.ToString());
        SetNum(StageReader.GetInstance().LockedStage);
        indexX = indexXTo;
        AdjustPanelPosition();

        /*StageReader.GetInstance().LockStage(num);
        StartCoroutine(WaitForRank(new WWW("https://rrgames.me:3000/stages/" +
            StageReader.GetInstance().GetStage().id +
            "/records?token=" + SecurityIO.GetString("token") + "&limit=300")));
        */
    }

    public void MissionTouched()
    {
        if (panelList[num].GetComponent<StagePanel>().MissionButton() && isSFX == true)
            audio.PlayOneShot(menuPressed);
    }

    public void RankingTouched()
    {
        if (panelList[num].GetComponent<StagePanel>().RankingButton() && isSFX == true)
            audio.PlayOneShot(menuPressed);
        //                                    panelList[num].transform.FindChild("rankingContent").gameObject.GetComponent<RankingScript>().ShowScreen();
    }

    public void StartTouched()
    {
        if( isSFX == true)
            audio.PlayOneShot(startPressed);
        GameObject button = panelList[num].transform.FindChild("mask").gameObject;
        button.transform.localPosition = panelList[num].transform.FindChild("start").localPosition;
        Application.LoadLevel("GameScene");
    }

    public void LeftTouched()
    {
        int value = num - 1;
        if (value < 0)
            value = 0;
        else if (value > maxNum)
            value = maxNum;
        if( value != num)
        {
            if( isSFX == true)
            {
                audio.PlayOneShot(leftRightSound);
            }
            SetNum(value);
            isButtonFlag = true;
        }
    }

    public void RightTouched()
    {
        int value = num + 1;
        if (value < 0)
            value = 0;
        else if (value > maxNum)
            value = maxNum;
        if (value != num)
        {
            if( isSFX == true)
            {
                audio.PlayOneShot(leftRightSound);
            }
            SetNum(value);
            isButtonFlag = true;
        }
    }

    void Start()
    {
        audio.clip = bgm;
        audio.loop = true;
        if( isBGM == true)
        {
            audio.Play();
        }


        //StartCoroutine(WaitForTest(new WWW("https://rrgames.me:3000/users/3/items")));

//        Debug.Log(SecurityIO.GetString("token"));

        //Screen.SetResolution(720, 1280, true);
        //GameObject.Find("money").GetComponent<TextMesh>().text = SecurityIO.GetString("money");
        //GameObject.Find("cash").GetComponent<TextMesh>().text = SecurityIO.GetString("cash");
        //SecurityIO.GetStageList();
        touchIndex = -1;
        //moveFlag = true;
    }

    void SetNum(int value)
    {
        if (value < 0)
            value = 0;
        else if( value > maxNum)
            value = maxNum;
        //else if (value >= panelList.Count)
        //    value = panelList.Count - 1;

        //Debug.Log(num.ToString() + " " + value.ToString());

        if( num == value)
            return;

        if( num != -1)
            panelList[num].GetComponent<StagePanel>().MissionButton();
        num = value;
        indexXTo = -1000f * num;
        moveFlag = true;

        startValue = num - 3;
        endValue = num + 4;
        if (startValue < 0)
            startValue = 0;
        if (endValue > panelList.Count)
            endValue = panelList.Count;

        for (int i = 0; i < startValue; i++)
        {
            panelList[i].SetActive(false);
        }
        for (int i = startValue; i < endValue; i++ )
        {
            panelList[i].SetActive(true);
        }
        for (int i = endValue; i < panelList.Count; i++)
        {
            panelList[i].SetActive(false);
        }
        StageReader.GetInstance().LockStage(num);
        StartCoroutine(WaitForRank(new WWW("https://rrgames.me:3000/stages/" + StageReader.GetInstance().GetStage().id + "/records?token=" + SecurityIO.GetString("token") + "&location=" + SecurityIO.GetString("id"))));
    }

    public void CloseMenuOpen()
    {
        nowState = SCENE_STATE.EXIT;
        exitParent.SetActive(true);
    }

    public void CloseMenuClose()
    {
        nowState = SCENE_STATE.STAGE;
        exitParent.SetActive(false);
    }

    public void GameQuit()
    {
        Application.Quit();
    }

    void Update()
    {
        if (touchIndex == -1 && moveFlag == true)
        {
            /*
            float movement = remainMovement * 0.15f;// 10 * Time.deltaTime;//0.15f;
            if (Mathf.Abs(movement) < 50f)
            {
                moveFlag = false;
                movement = remainMovement;
            }
            remainMovement -= movement;
            for (int i = 0; i < panelList.Count; i++)
            {
                panelList[i].transform.localPosition = panelList[i].transform.localPosition + new Vector3(movement, 0f, 0f);
            }
            AdjustPanelPosition();*/
        }

        if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (nowState == SCENE_STATE.SHOP)
                {
                    ShopCloseButtonTouched();
                }
                else if(nowState == SCENE_STATE.STAGE)
                {
                    CloseMenuOpen();
                }
                else if(nowState == SCENE_STATE.EXIT)
                {
                    CloseMenuClose();
                }
            }
        }


        if (Input.touchCount > 0)
        {
            foreach (Touch touchPoint in Input.touches)
            {
                if (touchIndex == -1 && touchPoint.phase == TouchPhase.Began)
                {
                    for (int i = 9; i >= 0; i--)
                    {
                        scrollHelper[i] = 0f;
                    }
                    scrollCount = 0;
                    touchIndex = touchPoint.fingerId;
                    startPoint = touchPoint.position;
                    nowPoint = touchPoint.position.x;
                    panelPoint = indexX;// panelList[0].transform.localPosition.x;

                    touchTime = Time.time;
                    touchVector = 0f;
                    /*
                    if (moveFlag == true && isButtonFlag == false)
                    {
                        isSwipe = true;
                    }
                    else
                    {
                        isSwipe = false;
                    }*/
                    isSwipe = false;
                    /*
                    if(isButtonFlag == true)
                    {
                    }
                    else
                    {
                        moveFlag = true;
                    }*/
                }
                else if (touchIndex == touchPoint.fingerId && touchPoint.phase == TouchPhase.Moved && touchIgnore == false)
                {
                    nowPoint = touchPoint.position.x;
                    if (isSwipe == false && touchIgnore == false)// && touchTime + 0.3f < Time.time)
                    {
                        touchVector = Vector2.Distance(startPoint, touchPoint.position);
                        if (touchVector > 50f)
                        {
                            float upset = Mathf.Abs(touchPoint.position.y - startPoint.y) / Mathf.Abs(touchPoint.position.x - startPoint.x);
                            if (upset > 1f)
                            {
                                touchIgnore = true;
                            }
                            else
                            {
                                isButtonFlag = false;
                                isSwipe = true;
                            }
                        }
                    }
                    else
                    {
                        for ( int i=9; i>0; i--)
                        {
                            scrollHelper[i] = scrollHelper[i - 1];
                        }
                        scrollHelper[0] = nowPoint;
                        scrollCount++;
                        float movement = (nowPoint - startPoint.x);// / 150f;
                        if (nowState == SCENE_STATE.STAGE)
                        {
                            indexX = panelPoint + movement;
                        }
                            /*
                        else if (nowState == SCENE_STATE.SHOP)
                        {
                            if (shopLoaded == true && shopCamera.isEnabled == true)
                            {
//                                shopCamera.rails = movement;
                            }
                        }*/
                    }
                }
                else if (touchIndex == touchPoint.fingerId && touchPoint.phase == TouchPhase.Ended)
                {
                    touchIgnore = false;
                    touchIndex = -1;
                    if (isSwipe == false)
                    {

                    }
                    else if (isSwipe == true)
                    {
                        moveFlag = true;
                        isButtonFlag = false;
                        isSwipe = false;
                        float movement = 0f;

                        if( scrollCount != 0)
                        {
                            if (scrollCount > 10)
                                scrollCount = 10;
                            movement = (touchPoint.position.x - scrollHelper[scrollCount-1]) / (scrollCount) * Time.deltaTime * 150f;
                            scrollCount = 0;
                        }
                        if (nowState == SCENE_STATE.STAGE)
                        {
                            SetNum((int)((indexX - 500f + movement) / -1000f));
                        }
                    }
                }
            }
        }
        AdjustPanelPosition();

    }

    void LateUpdate()
    {
//        cash.text = num.ToString();
    }
    void FixedUpdate()
    {
    }

    void OnGUI()
    {

    }

    private void AdjustPanelPosition()
    {
        if( moveFlag == true)
        {
            float remainMovement = (indexXTo - indexX) * 0.15f;
            indexX += remainMovement;
            if (Mathf.Abs(remainMovement) < 1f)
            {
                indexX = indexXTo;
                moveFlag = false;
                isButtonFlag = false;
            }
        }

        for (int i = startValue; i < endValue; i++)
        {
            float x = panelList[i].transform.localPosition.x;
            x = indexX + i * 1000f;
            float rate;
            if (x < -600f)
                rate = -1f;
            else if (x > 600f)
                rate = 1f;
            else
                rate = x / 600f;
            panelList[i].transform.localPosition = new Vector3(x, 0f, Mathf.Abs(rate) * 400f);
            panelList[i].transform.localRotation = Quaternion.Euler(0f, rate * 45f, 0f);
        }
    }
}
