﻿using UnityEngine;
using LitJson;
using System.Collections;

public class RankPanel : MonoBehaviour {
    public UILabel rank;
    public UILabel name;
    public UITexture image;
    public UILabel score;
    public bool loaded;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Hide()
    {
        rank.enabled = false;
        name.enabled = false;
        image.enabled = false;
        score.enabled = false;
        this.GetComponent<UITexture>().enabled = false;
    }

    public void UnHide()
    {
        rank.enabled = true;
        name.enabled = true;
        image.enabled = true;
        score.enabled = true;
        this.GetComponent<UITexture>().enabled = true;
    }

    public void SetRankingData(string id,  string rank_, string score_)
    {
		this.rank.text = rank_;
		if (rank_ == "1")
			this.rank.text += "st";
		else if (rank_ == "2")
			this.rank.text += "nd";
		else if (rank_ == "3")
			this.rank.text += "rd";
		else
			this.rank.text += "th";
        this.score.text = score_;
        StartCoroutine(WaitForProfile(new WWW("https://rrgames.me:3000/users/" + id + "/?token=" + SecurityIO.GetString("token"))));
    }

    public IEnumerator WaitForImage(WWW www)
    {
        yield return www;
        loaded = true;
        if(www.error == null)
        {
            Texture2D profileImage = new Texture2D(70, 70, TextureFormat.DXT1, false);
            www.LoadImageIntoTexture(profileImage);
            image.mainTexture = profileImage;
        }
        else
        {
            Debug.Log(www.error);
        }
    }


    private IEnumerator WaitForProfile(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            JsonReader reader = new JsonReader(www.text);
            while (reader.Read())
            {
                if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "name")
                {
                    reader.Read();
                    name.text = reader.Value.ToString();
                }
                if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "profile_image")
                {
                    reader.Read();
                    StartCoroutine(WaitForImage(new WWW(reader.Value.ToString())));
                    continue;
                }
            }
        }
    }

}
