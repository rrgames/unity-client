﻿using UnityEngine;
using System.Collections;
using System.Security.Cryptography;

public class RequestManager{
//    public string token;
//    public string id;

    private static RequestManager manager;
    public static RequestManager GetInstance()
    {
        if (manager == null)
            manager = new RequestManager();
        return manager;
    }
    public static void RemoveInstance()
    {
        manager = null;
    }
    /*
    public void InitSession(string token, string id)
    {
        this.token = token;
        this.id = id;
    }*/

    public void RegisterSession(string token, string id)
    {
//        this.token = token;
//        this.id = id;

        string key_1 = "token";
        string value_1 = token;

        string key_2 = "id";
        string value_2 = id;

        SecurityIO.SetString(key_1, value_1);
        SecurityIO.SetString(key_2, value_2);
    }

	public void RegisterMoney(string money, string cash)
	{
		SecurityIO.SetString ("money", money);
		SecurityIO.SetString ("cash", cash);
	}
}
