﻿using UnityEngine;
using LitJson;
using System.Collections;

public class GameScene : MonoBehaviour {

    public AudioClip tapBlock;
	public AudioClip tapFail;
	public AudioClip bgm;

	public AudioClip combo1Sound;
	public AudioClip combo2Sound;
    public AudioClip combo3Sound;
    public AudioClip combo4Sound;

    public AudioClip menuButtonSound;
    public AudioClip menuCloseButtonSound;

	public UIButton menuButton;
    public UIButton menuCloseButton;
    public UIButton bgmButton;
    public UIButton sfxButton;
    public UIButton titleButton;
    public UIButton retryButton;
    public GameObject menuParent;

    public UITexture bgmCheck;
    public UITexture sfxCheck;


    public UIButton item1Button;
    public UIButton item2Button;
    public UIButton item3Button;
    public UIButton item4Button;
    public UIButton item5Button;

    public UILabel item1Label;
    public UILabel item2Label;
    public UILabel item3Label;
    public UILabel item4Label;
    public UILabel item5Label;

    public GameObject scoreFrame;
    public GameObject bonusFrame;

    private bool isBGM;
    private bool isSFX;

    private bool isPaused;




    public void BGMButtonTouched()
    {
        isBGM = !isBGM;
        SecurityIO.SetString("bgm", isBGM.ToString());
        if( isBGM == true)
        {
            bgmCheck.enabled = true;
            audio.Play();
        }
        else
        {
            bgmCheck.enabled = false;
            audio.Stop();
        }
        if( isSFX == true)
        {
            audio.PlayOneShot(menuCloseButtonSound);
        }
    }

    public void SFXButtonTouched()
    {
        isSFX = !isSFX;
        SecurityIO.SetString("sfx", isSFX.ToString());
        if (isSFX == true)
        {
            audio.PlayOneShot(menuCloseButtonSound);
            sfxCheck.enabled = true;
        }
        else
        {
            sfxCheck.enabled = false;
        }
    }
    public void TitleButtonTouched()
    {
        audio.PlayOneShot(menuCloseButtonSound);
        Application.LoadLevel("TitleScene");
    }
    public void RetryButtonTouched()
    {
        audio.PlayOneShot(menuCloseButtonSound);
        Application.LoadLevel("GameScene");
    }


    void Awake()
    {
        float targetaspect = 9f / 16f;
        float windowaspect = (float)Screen.width / (float)Screen.height;
        float scaleheight = windowaspect / targetaspect;
        Rect rect = new Rect();
        if (scaleheight < 1.0f)
        {
            rect.width = 1.0f;
            rect.height = scaleheight;
            rect.x = 0;
            rect.y = (1.0f - scaleheight) / 2.0f;
        }
        else
        {
            float scalewidth = 1.0f / scaleheight;
            rect.width = scalewidth;
            rect.height = 1.0f;
            rect.x = (1.0f - scalewidth) / 2.0f;
            rect.y = 0;
        }
        for (int i = 0; i < Camera.allCamerasCount; i++)
        {
            Camera camera = Camera.allCameras[i];
            if (camera.name == "ClearCamera")
                continue;
            camera.rect = rect;
        }

        if (bool.TryParse(SecurityIO.GetString("bgm"), out isBGM) == false)
        {
            SecurityIO.SetString("bgm", "true");
            isBGM = true;
        }
        if (bool.TryParse(SecurityIO.GetString("sfx"), out isSFX) == false)
        {
            SecurityIO.SetString("sfx", "true");
            isSFX = true;
        }

        if (isSFX == true)
        {
            sfxCheck.enabled = true;
        }
        else
        {
            sfxCheck.enabled = false;
        }
        if (isBGM == true)
        {
            bgmCheck.enabled = true;
        }
        else
        {
            bgmCheck.enabled = false;
        }


        menuButton.onClick.Add(new EventDelegate(this, "MenuButton"));
        menuCloseButton.onClick.Add(new EventDelegate(this, "MenuCloseButton"));
        item1Button.onClick.Add(new EventDelegate(this, "Item1Button"));
        item2Button.onClick.Add(new EventDelegate(this, "Item2Button"));
        item3Button.onClick.Add(new EventDelegate(this, "Item3Button"));
        item4Button.onClick.Add(new EventDelegate(this, "Item4Button"));
        item5Button.onClick.Add(new EventDelegate(this, "Item5Button"));
        sfxButton.onClick.Add(new EventDelegate(this, "SFXButtonTouched"));
        bgmButton.onClick.Add(new EventDelegate(this, "BGMButtonTouched"));
        titleButton.onClick.Add(new EventDelegate(this, "TitleButtonTouched"));
        retryButton.onClick.Add(new EventDelegate(this, "RetryButtonTouched"));

        GameManager manager = GameManager.GetInstance();
        manager.Init();
        manager.SetFrame(scoreFrame, bonusFrame);
	}

    public void MenuOpen()
    {
        menuParent.SetActive(true);
        isPaused = true;
    }
    public void MenuClose()
    {
        menuParent.SetActive(false);
        isPaused = false;
    }

    public void MenuCloseButton()
    {
        MenuClose();
        if( isSFX == true)
        {
            audio.PlayOneShot(menuCloseButtonSound);
        }
    }

    public void MenuButton()
    {
        MenuOpen();
        if( isSFX == true)
        {
            audio.PlayOneShot(menuButtonSound);
        }
    }

    private IEnumerator WaitForUseItem(WWW www)
    {
        yield return www;
//        Debug.Log(www.url);
        if( www.error == null)
        {
//            Debug.Log(www.text);
        }
        else
        {
//            Debug.Log(www.error);
        }
    }

    private IEnumerator WaitForItemQTY(WWW www)
    {
        yield return www;
        if( www.error == null)
        {
            int[] item = new int[5];
            JsonReader reader = new JsonReader(www.text);
            int id = 0;
            //int qty = 0;
            while (reader.Read())
            {
                if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "item_id")
                {
                    reader.Read();
                    int tryOut_int;
                    int.TryParse(reader.Value.ToString(), out tryOut_int);
                    id = tryOut_int;
                }
                else if (reader.Token == JsonToken.PropertyName && reader.Value.ToString() == "quantity")
                {
                    reader.Read();
                    int tryOut_int;
                    int.TryParse(reader.Value.ToString(), out tryOut_int);
                    if (id == 3)
                        item[0] = tryOut_int;
                    else if (id == 4)
                        item[1] = tryOut_int;
                }
            }
            item1Label.text = "x" + item[0].ToString();
            item2Label.text = "x" + item[1].ToString();
            GameManager.GetInstance().SetItemQTY(item[0], item[1], item[2], item[3], item[4]);
        }
        else
        {

        }
    }

    public void Item1Button()
    {
        int qty = GameManager.GetInstance().UseItem(0);
        if( qty != -1)
        {
            item1Label.text = "x" + qty.ToString();
            WWWForm form = new WWWForm();
            form.AddField("quantity", qty.ToString());
            StartCoroutine(WaitForUseItem(new WWW("https://rrgames.me:3000/users/" + SecurityIO.GetString("id") + "/items/3?token=" + SecurityIO.GetString("token") , form)));
            // turn add id
        }
    }
    public void Item2Button()
    {
        int qty = GameManager.GetInstance().UseItem(1);
        if( qty != -1)
        {
            item2Label.text = "x" + qty.ToString();
            WWWForm form = new WWWForm();
            form.AddField("quantity", qty.ToString());
            StartCoroutine(WaitForUseItem(new WWW("https://rrgames.me:3000/users/" + SecurityIO.GetString("id") + "/items/4?token=" + SecurityIO.GetString("token"), form)));
            // timeslip id
        }
    }
    public void Item3Button()
    {
        //GameManager.GetInstance().UseItem(2);
    }
    public void Item4Button()
    {
        //GameManager.GetInstance().UseItem(3);
    }
    public void Item5Button()
    {
        //GameManager.GetInstance().UseItem(4);
    }

	// Use this for initialization
    void Start()
    {
        StartCoroutine(WaitForItemQTY(new WWW("https://rrgames.me:3000/users/"+SecurityIO.GetString("id")+"/items")));
        audio.clip = bgm;
        audio.loop = true;
        if (isBGM == true)
        {
            audio.Play();
        }
	}
	



	// Update is called once per frame
	void Update () {

        if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if( isPaused == true)
                {
                    MenuCloseButton();
                }
                else
                {
                    MenuButton();
                }
            }
            else if (Input.GetKeyDown(KeyCode.Menu))
            {
                if( isPaused == true)
                {

                }
                else
                {
                    MenuButton();
                }
                //menu button
            }
        }
        if( isPaused == true)
        {
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector3 rot = transform.TransformDirection(0, 0, 1);
                Vector3 pos = this.gameObject.camera.ScreenToWorldPoint(Input.mousePosition);
                pos.z = -100.0f;
                RaycastHit input_hit;
                if (Physics.Raycast(pos, rot, out input_hit, 500.0f))
                {
                    if (input_hit.collider.name == "block")
                    {
                        Block block = input_hit.collider.GetComponent<Block>();
                        int sound = GameManager.GetInstance().TryTouch(block.x, block.y);
                        if (isSFX == false)
                            sound = -1;
                        switch (sound)
                        {
                            case 0:
                                audio.PlayOneShot(tapFail);
                                break;
                            case 1:
                                audio.PlayOneShot(combo1Sound);
                                // 1combo sound
                                break;
                            case 2:
                                audio.PlayOneShot(combo2Sound);
                                // 2combo sound
                                break;
                            case 3:
                                audio.PlayOneShot(combo3Sound);
                                // 3combo sound
                                break;
                            case 4:
                                audio.PlayOneShot(combo4Sound);
                                // 3combo sound
                                break;
                        }
                    }
                }
            }
            GameManager.GetInstance().Update(Time.deltaTime);
        }
	}
}
