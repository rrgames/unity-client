﻿using UnityEngine;
using System.Collections;

public class BackGroundScript : MonoBehaviour {
    public Sprite renderingTarget;
    public float speed;
    public float startX;
    public float lengthX;
    public float startY;
    public float lengthY;
    public float interval;
    public Color color;

    private GameObject[] garo;
    private GameObject[] sero;
    private float progress;

    void Awake()
    {
        int garoSize = (int)(lengthX / interval) + 1;
        int seroSize = (int)(lengthY / interval) + 1;
        garo = new GameObject[garoSize];
        sero = new GameObject[seroSize];
    }
	// Use this for initialization
	void Start () {
        GameObject groupX = new GameObject("LineGroupX");
        GameObject groupY = new GameObject("LineGroupY");
        groupX.transform.parent = GameObject.Find("Main Camera").transform;
        groupY.transform.parent = GameObject.Find("Main Camera").transform;
        groupX.transform.localPosition = new Vector3(0f, 0f, 10f);
        groupY.transform.localPosition = new Vector3(0f, 0f, 10f);
        for (int i = 0; i < garo.Length; i++)
        {
            garo[i] = new GameObject("Line");
            garo[i].AddComponent<SpriteRenderer>();
            garo[i].transform.parent = groupX.transform;
            garo[i].transform.localPosition = new Vector3(startX + interval * i, 0f, 0f);
            garo[i].transform.localScale = new Vector3(1.5f,1.3f,1f);
			garo[i].transform.localRotation = Quaternion.Euler(new Vector3(0f,0f,0f));
			garo[i].GetComponent<SpriteRenderer>().sprite = renderingTarget;
			garo[i].GetComponent<SpriteRenderer>().sortingOrder = -2;
            garo[i].GetComponent<SpriteRenderer>().color = color;// new Color(0f, 0.13f, 0.13f, 1f);
        }
        for (int i = 0; i < sero.Length; i++)
        {
            sero[i] = new GameObject("Line");
            sero[i].AddComponent<SpriteRenderer>();
            sero[i].transform.parent = groupY.transform;
            sero[i].transform.localPosition = new Vector3(0f, startY + interval * i);
            sero[i].transform.localScale = new Vector3(1.5f, 0.9f, 1f);
            sero[i].transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, 90f));
			sero[i].GetComponent<SpriteRenderer>().sprite = renderingTarget;
			sero[i].GetComponent<SpriteRenderer>().sortingOrder = -2;
            sero[i].GetComponent<SpriteRenderer>().color = color;//new Color(0f, 0.13f, 0.13f, 1f);
        }
	/*
        float startTime = Time.time;
        GameObject texture = new GameObject("Star");// MonoBehaviour.Instantiate(instance) as GameObject;
        texture.AddComponent<SpriteRenderer>();
        texture.transform.parent = gameObject.transform;
        texture.transform.localPosition = Vector3.zero;
        texture.transform.localScale = new Vector3(fromScale, fromScale);
        texture.transform.localRotation = Quaternion.Euler(new Vector3(0f,0f,fromAngle));
        texture.GetComponent<SpriteRenderer>().sprite = renderingTarget;
        texture.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, alpha);
        yield return null;

        while(true)
        {
            float time = Time.time - startTime;
            if( animationTime < time)
            {
                texture.transform.localScale = new Vector3(toScale, toScale);
                texture.transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, toAngle));
                break;
            }
            else
            {
                float progress = time / animationTime;
                float scale = Mathf.Lerp(fromScale, toScale, progress);
                texture.transform.localScale = new Vector3(scale,scale);
                texture.transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, Mathf.Lerp(fromAngle, toAngle, progress)));
            }
            yield return null;
        }
        if( !noDelete)
            GameObject.Destroy(texture);
        yield break;*/
	}
	
	// Update is called once per frame
	void Update () {
        progress += Time.deltaTime * speed;
        if( progress > interval)
        {
            progress -= interval;
        }

        for (int i = 0; i < garo.Length; i++)
        {
            garo[i].transform.localPosition = new Vector3(startX + progress + interval * i, 0f, 0f);
            garo[i].GetComponent<SpriteRenderer>().color = color;// new Color(0f, 0.13f, 0.13f, 1f);
        }
        for (int i = 0; i < sero.Length; i++)
        {
            sero[i].transform.localPosition = new Vector3(0f, startY - progress + interval * i);
            sero[i].GetComponent<SpriteRenderer>().color = color;//new Color(0f, 0.13f, 0.13f, 1f);
        }
	}
}
